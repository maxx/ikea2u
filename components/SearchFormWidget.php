<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii;

class SearchFormWidget extends Widget {

    public function init() {

        parent::init();
    }

    public function run() {
         

        $model = new \app\models\SearchForm();
        $model->q = Yii::$app->getRequest()->getQueryParam('q');
        return $this->render('SearchForm', [
                    'model' => $model,
        ]);
    }

}

?>
