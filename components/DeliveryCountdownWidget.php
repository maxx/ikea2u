<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii;

class DeliveryCountdownWidget extends Widget {

    public function init() {
        parent::init();
    }

    public function run() {
        $modelSetting = \app\models\Setting::find()->one();
        
        return $this->render('DeliveryCountdown', [
                    'modelSetting' => $modelSetting,
        ]);
    }

}

?>
