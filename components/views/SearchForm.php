<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'search-form',
            'options' => ['class' => 'navbar-form'],
            'action' => '/item/search',
            'method' => 'GET',
            'fieldConfig' => [
                'template' => "n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-4\"></div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]);
?>

<div class="input-group">
    <?= $form->field($model, 'q')->textInput(array('name' => 'q', 'placeholder'=>'Поиск...')); ?>


    <div class="input-group-btn">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-default', 'name' => '']) ?>

    </div>
</div>
<?php ActiveForm::end();
?>
 