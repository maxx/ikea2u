<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Utilities;

class ShoppingCartWidget extends Widget {

    public function run() {

        return $this->render('ShoppingCart', [
                    'cart_qty' => \Yii::$app->cart->getCount(),
                    'cart_total_price' => Utilities::Currency(\Yii::$app->cart->getCost()),
        ]);
    }

}

?>
