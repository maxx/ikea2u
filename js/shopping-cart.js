$(function () {
    $('.delete').click(function () {
        var row = $(this).closest('tr');
        var id = getItemId($(this).attr('data-item'));
        /*var cont = $(this)
         cont.hide();
         cont.parent().append('<div id="loading"></div>');*/
        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType: 'json',
            data: 'id=' + id,
            success: function (data) {
                if (data.code === 200) {
                    //$('#cart-quantity').html(data.count);
                    //$('.cart-cost').html(data.cost);
                    row.remove();
                    $('.navbar .cart-list.pull-right').replaceWith(data.cart_html);
                }
            }
        });
        return false;
    });

    $(document).on('click', '.add-to-cart', (function (e) {
        e.preventDefault();

        if ($('.alert-success').is(':animated')) {
            $('.alert-success').stop().animate({opacity: '100'});
        }

        var id = $(this).data('item-id')
        var qty = parseInt($(this).parent().find('.qty').val());

        qty = qty ? qty : 1;

        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            data: 'id=' + id + '&qty=' + qty,
            dataType: 'json',
            success: function (data) {

                if (data.code === "200") {
                    $('.alert-success').fadeIn(100).fadeOut(6000);
                    $('.cart-qty').html(data.cart_qty);
                    $('.cart-total-price').html(data.cart_total_price);
                    $('.cart-total-items').html(data.cart_total_items);
                    $('.navbar .cart-list.pull-right').replaceWith(data.cart_html);
                }
                if (data.code === "404") {
                    alert('Item is not exists');
                }
            }
        });

        return false;
    }));

    /*    
     function showMsg(){
     var body = '<strong>Товар добавлен в корзину!</strong> <br/> <a href="/cart">Перейти к оформлению</a>';
     $.jGrowl(body, {
     check:2000,
     position:'top-right'
     });
     $.jGrowl.defaults.closerTemplate = '<div>Скрыть все</div>';
     }
     
     $('#update-cart').click(function(event){
     event.preventDefault();
     $('#cart').submit();
     });
     
     $('#order-add').click(function(){
     $('#order-details-form').submit();
     });    
     
     $('.plus').click(function(event){
     event.preventDefault();        
     var input = $(this).parent('div').find('.qty,#qty');
     var newQty = +(input.val()) + 1;
     input.val(newQty);
     });
     $('.minus').click(function(event){
     event.preventDefault();
     var input = $(this).parent('div').find('.qty,#qty');
     var newQty = +(input.val()) - 1;
     if (newQty>=0) input.val(newQty);
     else input.val('0');
     });        
     */
});


function getItemId(str) {
    return str.replace('item_', '')
}

function DeleteItem(id) {
    if (confirm("Вы уверены, что хотите удалить товар из корзины?")) {
        $.ajax({
            url: "/cart/delete",
            type: 'POST',
            data: "id=" + $(this).parents('tr').find('.qty'),
            success: function () {
                RefreshCart();
                UpdateShoppingCart();
                $(this).parents('tr').fadeOut();
            }
        });

    }
}

function RefreshCart() {
    $.ajax({
        url: "/order/cartajax",
        type: 'POST',
        dataType: "html",
        success: function (data) {
            $("#cart-positions").html(data);
        }
    });
}

function UpdateShoppingCart() {
    $.ajax({
        //url:'/cart/get-cart-quantity',
        url: '/cart/getcartquantity',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#cart-quantity').html(data.count);
            $('.cart_price, #cart-total').html(data.cost);
        }
    });
}


