$(document).ready(function () {

    $(document).on('click', '.item-create .delete', (function (e) {
        if (confirm('Вы уверены, что хотите удалить товар из корзины?')) {
            var row = $(this).closest('tr');
            var id = getItemId($(this).attr('data-item'));
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                dataType: 'json',
                data: 'id=' + id,
                success: function (data) {
                    if (data.code === 200) {
                        row.remove();
                    }
                }
            });
        }
        return false;
    }));

    $(".add-to-stock").on('click', function (e) {
        e.preventDefault();
        var obj = $(this);
        $.ajax({
            url: $(this).attr('href'),
            dataType: 'json',
            success: function (data) {
                if (data.code === 200) {
                    obj.text('Добавлен');
                }
            }
        });

        return false;
    });
});

