$(document).ready(function () {

    var finalDate = new Date($("#finalCountdown").html());
    $("#finalCountdown")
            .countdown(finalDate, function (event) {
                $(this).text(
                        event.strftime('%D дней %H часов %M минут %S секунд')
                        );
            });
            
        $('.items-list').jscroll({
            loadingHtml: '<div style="text-align:center">Loading...</div>',
            debug: false,
            padding: 1,
            nextSelector: '.pagination .next a',
            contentSelector: '',
        });           
});
