<?php

namespace app\Entities;
class Category
{
  public $ikeaIdSlug;
  public $title;
  public $href;
  public $number;
  public $imageSrc;

  public function __construct($ikeaIdSlug='', $title='', $href='', $imageSrc='')
  {
    $this->ikeaIdSlug=$ikeaIdSlug;
    $this->title=$title;
    $this->href=$href;
    $this->imageSrc=$imageSrc;
  }

  /**
   * @return mixed|string
   */
  public function getTitle()
  {
    return trim($this->title);
  }

  /**
   * @return mixed|string
   */
  public function getIkeaUrlSlug()
  {
    return str_replace('https://www.ikea.com', '', $this->href);
  }

  /**
   * @return mixed|string
   */
  public function getSlug() //TODO implement
  {
    //return str_replace('http://www.ikea.com', '', $this->href);
  }

  public function getCategoryNumber()
  {
    $ar=explode('/', $this->href);
    end($ar);
    $prev=prev($ar);
    $categoryAr=explode('-', $prev);
    $ikea_category=end($categoryAr);
    return $ikea_category;
  }

}