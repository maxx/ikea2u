<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\IkeaItemService;
use app\models\Utilities;
use DeepCopy\Matcher\PropertyTypeMatcher;
use yii\console\Controller;
use DiDom\Document;
use app\models\Category;
use app\models\CategoryItem;
use app\models\Item;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
  /**
   * This command echoes what you have entered as the message.
   * @param string $message the message to be echoed.
   */
  /*public function actionIndex($message = 'hello world')
  {
      echo $message . "\n";
  }*/

  // php yii hello/main-cats-list
  public function actionMainCatsList()
  {
    $link='https://www.ikea.com/ru/ru/cat/tovary-products/';
    $curl=curl_init();
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);

    $html=curl_exec($curl);
    $info=curl_getinfo($curl);

    curl_close($curl);
    $document=new Document();
    $parsedCategoryList=[];
    if ($html) {
      $document->loadHTML($html);
      $columns=$document->find('.vn-accordion__item');
      if (!empty($columns)) {

        foreach ($columns as $column) {


          $categoryData=$column->find('h4.smula span')[0];
          $categoryIdSlug=trim($categoryData->attr('id'));
          $categoryTitle=trim($categoryData->text());

          //Игнорируем ЕДУ
          if (strpos(mb_strtolower($categoryTitle), 'еда') !== false || strpos($categoryTitle, 'Eда') !== false
              || $categoryTitle == 'Еда в ИКЕА' || $categoryIdSlug == 'vn-accordion-title-fb001'
          ) {
            continue;
          }

          //Main category
          $imageSrc=$column->find('img.vn-aspect-ratio-image__image')[0]->attr('src');
          $mainCategoryEntity=new \app\Entities\Category($categoryIdSlug, $categoryTitle, '', $imageSrc);
          $parsedCategoryEntity[$categoryIdSlug]['id']=$mainCategoryEntity->ikeaIdSlug;
          $parsedCategoryEntity[$categoryIdSlug]['title']=$mainCategoryEntity->title;
          $parsedCategoryEntity[$categoryIdSlug]['href']='';
          $parsedCategoryEntity[$categoryIdSlug]['number']='';
          $parsedCategoryEntity[$categoryIdSlug]['imageSrc']=$mainCategoryEntity->imageSrc;

          //for subcategories
          $parsedCategoryEntity[$categoryIdSlug]['subcategories']=[];

          $subcategoriesList=$column->find('ul li a.vn-link.vn-nav__link');
          if (!empty($subcategoriesList)) {
            $i=0;

            foreach ($subcategoriesList as $subcategory) {
              $subcategoryTitle=$subcategory->text();
              $subcategoryHref=$subcategory->attr('href');
              if ($i === 0) {
                $mainCategoryEntity->href=$subcategoryHref;
                $parsedCategoryEntity[$categoryIdSlug]['href']=$mainCategoryEntity->href;
                $parsedCategoryEntity[$categoryIdSlug]['number']=$mainCategoryEntity->getCategoryNumber();
              } else {
                $parsedCategoryEntitySubcategory=new \app\Entities\Category('', $subcategoryTitle, $subcategoryHref, '');
                $parsedCategoryEntitySubcategory=[
                    'id'=>$parsedCategoryEntitySubcategory->ikeaIdSlug,
                    'title'=>$parsedCategoryEntitySubcategory->title,
                    'href'=>$parsedCategoryEntitySubcategory->href,
                    'number'=>$parsedCategoryEntitySubcategory->getCategoryNumber()
                ];
                array_push($parsedCategoryEntity[$categoryIdSlug]['subcategories'], $parsedCategoryEntitySubcategory);
              }
              $i++;
            }
            array_push($parsedCategoryList, $parsedCategoryEntity[$categoryIdSlug]);
          }
        }
        var_dump($parsedCategoryList);
        //die();
      }
    }

    if (!empty($parsedCategoryList)) {
      $category=\app\models\Category::findOne(['title'=>'ikea_categories']);
      if (!$category) {
        $category=new \app\models\Category(array('title'=>'ikea_categories'));
        $category->makeRoot();
      }
      foreach ($parsedCategoryList as $cat) {
        $categoryEntity=new \app\Entities\Category($cat['id'], $cat['title'], $cat['href'], $cat['imageSrc']);

        $node=new \app\models\Category(
            [
                'title'=>$categoryEntity->title,
                'ikea_url_slug'=>$categoryEntity->getIkeaUrlSlug(),
                'ikea_category'=>$categoryEntity->getCategoryNumber(),
                'main_image_src'=>$categoryEntity->imageSrc
            ]
        );
        $node->appendTo($category);
        if (!empty($cat['subcategories'])) {
          foreach ($cat['subcategories'] as $subcategory) {
            $subCategoryEntity=new \app\Entities\Category($subcategory['id'], $subcategory['title'], $subcategory['href']);
            $newCat=new \app\models\Category([
                'title'=>$subCategoryEntity->title,
                'ikea_url_slug'=>$subCategoryEntity->getIkeaUrlSlug(),
                'ikea_category'=>$subCategoryEntity->getCategoryNumber(),
            ]);
            $newCat->appendTo($node);
          }
        }


        /*$node=\app\models\Category::findOne(['ikea_url_slug'=>$cat]);
        if (!$node) {
          $x=mb_strtolower($title, 'UTF-8');
          $fc=ucfirst($x);

          $node=new \app\models\Category(['title'=>$fc, 'ikea_url_slug'=>$cat]);
          $node->appendTo($category);
        }*/
      }

      die('Main categories have been created');
    }

  }

  public function actionPMainCats_OLD()
  {

    $category=\app\models\Category::findOne(['title'=>'ikea_categories']);
    if (!$category) {
      $category=new \app\models\Category(array('title'=>'ikea_categories'));
      $category->makeRoot();
    }

    $cats=[
        '/ru/ru/rooms/bedroom/'=>'СПАЛЬНЯ',
        '/ru/ru/rooms/living-room/'=>'ГОСТИНАЯ',
        '/ru/ru/rooms/kitchen/'=>'КУХНЯ',
        '/ru/ru/rooms/bathroom/'=>'ВАННАЯ',
        '/ru/ru/rooms/dining/'=>'СТОЛОВАЯ',
        '/ru/ru/rooms/childrens-room/'=>'ДЕТСКАЯ',
        '/ru/ru/rooms/home-office/'=>'РАБОЧИЙ КАБИНЕТ',
        '/ru/ru/rooms/hallway/'=>'ПРИХОЖАЯ',
        '/ru/ru/rooms/bathroom/'=>'ВАННАЯ',
        '/ru/ru/rooms/outdoor/'=>'БАЛКОН И САД',
        '/ru/ru/cat/zimnyaya-kollekciya-wt001/'=>'ЗИМНЯЯ КОЛЛЕКЦИЯ'
    ];

    $site='http://www.ikea.com';
    $subcats=array();

    foreach ($cats as $cat=>$title) {
      $node=\app\models\Category::findOne(['ikea_url_slug'=>$cat]);
      if (!$node) {
        $x=mb_strtolower($title, 'UTF-8');
        $fc=ucfirst($x);

        $node=new \app\models\Category(['title'=>$fc, 'ikea_url_slug'=>$cat]);
        $node->appendTo($category);
      }
    }

    die('Main categories have been created');
  }

  public
  function actionPSubCatsOLD()
  {
    $root=\app\models\Category::findOne(['title'=>'ikea_categories']);
    $cats=$root->children()->all();
    $parsedSubCategories=[];
    $test=[];

    $mainUrl='https://www.ikea.com';
    foreach ($cats as $cat) {

      $targetUrl=$mainUrl . $cat->ikea_url_slug;

      // $targetUrl = 'https://www.ikea.com/ru/ru/cat/tovary-dlya-detey-3-7-let-bc003/';
      //          var_dump($x);

      $curl=curl_init();
      curl_setopt($curl, CURLOPT_URL, $targetUrl);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HEADER, false);

      $html=curl_exec($curl);
      $info=curl_getinfo($curl);

      curl_close($curl);
      //if ($info['http_code'] !== 200)
      //  return array();

      $html=trim($html);

      if (!$html) {
        var_dump($targetUrl);
      } else {
        $document=new Document();
        $document->loadHTML($html);

        $subCategories=$document->find('div.vn-carousel div.vn__wrapper nav.vn__nav a'); //for page like this https://www.ikea.com/ru/ru/catalog/categories/departments/bedroom/

        if (!empty($subCategories)) {
          foreach ($subCategories as $subCategory) {
            $title=trim(str_replace('>', '', $subCategory->text()));
            if (strpos($title, 'Доставка') !== false || strpos($title, 'Возврат') !== false) {

            } else {
              $cleanedUrl=\app\models\Utilities::cleanIkeaUrl($subCategory->attr('href'));
              if (strpos($cleanedUrl, 'vozvrat') == false) {
                if ($cleanedUrl) {
                  $newCat=new \app\models\Category(['title'=>$title, 'ikea_url_slug'=>$cleanedUrl]);
                  $newCat->appendTo($cat);
                }
              }
            }
          }
        } else {//for pages like https://www.ikea.com/ru/ru/catalog/categories/departments/childrens_ikea/
          /* $subCategories=$document->find('.range-catalog-list__list li a');
           foreach ($subCategories as $subCategory) {
             $title=trim(str_replace('>', '', $subCategory->text()));
             if (strpos($title, 'Доставка') !== false || strpos($title, 'Возврат') !== false) {

             } else {
               $cleanedUrl=\app\models\Utilities::cleanIkeaUrl($subCategory->attr('href'));
               if (strpos($cleanedUrl, 'vozvrat') == false) {
                 if ($title && $cleanedUrl) {
                   $newCat=new \app\models\Category(['title'=>trim($title), 'ikea_url_slug'=>$cleanedUrl]);
                   $newCat->appendTo($cat);
                 }
               }
             }
           }*/
        }
      }
    }
    die(' SUBCATEGORIES  have been created');
  }

//ALTER TABLE `category` ADD COLUMN `main_image_src` VARCHAR(50) NULL DEFAULT NULL AFTER `ikea_url_slug`;
//ALTER TABLE `item` ADD COLUMN `image_json` JSON NULL DEFAULT NULL AFTER `image_json`;
//ALTER TABLE `item` ADD COLUMN `image_json` JSON NULL DEFAULT NULL AFTER `image_uri`;

  public function actionPSubCatsXml()
  {
    $root=\app\models\Category::findOne(['title'=>'ikea_categories']);
    //$cats=$root->children()->all();
    $parsedSubCategories=[];
    $test=[];

    $targetUrl='https://www.ikea.com/sitemaps/cat-ru-RU_1.xml';

    $curl=curl_init();
    curl_setopt($curl, CURLOPT_URL, $targetUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);

    $html=curl_exec($curl);
    $info=curl_getinfo($curl);

    curl_close($curl);
    //if ($info['http_code'] !== 200)
    //  return array();

    $html=trim($html);

    $pattern='~[a-z]+://www.ikea.com/ru/ru/cat/(.*?)/~';

    $res=[];
    if ($num_found=preg_match_all($pattern, $html, $out)) {
      $res=!empty($out[0]) ? $out[0] : [];
      $uniq=array_unique($res);
      echo "FOUND " . count($uniq) . " LINKS:\n";
      //print_r($uniq);
    }
    $tree=[];
    $treeLatest=[];
    if (!empty($res)) {
      $max=0;
      $limit=2;
      foreach ($res as $link) {
        //if ($max > $limit) break;

        $curl=curl_init();
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);

        $html=curl_exec($curl);
        $info=curl_getinfo($curl);

        curl_close($curl);
        $document=new Document();
        //libxml_use_internal_errors(true);


        if ($html) {
          $document->loadHTML($html);

          /*
           * TODO improve finding parent of this category
           * https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/
           *             <li class="bc-breadcrumb__list-item">
              <a href="https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/"
                 class="bc-breadcrumb__link bc-link bc-link--black"
                 data-tracking-label="products | kt001 | 18850 | 18851"><span>Кухонные полотенца</span></a>
            </li>
           * */
//extract categoryNumber fine
          /*$last=$document->find('ol.bc-breadcrumb__list li:last-child a')[0];
          //string(40) "products | bm001 | tl004 | 20529 | 20530"
          $dataTrackingLabel=$last->attr('data-tracking-label');
          $dataTrackingLabelArray=explode('|', $dataTrackingLabel);
          $categoryNumber=trim($dataTrackingLabelArray[count($dataTrackingLabelArray) - 2]);
          var_dump($dataTrackingLabelArray, $categoryNumber);
          die();*/

          $breadcrumbs=$document->find('ol.bc-breadcrumb__list a');
          if (!empty($breadcrumbs)) {
            $i=0;
            $s='';
            $tmpLinks=[];
            foreach ($breadcrumbs as $node) {
              $href=$node->attr('href');
              $title=trim($node->text());
              if (Utilities::isForbiddenCategoriesWord($title)) {
                continue;
              }


              //$tree[$i] = array($tree[$i] => $href);
              //$s.=str_replace('https://www.ikea.com/ru/ru/', '', $href) . '>';
              $cleanedStr=str_replace('https://www.ikea.com/ru/ru/cat/', '', $href);
              if (strpos($cleanedStr, 'eda') !== false) {
                $cleanedStr='';
              }
              if ($cleanedStr) {
                echo $cleanedStr . "\n";
                $s=$cleanedStr;
                $tmpLinks[$i]=$cleanedStr;
                $tmp[$i]=$s;
                $before=$i > 0 ? $i - 1 : 0;
                if ($i > 0) {
                  $s=$tmp[$before] . $s;

                }

                $tmp[$i]=$s;
                $tree[$s]=$i;

                $ikea_url_slug='/ru/ru/cat/' . $cleanedStr;
                $before_ikea_url_slug='/ru/ru/cat/' . $tmpLinks[$before];


                $ar=explode('/', $ikea_url_slug);
                end($ar);
                $prev=prev($ar);
                $categoryAr=explode('-', $prev);
                $ikea_category=end($categoryAr);

                // var_dump('current: ' . $ikea_url_slug);
                //var_dump('parent: ' . $before_ikea_url_slug);
                echo "\n";
                if ($ikea_url_slug !== $before_ikea_url_slug) {
                  $cat=\app\models\Category::find()->where(['ikea_url_slug'=>$ikea_url_slug])->one();
                  if (!$cat) {
                    $parentCat=\app\models\Category::find()->where(['ikea_url_slug'=>$before_ikea_url_slug])->one();
                    $newCat=new Category();
                    $newCat->title=$title;
                    $newCat->ikea_url_slug=$ikea_url_slug;
                    $newCat->ikea_category=$ikea_category;

                    if ($parentCat) {
                      $newCat->appendTo($parentCat);
                    } else {
                      $newCat->appendTo($root);
                    }
                  } else {
                    $cat->title=$title;
                    $cat->ikea_category=$ikea_category;
                    $cat->save();
                  }
                }
                $i++;
              }
            }
          }

        }
        $max++;
        $treeLatest[]=end($tree);
      }
      //var_dump($breadcrumbs);
      //print_r($tree);
      //print_r($treeLatest);

      //var_dump(count($tree));
      die('finished');

      //var_dump($link);
      die();

    }
    //bc-breadcrumb__list

  }

  public function actionPItemsXml()
  {
    $allItemsLinks=[];
    for ($m=1; $m < 13; $m++) {
      $targetUrl='https://www.ikea.com/sitemaps/prod-ru-RU_' . $m . '.xml';

      $curl=curl_init();
      curl_setopt($curl, CURLOPT_URL, $targetUrl);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HEADER, false);

      $html=curl_exec($curl);
      $info=curl_getinfo($curl);

      curl_close($curl);
      //if ($info['http_code'] !== 200)
      //  return array();

      $html=trim($html);

      $pattern='~[a-z]+://www.ikea.com/ru/ru/p/(.*?)/~';

      $res=[];
      if ($num_found=preg_match_all($pattern, $html, $out)) {
        $res=!empty($out[0]) ? $out[0] : [];
        $uniq=array_unique($res);
        unset($res);
        echo "FOUND " . count($uniq) . " LINKS:\n";
        //print_r($uniq);
      }
      $allItemsLinks=array_merge($allItemsLinks, $uniq);
    }
    $c=count($allItemsLinks);
    $rate=\app\models\Setting::find()->asArray()->one();

    foreach ($allItemsLinks as $link) {

      $curl=curl_init();
      curl_setopt($curl, CURLOPT_URL, $link);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HEADER, false);
      curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');

      $html=curl_exec($curl);
      $info=curl_getinfo($curl);
      curl_close($curl);

      if ($html) {
        $values=IkeaItemService::parseItemFromIkeaItemHtml($html);
        if (!empty($values['productid'])) {
          $model=IkeaItemService::createModel($values, $rate);
          if ($model->save()) {
            if (!empty($values['categoryId'])) {
              $cid=$values['categoryId'];
              $cat=Category::find()->where(
                  ['ikea_category'=>$cid]
              )->one();
              if ($cat) {
                $catItem=CategoryItem::findOne(['item_id'=>$model->id, 'category_id'=>$cat->id]);
                if (!$catItem) {
                  $catItem=new CategoryItem();
                  $catItem->item_id=$model->id;
                  $catItem->category_id=$cat['id'];
                  if ($catItem->save()) {

                  }
                }
              }
            }
            // return TRUE;
          } else {
            var_dump($model->getErrors());
            // die();
            //return FALSE;
          }
        }
      }

    }


  }

  public
  function actionPItemsNotUSe()
  {
    set_time_limit(10000);
    ini_set('memory_limit', '2048M');

    $rate=\app\models\Setting::find()->asArray()->one();
    $rate_rus=$rate['rate_rus_rubl'];
    $rate_additional=$rate['additional_rate'];

    //$root = \app\models\Category::findOne(['title' => 'ikea_categories']);
    //$cats = $root->children(4)->all();
    //$cats = \app\models\Category::findAll();
    //$cats = \app\models\Category::find()->leaves()->orderBy('`id` DESC')->all();
    //$cats = \app\models\Category::find()->where(['depth'=>3])->orderBy('`lft` ASC')->all();
    $cats=\app\models\Category::find()->where(['id'=>368])->orderBy('`id` DESC')->all();

    $i=0;
    $f=0;

    //sleep(2);
    foreach ($cats as $cat) {
      for ($m=1; $m < 13; $m++) {
        $catUrl=$cat->ikea_url_slug;

        if ($catUrl) {
          //$postfix="?page=99";
          $m=99;
          $postfix=($m > 1) ? "?sort=priceAsc&page={$m}" : '';
          $site='https://www.ikea.com' . $catUrl . $postfix;
//          $site = 'https://www.ikea.com/ru/ru/cat/aksessuary-dlya-metod-50003/?page=3';

          $agent='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

          $curl=curl_init();
          curl_setopt($curl, CURLOPT_URL, $site);
          curl_setopt($curl, CURLOPT_USERAGENT, $agent);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                  'Content-Type: application/json',
              )
          );

          $html=curl_exec($curl);

          //print_r($html);
          //print_r($site);
          //die();
          $info=curl_getinfo($curl);

          curl_close($curl);
          $document=new Document();
          //libxml_use_internal_errors(true);
          if ($html) {
            $document->loadHTML($html);

            $posts=$document->find('.plp-product-list__products .range-revamp-product-compact');
            //$posts = $document->find("//div[contains(@class, 'product')]", \DiDom\Query::TYPE_XPATH);

            if (count($posts) > 0) {
            } else {
            }

            foreach ($posts as $post) {
              $product=$post->find('.range-revamp-product-compact');
              if (!empty($product[0])) {
                $product=$product[0];
                $url=$product->find('a')[0]->attr('href');
                $number=\app\models\Utilities::calculateIkeaItemNumber($product->attr('data-ref-id'));
                $text=$product->attr('data-product-name');
                // $html=$product->html();


                $divPrice=$product->find('.range-revamp-price__integer');
                $divPriceSpan=$product->find('.range-revamp-price__unit');


                $divImage=$post->find('img');
                $imageSrc=null;
                if (!empty($divImage[0])) {
                  $divImage=$divImage[0];
                  $imageSrc=$divImage->attr('src');
                  $imageSrc=str_replace('S3.JPG', 'S4.jpg', $imageSrc);
                }


                if (isset($divPrice[0])) {
                  $divPriceText=$divPrice[0]->text();
                  $divPriceSpanText=!empty($divPriceSpan[0]) ? str_replace('/', '', $divPriceSpan[0]->text()) : null;

                  //$shortDesc=$product->find('.range-revamp-header-section__description-text')[0]->text();
                  $shortDesc=$product->find('.range-revamp-header-section__description-text')[0]->text();
                  $shortDesc2=$product->find('.range-revamp-header-section__description-measurement')[0];
                  if ($shortDesc2)
                    $shortDesc2=$shortDesc2->text();
                  if ($shortDesc2)
                    $shortDesc.=', ' . $shortDesc2;

                  $title=$product->find('.range-revamp-header-section__title--small')[0]->text();


                  $qty_in_pack=1;
                  if (isset($product->find('.range-revamp-price__unit')[0])) {
                    $qty_in_pack=$product->find('.range-revamp-price__unit')[0]->text();
                    $qty_in_pack=filter_var($qty_in_pack, FILTER_SANITIZE_NUMBER_INT);
                  }

                  /*                                     * Не доработан, т.к. данные на уровень выше */
                  /* $metric = '';
                    if (isset($post->find('.size')[0])){
                    $metric = $post->find('.size')[0]->text();
                    }
                   */

                  //$price = substr($divPriceText, 0, strpos($divPriceText, ".–"));
                  $price=str_replace(' ', '', $divPriceText);
                  $arr=explode(".", $price);
                  $price=!empty($arr[0]) ? $arr[0] : 0;
                  $price=filter_var($price, FILTER_SANITIZE_NUMBER_INT);


                  //  $number = \app\models\Utilities::calculateIkeaItemNumber($url);
                  $model=\app\models\Item::findOne(['ikea_url_slug'=>$url]);
                  if (!$model && $number) {
                    $model=\app\models\Item::findOne(['number'=>$number]);
                  }
                  if (!$model) {
                    $model=new \app\models\Item();
                  }
                  //  var_dump($url, $number, $price, $text, $title, $url, $divPriceSpanText, $shortDesc, $qty_in_pack, $imageSrc);
                  //die();
                  //var_dump($shortDesc);
                  //var_dump($site);


                  if (strpos('Держатель для тарелок', $shortDesc)) {
                    var_dump('maxx');
                    die();

                  }
                  /*if ($number==='103.846.96'){
                    var_dump($html);
                    die();

                  }*/

                  if ($number != '303.846.24') { //forbid to update Albert from Ikea because item has custom price

                    $f++;
                    var_dump($number);


                    $model->number=$number;
                    $model->ikea_url_slug=$url;
                    $model->rus_price=(int)$price;
                    $model->price_span=$divPriceSpanText;
                    $model->title=trim($title);
                    $model->short_description=trim($shortDesc);
                    $model->qty_in_pack=$qty_in_pack ? $qty_in_pack : 1;
                    $model->image_uri=$imageSrc;
                    //$model->metric = $metric; недоработан
                    $model->bel_price=\app\models\IkeaItem::calcBelPrice($model->rus_price, $rate_additional, $rate_rus);
                    if ($model->validate()) {
                      if ($model->save()) {
                        $catItem=CategoryItem::findOne(['item_id'=>$model->id, 'category_id'=>$cat['id']]);
                        if (!$catItem) {
                          $catItem=new CategoryItem();
                          $catItem->item_id=$model->id;
                          $catItem->category_id=$cat['id'];
                          if ($catItem->save()) {
                            $i++;
                          }
                        }
                      } else {
                        var_dump($model->getErrors());
                        die();
                      }
                    } else {
                      var_dump($model->getErrors());
                      die();
                    }
                  }
                } else {

                }
              } else {
                //var_dump(gettype($product));
              }
            }
          }
        }
      }
    }
    $arr=array();
    $arr['success']=$i;
    $arr['attempts']=$f;
    print_r(json_encode($arr));
    die();
  }

  public
  function actionPItemsOld()
  {
    set_time_limit(10000);
    ini_set('memory_limit', '2048M');

    $rate=\app\models\Setting::find()->asArray()->one();
    $rate_rus=$rate['rate_rus_rubl'];
    $rate_additional=$rate['additional_rate'];

    //$root = \app\models\Category::findOne(['title' => 'ikea_categories']);
    //$cats = $root->children(4)->all();
    //$cats = \app\models\Category::findAll();
    //$cats = \app\models\Category::find()->leaves()->orderBy('`id` DESC')->all();
    //$cats = \app\models\Category::find()->where(['depth'=>3])->orderBy('`lft` ASC')->all();
    $cats=\app\models\Category::find()->orderBy('`id` DESC')->all();

    $i=0;
    $f=0;

    //sleep(2);
    foreach ($cats as $cat) {
      for ($m=12; $m > 0; $m--) {
        $catUrl=$cat->ikea_url_slug;

        /* if ($cat->depth == 2 || $cat->depth == 1 || $cat->depth == 3) {
          $catUrl = null;
          } */


        if ($catUrl) {
          $postfix=($m > 1) ? "page-{$m}/" : '';
          $site='https://www.ikea.com' . $catUrl . $postfix;

          $curl=curl_init();
          curl_setopt($curl, CURLOPT_URL, $site);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_HEADER, false);

          $html=curl_exec($curl);
          $info=curl_getinfo($curl);

          curl_close($curl);
          $document=new Document();
          //libxml_use_internal_errors(true);
          if ($html) {
            $document->loadHTML($html);

            $posts=$document->find('.range-product-list .product-compact');
            //$posts = $document->find("//div[contains(@class, 'product')]", \DiDom\Query::TYPE_XPATH);

            if (count($posts) > 0) {
              /* var_dump($site);
                var_dump(count($posts));
                echo "<br/>";
                echo "<br/>"; */
            } else {
              // var_dump($document);die();
            }
            foreach ($posts as $post) {
              $product=$post->find('a');
              if (!empty($product[0])) {
                $product=$product[0];
                $url=$product->attr('href');
                $number=\app\models\Utilities::calculateIkeaItemNumber($post->attr('data-ref-id'));
                $text=$product->text();
                $html=$product->html();
                $f++;

                $divPrice=$product->find('.product-compact__price__value');
                $divPriceSpan=$product->find('.product-compact__price__unit');


                $divImage=$post->find('img');
                $imageSrc=null;
                if (!empty($divImage[0])) {
                  $divImage=$divImage[0];
                  $imageSrc=$divImage->attr('src');
                  $imageSrc=str_replace('S3.JPG', 'S4.jpg', $imageSrc);
                }
                if (isset($divPrice[0])) {
                  $divPriceText=$divPrice[0]->text();
                  $divPriceSpanText=!empty($divPriceSpan[0]) ? str_replace('/', '', $divPriceSpan[0]->text()) : null;

                  $shortDesc=$product->find('.product-compact__type')[0]->text();
                  $title=$product->find('.product-compact__name')[0]->text();

                  $qty_in_pack=1;
                  if (isset($product->find('.unit')[0])) {
                    $qty_in_pack=$product->find('.unit')[0]->text();
                    $qty_in_pack=filter_var($qty_in_pack, FILTER_SANITIZE_NUMBER_INT);
                  }

                  /*                                     * Не доработан, т.к. данные на уровень выше */
                  /* $metric = '';
                    if (isset($post->find('.size')[0])){
                    $metric = $post->find('.size')[0]->text();
                    }
                   */

                  //$price = substr($divPriceText, 0, strpos($divPriceText, ".–"));
                  $price=str_replace(' ', '', $divPriceText);
                  $arr=explode(".", $price);
                  $price=!empty($arr[0]) ? $arr[0] : 0;
                  $price=filter_var($price, FILTER_SANITIZE_NUMBER_INT);


                  //  $number = \app\models\Utilities::calculateIkeaItemNumber($url);
                  $model=\app\models\Item::findOne(['ikea_url_slug'=>$url]);
                  if (!$model && $number) {
                    $model=\app\models\Item::findOne(['number'=>$number]);
                  }
                  if (!$model) {
                    $model=new \app\models\Item();
                  }

                  if ($number != '303.846.24') { //forbid to update Albert from Ikea because item has custom price
                    $model->number=$number;
                    $model->ikea_url_slug=$url;
                    $model->rus_price=(int)$price;
                    $model->price_span=$divPriceSpanText;
                    $model->title=trim($title);
                    $model->short_description=trim($shortDesc);
                    $model->qty_in_pack=$qty_in_pack ? $qty_in_pack : 1;
                    $model->image_uri=$imageSrc;
                    //$model->metric = $metric; недоработан
                    $model->bel_price=\app\models\IkeaItem::calcBelPrice($model->rus_price, $rate_additional, $rate_rus);
                    if ($model->validate()) {
                      if ($model->save()) {
                        $catItem=CategoryItem::findOne(['item_id'=>$model->id, 'category_id'=>$cat['id']]);
                        if (!$catItem) {
                          $catItem=new CategoryItem();
                          $catItem->item_id=$model->id;
                          $catItem->category_id=$cat['id'];
                          if ($catItem->save()) {
                            $i++;
                          }
                        }
                      } else {
                        var_dump($model->getErrors());
                      }
                    } else {
                      var_dump($model->getErrors());
                    }
                  }
                } else {

                }
              } else {
                //var_dump(gettype($product));
              }
            }
          }
        }
      }
    }
    $arr=array();
    $arr['success']=$i;
    $arr['attempts']=$f;
    print_r(json_encode($arr));
    die();
  }


}
