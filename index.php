<?php

// comment out the following two lines when deployed to production
//defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');



if (strpos($_SERVER['HTTP_HOST'], 'i2u.by') !== false || strpos($_SERVER['HTTP_HOST'], '188.166.5.117') !== false) {
    $config = require(__DIR__ . '/config/prod.php');
    defined('YII_DEBUG') or define('YII_DEBUG', false);
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
    defined('YII_ENV_DEV') or define('YII_ENV_DEV', true);
    
    $config = require(__DIR__ . '/config/web.php');
    
}
require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');

(new yii\web\Application($config))->run();
