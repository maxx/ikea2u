<?php

namespace app\models;

use Yii;
use yii\base\Model;

use yii\helpers\VarDumper;

class IkeaParsedItem extends Model {

    public $number; //ikea part number
    public $title;
    public $short_description;
    public $metric;
    public $middle_description;
    public $rus_price_total;
    public $bel_price_total;
    public $package_quantity;
    public $small_img_src;
    public $image_src;
    public $ikea_url_slug;

    

    public function getPrice() {
        return $this->bel_price_total;
    }

    public function getId() {
        return $this->number;
    }

    public function __construct() {

    }


}
