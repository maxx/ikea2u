<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class DeliveryForm extends Model {

    public $name;
    public $phone;
    public $email;
    public $address;
    public $note;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['name', 'phone', 'address'], 'required'],
            ['email', 'email'],
            [['address', 'note'], 'filter', 'filter' => 'trim'],
        ];
    }
    
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'name' => 'Имя и Фамилия',
            'phone' => 'Телефон',
            'email' => 'Email',
            'address' => 'Адрес доставки',
            'note' => 'Комментарии к заказу',
        ];
    }    

}
