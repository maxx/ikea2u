<?php

namespace app\models;

use Yii;
use yz\shoppingcart\CartPositionInterface;
/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $number
 * @property string $bel_price
 * @property string $rus_price
 * @property string $price_span
 * @property string $image_uri
 * @property string $image_json
 * @property string $title
 * @property string $short_description
 * @property string $full_description
 * @property string $created_at
 * @property string $updated_at
 * @property string $ikea_url_slug
 * @property string $metric
 * @property integer $qty_in_pack
 * @property string $key_features
 * @property string $package_info
 * @property string $care_inst
 * @property integer is_manually_created
 * @property integer in_stock
 * @property integer stock_price
 * @property integer fixed_bel_price
 *
 * @property CategoryItem[] $categoryItems
 * @property Category[] $categories
 * @property OrderItem[] $orderItems
 */
use yii\data\ActiveDataProvider;

class Item extends \yii\db\ActiveRecord implements CartPositionInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'item';
    }

    use \yz\shoppingcart\CartPositionTrait;

    public function getPrice($type = 'default') {
        $price = 0;
        if ($type === 'in_stock') {
            $price =  $this->stock_price;
        } else{
          $price=$this->fixed_bel_price ? $this->fixed_bel_price : $this->bel_price;
        }
        //return Utilities::currencyDenom($price);
        return $price;
    }

    public function getId() {
        return $this->id;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['bel_price', 'rus_price', 'stock_price', 'fixed_bel_price'], 'number'],
            [['short_description', 'full_description', 'metric', 'key_features', 'package_info',
                'care_inst', 'price_span'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['qty_in_pack', 'is_manually_created', 'in_stock'], 'integer'],
            [['number', 'image_uri', 'title', 'ikea_url_slug', 'price_span'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['ikea_url_slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => 'Артикул',
            'bel_price' => 'Bel Price',
            'rus_price' => 'Rus Price',
            'image_uri' => 'Image Uri',
            'title' => 'Название',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'ikea_url_slug' => 'Ikea Url Slug',
            'metric' => 'Metric',
            'qty_in_pack' => 'Qty In Pack',
            'key_features' => 'Key Features',
            'package_info' => 'Package Info',
            'care_inst' => 'Care Inst',
            'is_manually_created' => 'Создан вручную',
            'in_stock' => 'В наличии',
            'stock_price' => 'Цена товара в наличии',
            'fixed_bel_price'=>'Фиксированная цена в бел. рублях (Высший приоритет)'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryItems() {
        return $this->hasMany(CategoryItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_item', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems() {
        return $this->hasMany(OrderItem::className(), ['item_id' => 'id']);
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
        }
        $this->updated_at = date('Y-m-d H:i:s');

        $this->fixed_bel_price = round(ceil($this->fixed_bel_price), -3);
        $this->bel_price = round(ceil($this->bel_price), -3);
        $this->in_stock = $this->in_stock>0?$this->in_stock:null;
        

        return parent::beforeSave($insert);
    }
    
    public function search($params) {
        $query = Item::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */
        $dataProvider->setSort([
            'attributes' => [
                'number',
                'title',
            ]
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'number', $this->number]);
        $query->andFilterWhere(['like', 'bel_price', $this->bel_price]);
        $query->andFilterWhere(['like', 'rus_price', $this->rus_price]);
        $query->andFilterWhere(['like', 'created_at', $this->created_at]);
        $query->andFilterWhere(['like', 'updated_at', $this->updated_at]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'short_description', $this->short_description]);

        if ($this->in_stock > 0) {
            $query->andFilterWhere(['>', 'in_stock', 0]);
        } elseif ($this->in_stock === ' ') {

            $query->andWhere(['in_stock' => NULL]);
        }



        return $dataProvider;
    }

}
