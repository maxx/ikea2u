<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_item".
 *
 * @property string $item_number
 * @property integer $order_id
 * @property integer $qty
 * @property string $bel_price_due
 * @property string $rus_price_due
 * @property string $bel_price_per_item
 * @property string $rus_price_per_item
 *
 * @property Item $itemNumber
 * @property Order $order
 */
class OrderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_number', 'order_id'], 'required'],
            [['order_id', 'qty'], 'integer'],
            [['bel_price_due', 'rus_price_due', 'bel_price_per_item', 'rus_price_per_item'], 'number'],
            [['item_number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_number' => 'Item Number',
            'order_id' => 'Order ID',
            'qty' => 'Qty',
            'bel_price_due' => 'Bel Price Due',
            'rus_price_due' => 'Rus Price Due',
            'bel_price_per_item' => 'Bel Price Per Item',
            'rus_price_per_item' => 'Rus Price Per Item',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNumber()
    {
        return $this->hasOne(Item::className(), ['number' => 'item_number']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
