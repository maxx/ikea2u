<?php

namespace app\models;

use Yii;
use DiDom\Document;
use app\models\IkeaItem;

class IkeaItemService
{

  public static function getData($url)
  {
    $proxy='104.131.65.115:1337'; //free anonymous proxy http://proxylist.hidemyass.com/
    $res=array('data'=>'', 'httpCode'=>null);
    $ch=curl_init();
    $timeout=5;
    curl_setopt($ch, CURLOPT_URL, $url);
    //curl_setopt($ch, CURLOPT_PROXY, $proxy);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_HEADER, 1);

//        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

    $res['data']=curl_exec($ch);
    $res['httpCode']=curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    return $res;
  }

  /**
   * $knownUrl - когда мы точно знаем URL товара
   * **/
  public static function getHtmlByItemNumberOrLink($ikea_number_or_url, $knownUrl=null)
  {
    $url=$ikea_number_or_url;
    if ($knownUrl) {
      $str=IkeaItemService::getData($knownUrl);
    } else {
      if (filter_var($url, FILTER_VALIDATE_URL)) {
        if (strpos($url, 'ikea.com/ru/ru/p')) {
          $str=IkeaItemService::getData($url);
        } else {
          //$this->addError('ikea_number_or_url', 'Ссылка некорректная');
        }
      } else {
        /* check if it is part number */
        $valid=preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,2}\z/', $url);

        if ($valid) {
          $itemPartNumber=str_replace('.', '', $url) . '/';
          //$defaultUrl='http://www.ikea.com/ru/ru/catalog/products/';
          $defaultUrl='https://www.ikea.com/ru/ru/p/-';
          $url=$defaultUrl . $itemPartNumber;
          $str=IkeaItemService::getData($url);
          $is404=strpos($str['data'], '404 Not Found');
          if ($is404 !== false) {
            $defaultUrl='https://www.ikea.com/ru/ru/p/-s';
            $url=$defaultUrl . $itemPartNumber;
            $str=IkeaItemService::getData($url);
          }

          $is301=strpos($str['data'], '301 Moved Permanently');
          if ($is301 !== false) {
            $headers=get_headers($url);
            $location=IkeaItemService::findLocation($headers);
            $str=IkeaItemService::getData($location);
            $is301=strpos($str['data'], '301 Moved Permanently');
            $is307=strpos($str['data'], '307 Temporary Redirect');

            if ($is301 !== false) {
              $headers=get_headers($location);
              $location=IkeaItemService::findLocation($headers);
              $str=IkeaItemService::getData($location);
            }
            if ($is307 !== false) {
              $url=$defaultUrl . 'S' . $itemPartNumber;
              $headers=get_headers($url);
              $location=IkeaItemService::findLocation($headers);
              $str=IkeaItemService::getData($location);
              $is301=strpos($str['data'], '301 Moved Permanently');
              if ($is301 !== false) {
                $headers=get_headers($location);
                $location=IkeaItemService::findLocation($headers);
                $str=IkeaItemService::getData($location);
              }

            }

          }

          $is301=strpos($str['data'], '301 Moved Permanently');

          if ($str['httpCode'] === 404 || ($is301 !== false) || $url == 'https://www.ikea.com/ru/ru/') {

            $url=$defaultUrl . 'S' . $itemPartNumber;
            $headers=get_headers($url);

            $location=IkeaItemService::findLocation($headers);
            $str=IkeaItemService::getData($location);
          }
        } else {
          return false;
        }
      }

    }
    if ($str['data'] && ($str['httpCode'] === 200 || $str['httpCode'] === 301)) {
      return $str['data'];
    }
    return false;
  }

  public static function parseItemFromIkeaItemHtml($html, $clean=true)
  {

    $metaTags=[];
    if ($html) {
      $document=new Document();
      $document->loadHTML($html);
      foreach ($document->find('meta') as $meta) {
        /** @var Element $meta */
        $name=strtolower($meta->attr('name'));
        $property=strtolower($meta->attr('property'));
        $itemprop=strtolower($meta->attr('itemprop'));
        $content=$meta->attr('content');
        $metaTags[$name]=$content;
        $metaTags[$property]=$content;
        $metaTags[$itemprop]=$content;
      }

      $price_span=$document->find('.js-price-package.range-revamp-pip-price-package .range-revamp-price__unit');
      if (!empty($price_span[0]))
        $price_span=str_replace('/', '', $price_span[0]->text());
      if ($price_span)
        $metaTags['priceSpan']=$price_span;


      foreach ($document->find('script[data-type]') as $item) {
        $text=$item->text();
        $foo=Utilities::getStringBetween($text, '{', '}');
        $jsonStr='{' . $foo . '}';
        $json=json_decode($jsonStr);
        /*if (isset($json->product_names[0])){
          if (!$metaTags[$name])
          var_dump($json->product_names[]);
          die();

        }*/

        if (isset($json->category)) {
          $metaTags['categoryId']=$json->category;
        }
        /*  if (isset($json->product_variant)) {
            $metaTags['productVariant']=html_entity_decode($json->product_variant);
          }*/

      }

      foreach ($document->find('script[type=application/ld+json]') as $item) {

        $text=$item->text();
        if (strpos($text, '"@type":"Product"') !== false) {


          $foo=Utilities::getStringBetween($text, '"description":', '. ');

          $name=trim(str_replace('"', '', $foo));
          if ($name && empty($metaTags['short_description'])) {
            $name=html_entity_decode($name);
            //удаляем первых 2 слова т.к. VARDAGEN ВАРДАГЕН Банка с крышкой - прозрачное стекло 1.9
            $string=explode(' ', $name, 3);
            $name=!empty($string[2]) ? $string[2] : $name;
            $string=explode('.,', $name, 2);
            $name=!empty($string[0]) ? $string[0] : $name;

            $metaTags['short_description']=$name;
          }

          $foo=Utilities::getStringBetween($text, 'sku":', ',');
          $sku=trim(str_replace('"', '', $foo));
          if ($sku && empty($metaTags['productid'])) {
            $metaTags['productid']=$sku;
          }

          $foo=Utilities::getStringBetween($text, '"price":', ',');
          $price=trim(str_replace('"', '', $foo));
          if ($price && empty($metaTags['price'])) {
            $metaTags['price']=$price;
          }

          $foo=Utilities::getStringBetween($text, '"image":', '],');
          $images=trim(str_replace(['"', "["], '', $foo));
          if ($images && empty($metaTags['images'])) {
            $metaTags['images']=$images;
          }


          /*   $foo=Utilities::getStringBetween($text, ' "image": [', ',');
             $img=trim(str_replace('"', '', $foo));
             if ($img && empty($metaTags['image'])) {
               $metaTags['image']=$img;
             }*/
        }
      }

      if ($clean)
        $metaTags=self::cleanValues($metaTags);

      return $metaTags;
    }
    return false;
  }

  public
  static function cleanValues($data=[])
  {
    foreach ($data as &$value) {
      $value=str_replace(Yii::$app->params['domain'], '', $value);
      $value=str_replace('IKEA -', '', $value);
      $value=str_replace('- IKEA', '', $value);
      $value=str_replace('купить в интернет-магазине', '', $value);
      $value=str_replace(' Доставка по всей России.', '', $value);
      $value=str_replace('по выгодной цене в интернет-магазине', '', $value);
      $value=str_replace(' в интернет-магазине', '', $value);
      $value=str_replace('Купить ', '', $value);
      $value=str_replace(' купить онлайн', '', $value);

      $value=str_replace(' .', '.', $value);
      $value=trim($value);
    }

    return $data;
  }

  public static function createModel($values=[], $additionalAttributes=[])
  {
    $model=\app\models\Item::findOne(['number'=>$values['productid']]);
    if (!$model) {
      $model=new Item();
    }

    $model->number=$values['productid'];
    $model->ikea_url_slug=$values['og:url'];
    //пытаемя получить название
    //'keywords' => string 'MARREN МАРРЕН, Стол для компьютера' (length=57)
    $title=explode(' ', trim($values['keywords']));
    if (!empty($title[1]))
      $title=trim(str_replace(',', '', $title[1]));
    else
      $title='';
    if (!$title)
      $title=trim($values['og:title']);
    $model->title=$title;
    /*$title=explode(' ', trim($values['og:title']));
    if (!empty($title[0] && $title[1])){
      $model->title=$title[0].' '.$title[1];
    }else{
      $model->title=$title[0];
    }*/


    $model->rus_price=$values['price'];

    if (!empty($additionalAttributes['rate_rus_rubl']) && !empty($additionalAttributes['additional_rate'])) {
      $rate_rus=$additionalAttributes['rate_rus_rubl'];
      $rate_additional=$additionalAttributes['additional_rate'];
      $model->bel_price=\app\models\IkeaItem::calcBelPrice($model->rus_price, $rate_additional, $rate_rus);
    }

    $model->price_span=!empty($values['priceSpan']) ? $values['priceSpan'] : null;
    $model->image_uri=!empty($values['og:image']) ? $values['og:image'] : '';
    $model->short_description=$values['short_description'];
    if (!empty($values['productVariant'])) {
      if (strstr($model->short_description, $values['productVariant']) === false)
        $model->short_description.=', ' . $values['productVariant'];
    }
    if (!empty($values['images'])) {
      $arr=explode(',', $values['images']);
      if (!empty($arr)) {
        $json=json_encode($arr);
        $model->image_json=$json;
      }
    }
    $model->full_description=$values['description'];
    // $model->qty_in_pack = 0; //нужно доработать, т.к. в мета информации по ним нет
    // $model->metric = ''; //нужно доработать, т.к. в мета информации по ним нет
    $model->updated_at=date('Y-m-d H:i:s');

    return $model;
  }

  public
  static function createOrUpdateFromValues($values=[])
  {

    $model=\app\models\Item::findOne(['ikea_url_slug'=>$values['productid']]);
    if (!$model) {
      $model=new Item();
    }

    $model->number=$values['productid'];
    $model->ikea_url_slug=$values['og:url'];
    $title=explode(' ', trim($values['og:title']));
    $model->title=$title[0];


    //$model->bel_price = $ikeaItemModel->bel_price_total;
    $model->rus_price=$values['price'];
    $model->image_uri=$values['image'];
    $model->short_description=$values['short_description'];
    $model->full_description=$values['description'];
    $model->qty_in_pack=0; //нужно доработать, т.к. в мета информации по ним нет
    $model->metric=''; //нужно доработать, т.к. в мета информации по ним нет
    $model->updated_at=date('Y-m-d H:i:s');

    var_dump($values, $model);
    die();

    return $model->save();
  }

  public
  static function findLocation($data)
  {
    $location=false;
    if (!empty($data)) {
      foreach ($data as $key=>$str) {
        if (strpos($str, 'Location') !== false) {
          $location=trim(str_replace('Location:', '', $str));
          break;
        }
      }
    }

    return $location;
  }

}
