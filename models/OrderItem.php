<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $item_id
 * @property string $number
 * @property string $ikea_url_slug
 * @property string $short_description
 * @property string $title
 * @property integer $qty
 * @property string $bel_price
 * @property string $total_bel_price
 * @property string $rus_price
 * @property string $total_rus_price
 *
 * @property Item $item
 * @property Order $order
 */
class OrderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'item_id', 'qty'], 'integer'],
            [['number', 'title', 'ikea_url_slug'], 'string', 'max' => 255],
            [['short_description'], 'string'],
            [['bel_price', 'total_bel_price', 'rus_price', 'total_rus_price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'item_id' => 'Item ID',
            'qty' => 'Qty',
            'bel_price' => 'Bel Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
