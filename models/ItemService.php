<?php

namespace app\models;

use app\Entity\IkeaProductDataFromSearch;
use app\models\IkeaItemService;
use Yii;
use DiDom\Document;

class ItemService
{

  public static function createOrUpdateFromIkeaSearch($ikea_number_or_url, $settings, $knownUrl=null)
  {
    $html=IkeaItemService::getHtmlByItemNumberOrLink($ikea_number_or_url, $knownUrl);

    if ($html) {
      $values=IkeaItemService::parseItemFromIkeaItemHtml($html);
      if (!empty($values['productid'])) {
        $rate=$settings;

        $model=IkeaItemService::createModel($values, $rate);

        if ($model->save()) {
          if (!empty($values['categoryId'])) {
            $cid=$values['categoryId'];
            $cat=Category::find()->filterWhere(
                ['LIKE', 'ikea_url_slug', "$cid"]
            )->one();
            if ($cat) {
              $catItem=CategoryItem::findOne(['item_id'=>$model->id, 'category_id'=>$cat->id]);
              if (!$catItem) {
                $catItem=new CategoryItem();
                $catItem->item_id=$model->id;
                $catItem->category_id=$cat['id'];
                if ($catItem->save()) {

                }
              }
            }
          }
          return TRUE;
        } else {
          var_dump($model->getErrors());

          return FALSE;
        }

      }
    }

    return;
  }

  public static function createOrUpdateFromIkea($ikea_number_or_url)
  {
    $html=IkeaItemService::getHtmlByItemNumberOrLink($ikea_number_or_url);

    if ($html) {
      $values=IkeaItemService::parseItemFromIkeaItemHtml($html);
      if (!empty($values['productid'])) {
        $rate=\app\models\Setting::find()->asArray()->one();

        $model=IkeaItemService::createModel($values, $rate);

        if ($model->save()) {
          if (!empty($values['categoryId'])) {
            $cid=$values['categoryId'];
            $cat=Category::find()->filterWhere(
                ['LIKE', 'ikea_url_slug', "$cid"]
            )->one();
            if ($cat) {
              $catItem=CategoryItem::findOne(['item_id'=>$model->id, 'category_id'=>$cat->id]);
              if (!$catItem) {
                $catItem=new CategoryItem();
                $catItem->item_id=$model->id;
                $catItem->category_id=$cat['id'];
                if ($catItem->save()) {

                }
              }
            }
          }
          return TRUE;
        } else {
          var_dump($model->getErrors());

          return FALSE;
        }

      }
    }

    return;
  }

  public static function createOrUpdateFromIkeaByString($str)
  {

    //$url = Yii::$app->params['domain'] . '/ru/ru/search/products/?q=' . $str;
    $url='https://sik.search.blue.cdtapps.com/ru/ru/search-result-page?q=' . $str;

//  https://sik.search.blue.cdtapps.com/ru/ru/search-result-page?max-num-filters=8&q=%D0%BC%D0%B0%D0%BB%D1%8C%D0%BC

    $curl=curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);

    $html=curl_exec($curl);
    $info=curl_getinfo($curl);

    curl_close($curl);
    $document=new Document();

    if ($html) {
      $res=json_decode($html, true);
      $settings=\app\models\Setting::find()->asArray()->one();

      if (!empty($res['searchResultPage']['products']['main']['items'])) {
        $products=$res['searchResultPage']['products']['main']['items'];

        foreach ($products as $product) {
          if (!empty($product['product'])) {
            $p=$product['product'];

            //TODO finish OOP
            //$productEntity=new IkeaProductDataFromSearch($p);

            $number=\app\models\Utilities::calculateIkeaItemNumber($p['id']);
            self::createOrUpdateFromIkeaSearch($number, $settings, $p['pipUrl']);
          }
        }
      }
    }
  }

}
