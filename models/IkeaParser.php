<?php

namespace app\models;

use Yii;
use yii\base\Model;

class IkeaParser extends Model {

    public $endpoint = 'http://www.ikea.com';
    public $categoriesUrl = 'http://www.ikea.com/ru/catalog/allproducts/department/'; //should be generated base on  endpoint
    protected $_context;

    public function __construct() {
        $opts = array('http' => array('header' => 'Accept-Charset: UTF-8, *;q=0'));
        $this->_context = stream_context_create($opts);
    }

    public function getCategories() {
        $results = array();

        $html = file_get_contents($this->categoriesUrl, false, $this->_context);

        $dom = new \DOMDocument();
        @ $dom->loadHTML($html);

        $xp = new \DOMXPath($dom);
        $tags = $xp->query('//div[contains(@class, "productCategoryContainer ")]');
        //$tags = $dom->getElementsByTagName('a');

        $k = 0;
        foreach ($tags as $tag) {
            $headers = $tag->getElementsByTagName('span'); //1 header
            $main_category['title'] = '';
            $main_category['url'] = '';
            $main_category['subcategories'] = array();

            foreach ($headers as $header) {
                $header_class = $header->getAttribute('class');
                if (strpos($header_class, "header") !== false) {
                    $main_category['title'] = trim($header->nodeValue);
                }
            }

            $links = $tag->getElementsByTagName('a'); //1 header
            $m = 0;
            foreach ($links as $link) {
                $url = $link->getAttribute('href');
                $id = $link->getAttribute('id');

                if (strpos($id, "txt") !== false) {
                    $main_category['subcategories'][$m]['url'] = $url;
                    $main_category['subcategories'][$m]['title'] = trim($link->nodeValue);
                    $main_category['subcategories'][$m]['subcategories'] = $this->getSubCategories($url);
                    $m++;
                }
            }

            $x = rtrim($url, "/");
            $main_category['url'] = (substr($x, 0, strrpos($x, "/")));
            $results[$k] = $main_category;
            $k++;
        }


        return $results;
    }

    public function getSubCategories($url) {
        $html = file_get_contents($this->endpoint . $url, false, $this->_context);
        $dom = new \DOMDocument();
        @ $dom->loadHTML($html);

        $xp = new \DOMXPath($dom);
        //$tags = $xp->query('//a[@class="orangeBtn"]');
        //$tags = $xp->query('//div[@class="productsFilterChapters"]//a');
        $tags = $xp->query('//div[@class="departmentLinks"]//a'); //див вверху страницы с сылками на категории
        //$tags = $dom->getElementsByTagName('a');
        $res = array();
        $i = 0;
        foreach ($tags as $link) {
            $href = $link->getAttribute('href');
            if ($href !== $url) {
                $res[$i]['url'] = $href;
                $res[$i]['title'] = trim($link->nodeValue);
                $i++;
            }
        }
        return $res;
    }

    public function getItemsByUrl($url) {

        $url = $this->endpoint . $url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);

        $html = curl_exec($curl);
        $info = curl_getinfo($curl);

        curl_close($curl);
        if ($info['http_code'] !== 200)
            return array();

        //    $html = file_get_contents($this->endpoint . $url, false, $this->_context);
        $dom = new \DOMDocument();
        @ $dom->loadHTML($html);

        $xp = new \DOMXPath($dom);
        //$tags = $xp->query('//a[@class="orangeBtn"]');
        $tags = $xp->query('//a[@class="productLink"]');
        $res = array();
        $i = 0;
        foreach ($tags as $link) {
            $href = $link->getAttribute('href');
            $res[$i]['url'] = $href;
            $i++;
        }

        return $res;
    }

    public function getItemByUrl($url) {
        $html = file_get_contents($this->endpoint . $url, false, $this->_context);
        $dom = new \DOMDocument();
        @ $dom->loadHTML($html);

        $xp = new \DOMXPath($dom);
        //$tags = $xp->query('//a[@class="orangeBtn"]');
        $tags = $xp->query('//a[@class="productLink"]');
        $res = array();
        $i = 0;
        foreach ($tags as $link) {
            $href = $link->getAttribute('href');
            $res[$i]['url'] = $href;
            $i++;
        }
        return $res;
    }

    public function getItem($id) {
        $site = 'http://www.ikea.com/ru/ru/search/?query=' . $id;
        $document = new Document($site, true);

        $pageTitleObj = $document->find('title');
        $pageTitle = isset($pageTitleObj[0]) ? $pageTitleObj[0]->text() : null;
        if (!$pageTitle || (strpos($pageTitle, 'Technical Error') === false)) {

            $titleObj = $document->find('#name');
            $title = isset($titleObj[0]) ? $titleObj[0]->text() : null;
            $shortDescObj = $document->find('#rightNavInfoDiv #type');
            $shortDesc = isset($shortDescObj[0]) ? $shortDescObj[0]->text() : null;

            $fullDescObj = $document->find('#salesArg');
            $fullDesc = isset($fullDescObj[0]) ? $fullDescObj[0]->text() : null;

            $metricObj = $document->find('#metric');
            $metric = isset($metricObj[0]) ? $metricObj[0]->text() : null;

            $qtyInPackObj = $document->find('#storeformatpieces');
            $qtyInPack = isset($qtyInPackObj[0]) ? $qtyInPackObj[0]->text() : null;

            $imgObj = $document->find('#productImg');
            $imgSrc = isset($imgObj[0]) ? $imgObj[0]->attr('src') : null;

            $divPrice = $document->find('#price1');
            $priceText = isset($divPrice[0]) ? $divPrice[0]->text() : null;

            $itemNumberObj = $document->find('#rightNavInfoDiv #itemNumber');
            $itemNumber = isset($itemNumberObj[0]) ? $itemNumberObj[0]->text() : null;

            if ($priceText && $title) {
                $price = substr($priceText, 0, strpos($priceText, ".–"));
                $price = filter_var($priceText, FILTER_SANITIZE_NUMBER_INT);
                $item->rus_price = $price;
                $item->bel_price = \app\models\IkeaItem::calcBelPrice($price, $rate_additional, $rate_rus);
                $item->title = trim($title);
                $item->image_uri = $imgSrc;
                $item->short_description = trim($shortDesc);
                $item->full_description = trim(str_replace('Подробнее', '', $fullDesc));
                $item->metric = trim($metric);
                $item->qty_in_pack = ((int) $qtyInPack > 0) ? $qtyInPack : 1;
                $item->updated_at = date('Y-m-d H:i:s');

                if ($itemNumber) {
                    $item->number = $itemNumber;
                }

                if (!$item->save()) {
                    var_dump($item->getErrors());
                } else {
                    $i++;
                }
            } else {
                var_dump($item->validate());
                var_dump($item->getErrors());
                var_dump($item->attributes);
                //$item->ikea_url_slug = null;
                //$item->save();
                /* if ($item->delete()) {
                  $deleted++;
                  } */
            }
        } else {
            $ikeaPageBlock++;
        }
    }

}
