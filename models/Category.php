<?php

namespace app\models;

use Yii;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $ikea_url_slug
 * @property integer $order
 * @property string $main_image_src
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CategoryItem[] $categoryItems
 * @property Item[] $items
 */
class Category extends \yii\db\ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'category';
  }

  public function behaviors()
  {
    return [
        'tree'=>[
            'class'=>NestedSetsBehavior::className(),
          // 'treeAttribute' => 'tree',
          // 'leftAttribute' => 'lft',
          // 'rightAttribute' => 'rgt',
          // 'depthAttribute' => 'depth',
        ],
    ];
  }

  public function transactions()
  {
    return [
        self::SCENARIO_DEFAULT=>self::OP_ALL,
    ];
  }

  public static function find()
  {
    return new CategoryQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
        [['lft', 'rgt', 'depth', 'order'], 'integer'],
        [['created_at', 'updated_at'], 'safe'],
        [['title', 'slug', 'ikea_url_slug', 'main_image_src'], 'string', 'max'=>255],
        [['slug'], 'unique'],
        [['ikea_url_slug'], 'unique']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
        'id'=>'ID',
        'title'=>'Title',
        'slug'=>'Slug',
        'lft'=>'Lft',
        'rgt'=>'Rgt',
        'depth'=>'Depth',
        'ikea_url_slug'=>'Ikea Url Slug',
        'order'=>'Order',
        'created_at'=>'Created At',
        'updated_at'=>'Updated At',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategoryItems()
  {
    return $this->hasMany(CategoryItem::className(), ['category_id'=>'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getItems()
  {
    return $this->hasMany(Item::className(), ['id'=>'item_id'])->viaTable('category_item', ['category_id'=>'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getReadyItems($categories)
  {
    return $this->hasMany(Item::className(), ['id'=>'item_id'])
        ->where('bel_price > 0')->viaTable('category_item', ['category_id'=>'id']);
  }

  public function getReadyItemsNEW($categories, $id)
  {
    $catsIds[]=$id;
    foreach ($categories as $category) {
      $catsIds[]=$category->id;
    }

    $res=CategoryItem::find()
        ->select(['item_id'])
        ->where(['in', 'category_item.category_id', $catsIds])
        ->asArray()
        ->all();
    $products=array_unique(array_column($res, 'item_id'));
    $res=Item::find()
        ->where('bel_price > 0')
        ->where(['in', 'id', $products]);

    return $res;

    return $this->hasMany(Item::className(), ['id'=>'item_id'])
        ->where('bel_price > 0')
        ->where(['in', 'id', $products]);

    /*
        return $this->hasMany(Item::className(), ['id'=>'item_id'])
            ->where('bel_price > 0')

            //->whereIn('category_id', $catsIds)//
            ->viaTable('category_item', ['category_id'=>'id'], function ($query) use ($catsIds) {
              $query->where(['in', 'category_item.category_id', $catsIds]);
            });*/
  }

  public function getReadyItemsByCatIds($ids)
  {
    return $this->hasMany(Item::className(), ['id'=>'item_id'])
        ->where('bel_price > 0')->viaTable('category_item', ['category_id'=>'id']);
    $this->andWhere(['like', 'title', $this->title]);
  }

  public function beforeSave($insert)
  {
    if ($this->isNewRecord) {
      $this->created_at=date('Y-m-d H:i:s');
    }

    $this->updated_at=date('Y-m-d H:i:s');


    return parent::beforeSave($insert);
  }

}
