<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "item_stock".
 *
 * @property int $id
 * @property string $number
 * @property string $price
 * @property string $image_uri
 * @property string $title
 * @property string $short_description
 * @property string $full_description
 * @property string $created_at
 * @property string $updated_at
 * @property string $ikea_url_slug
 * @property string $metric
 * @property int $qty_in_pack
 * @property string $key_features
 * @property string $package_info
 * @property string $care_inst
 * @property int $in_stock
 * @property string $filter_price
 */
class ItemStock extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'item_stock';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            //[['price'], 'number'],
            [[ 'qty_in_pack', 'in_stock'], 'integer'],
            [['price'], 'string'],
            [['short_description', 'full_description', 'metric', 'key_features', 'package_info', 'care_inst'], 'string'],
            [['created_at', 'updated_at', 'parsed_at'], 'safe'],
            [['ikea_url_slug'], 'required'],
            [['number', 'image_uri', 'title', 'ikea_url_slug'], 'string', 'max' => 255],
            [['filter_price'], 'string', 'max' => 10],
            //[['number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => 'Артикул',
            'price' => 'Цена',
            'image_uri' => 'Image Uri',
            'title' => 'Название',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'ikea_url_slug' => 'Ikea Url Slug',
            'metric' => 'Metric',
            'qty_in_pack' => 'Qty In Pack',
            'key_features' => 'Key Features',
            'package_info' => 'Package Info',
            'care_inst' => 'Care Inst',
            'in_stock' => 'В наличии',
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
        }
        $this->updated_at = date('Y-m-d H:i:s');

        // $this->price = round(ceil($this->price), -3);
        $this->in_stock = $this->in_stock > 0 ? $this->in_stock : null;


        return parent::beforeSave($insert);
    }
    
     public function getPrice($type = 'default') {
        return $this->price;
    }

    public function search($params) {
        $query = ItemStock::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */
        $dataProvider->setSort([
            'attributes' => [
                'number',
                'title',
            ]
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'number', $this->number]);
        $query->andFilterWhere(['like', 'price', $this->price]);
        $query->andFilterWhere(['like', 'created_at', $this->created_at]);
        $query->andFilterWhere(['like', 'updated_at', $this->updated_at]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'short_description', $this->short_description]);

        if ($this->in_stock > 0) {
            $query->andFilterWhere(['>', 'in_stock', 0]);
        } elseif ($this->in_stock === ' ') {

            $query->andWhere(['in_stock' => NULL]);
        }



        return $dataProvider;
    }

}
