<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property string $name
 * @property string $title
 * @property string $body
 * @property string $seo_title
 * @property string $seo_description
 * @property integer $user_id
 *
 * @property User $user
 */
class Page extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['body', 'seo_description'], 'string'],
            [['user_id'], 'integer'],
            [['name', 'title', 'seo_title'], 'string', 'max' => 255],
            [['name'], 'unique'],
            ['name', 'match', 'pattern' => "/^[a-zA-Z0-9\-]+$/"]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Name',
            'title' => 'Title',
            'body' => 'Текст страницы',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }

}
