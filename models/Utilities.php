<?php

namespace app\models;

use Yii;

class Utilities
{

  public static function priceSpan($str='')
  {
    return $str ? '/' . $str : '';

  }

  public static function Currency($value, $addSymbol=false, $currencyCode='BYR')
  {

    //$str = \Yii::$app->formatter->asDecimal($value, 0, array('separator' => ' '));
    $str=number_format($value, 0, ' ', ' ');
    if ($addSymbol) {
      $str.=' б.р.';
    }

    /* $str = (int) $value;
      if ($addSymbol) {
      $str .= ' б.р.';
      } */

    return str_replace(',', ' ', $str);
  }

  public static function currencyDenom($value, $addSymbol=false, $sup=true)
  {
    if (strpos($value, '.') === false) {
      $value=($value / 10000);


      //$str = number_format(round($value, 1), 2);
      //$value = 1.5;
//            $str = round($value, 1);
      $str=number_format($value, 2);

      // $str = \Yii::$app->formatter->asDecimal($value, 2, array('separator' => '.'));
      $parts=explode('.', $str);
      $parts1=isset($parts[1]) ? $parts[1] : '0';
      if ($sup)
        $str=$parts[0] . '.<sup>' . $parts1 . '</sup>';
      else
        $str=$parts[0] . '.' . $parts1 . '';
      if ($addSymbol) {
        $str.=' б.р.';
      }
      return str_replace(',', ' ', $str);
    } else {
      return $value;
    }
  }

  public static function CurrencyRUR($value, $addSymbol=true, $currencyCode='RUR')
  {
    $str=\Yii::$app->formatter->asDecimal($value, 0);
    if ($addSymbol) {
      $str.=' рос. рубл.';
    }

    /* $str = (int) $value;
      if ($addSymbol) {
      $str .= ' б.р.';
      } */

    return str_replace(',', ' ', $str);
  }

  public static function calculateBelPrice($rusPrice, $customAdditionalRate, $rateRusRubl)
  {

    return ($rusPrice + ($rusPrice * $customAdditionalRate / 100)) * $rateRusRubl;
  }

  public static function calculateIkeaItemNumber($str)
  {
    $str=preg_replace("/[^0-9]/", "", $str);
    $str=implode(".", str_split($str, 3));
    return $str;
  }

  public static function cleanIkeaUrl($str)
  {
    $mainUrl='https://www.ikea.com';
    if (strpos($str, 'service.') !== false) {
      return NULL;
    }

    $str=str_replace($mainUrl, '', $str);
    //$str = '/ru/ru/cat/odeyala-20529/?itm_campaign=bedroom_page&itm_element=menu_block&itm_content=quilts';
    if (strpos($str, '/?') !== false) {
      $str=substr($str, 0, strpos($str, "?"));
    }
    return $str;
  }

  public static function imageUrl($url)
  {
    if (strpos($url, Yii::$app->params['domain']) !== false) {
      return $url;
    }
    return Yii::$app->params['domain'] . $url;
  }

  public static function ikeaUrl($url)
  {
    if (strpos($url, Yii::$app->params['domain']) !== false) {
      return $url;
    }
    return Yii::$app->params['domain'] . $url;
  }

  public static function getStringBetween($string, $start, $end)
  {
    $string=" " . $string;
    $ini=strpos($string, $start);
    if ($ini == 0)
      return "";
    $ini+=strlen($start);
    $len=strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }

  public static function forbiddenCategoriesWords()
  {
    $ar=[
        'еда',
        'Выпечка, десерты и печенье', 'выпечка', 'десерт', 'печенье', 'шоколад', 'ирис', 'обед',
        'Эргономичное рабочее место',
        'Соусы и джемы', 'cоус', 'джем',
        'Гарниры и соусы', 'гарнир', 'соус',
        'Товары, сокращающие потребление пластика',
        'Мебель из дерева',
        'Продукты из мяса', 'мясо',
        'Кулинарные книги', 'кулинар',
        'Хлеб и молочные продукты',
        'Напитки', 'напиток',
        'Рыбные продукты', 'рыбные',
        'Перекус с собой',
        'Овощи и гарниры',
    ];


    $res=array_map('mb_strtolower', $ar);

    return $res;
  }

  public static function isForbiddenCategoriesWord($value='')
  {
    if (!$value) return false;
    $words=Utilities::forbiddenCategoriesWords();
    if (in_array(mb_strtolower($value), $words)) {
      return true;
    }
    return false;

  }

}
