<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $rate_rus_rubl
 * @property string $additional_rate
 */
class Setting extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rate_rus_rubl', 'additional_rate'], 'required'],
            [['rate_rus_rubl', 'additional_rate'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'rate_rus_rubl' => 'Курс рос. рубля для сайта',
            'additional_rate' => '% доставки',
        ];
    }

    public function afterSave($insert, $changedAttributes) {


        $orders = Order::find()->where('status != :status', ['status' => Order::STATUS_COMPLETED])->all();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $order->custom_rate_rus_rubl = $this->rate_rus_rubl;
                $order->custom_additional_rate = $this->additional_rate;
                $order->save();
                Order::recalculateOrder($order->id);
            }
        }

        $sql = 'UPDATE `item` SET `bel_price`=(rus_price+(rus_price*'.$this->additional_rate.'/100))*'.$this->rate_rus_rubl;
        $x = \Yii::$app->db->createCommand($sql)->execute();
        $sql = 'UPDATE `item` SET `bel_price`=round(bel_price, -3)';
        $x = \Yii::$app->db->createCommand($sql)->execute();
                
        return parent::afterSave($insert, $changedAttributes);
    }

}
