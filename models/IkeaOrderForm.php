<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * IkeaOrderForm is the model behind the ikea product form.
 */
class IkeaOrderForm extends Model {

    public $ikea_number_or_url;
    private $data;
    public $url;
    public $itemModel;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [

            [['ikea_number_or_url'], 'required'],
            ['ikea_number_or_url', 'filter', 'filter' => 'trim'],
            //['ikea_number_or_url', 'maxx'],
            ['ikea_number_or_url', 'validateByIkea'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'ikea_number_or_url' => 'Артикул товара или ссылка',
        ];
    }

    public function validateByIkea($attribute, $params) {
        if (!$this->hasErrors()) {
            $str['data'] = null;
            $str['httpCode'] = null;
            $url = $this->ikea_number_or_url;
            /* check if it is url */
            
            if (filter_var($this->ikea_number_or_url, FILTER_VALIDATE_URL)) {
                if (strpos($this->ikea_number_or_url, 'ikea.com/ru/ru/p')) {
                    var_dump(121);die();
                    $str = IkeaItem::getData($url);
                    
                } else {
                    $this->addError('ikea_number_or_url', 'Ссылка некорректная');
                }
            } else {
                /* check if it is part number */
                $valid = preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,2}\z/', $this->ikea_number_or_url);
                if ($valid) {
                    $itemPartNumber = str_replace('.', '', $this->ikea_number_or_url) . '/';
                    $defaultUrl = 'http://www.ikea.com/ru/ru/catalog/products/';
                    $url = $defaultUrl . $itemPartNumber;
                    $str = IkeaItem::getData($url);
 
                    if ($str['httpCode'] === 404) {
                        $url = $defaultUrl . 'S' . $itemPartNumber;
                        $str = IkeaItem::getData($url);
                    }
                } else {
                    $this->addError('ikea_number_or_url', 'Артикул некорректный');
                }
            }
            
            $this->url = $url;


            if ($str['data'] && ($str['httpCode'] === 200 || $str['httpCode'] === 301)) {
                $this->data = $str['data'];
                $modelIkeaItem = new IkeaItem();
                $modelIkeaItem->letsGo($this->url);
                $this->itemModel = $modelIkeaItem;
                
            } else {
                $this->addError('ikea_number_or_url', 'Товар не найден');
            }
        }
    }

    public function getData() {
        return $this->data ? $this->data : null;
    }

}
