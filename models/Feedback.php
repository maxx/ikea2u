<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $title
 * @property string $message
 * @property string $name
 * @property string $place
 * @property integer $status
 * @property integer $opinion
 * @property string $answer
 * @property string $created_at
 * @property string $updated_at
 */
class Feedback extends \yii\db\ActiveRecord {

    const STATUS_ADDED = 1;
    const STATUS_CONFIRMED = 10;
    const STATUS_REJECTED = 20;
    const OPINION_POSITIVE = 1;
    const OPINION_NEGATIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['message', 'opinion'], 'required'],
            [['message', 'answer'], 'string'],
            [['status', 'opinion'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'name', 'place'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'message' => 'Отзыв',
            'name' => 'Имя',
            'place' => 'Место откуда отзыв',
            'status' => 'Статус',
            'opinion' => 'Мнение',
            'answer' => 'Ответ',
            'status' => 'Статус',
            'created_at' => 'Добавлен',
            'updated_at' => 'Обновлен',
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
        }

        $this->updated_at = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }

    public static function itemAlias($type, $code = null) {
        $_items = [
            'Status' => [
                self::STATUS_ADDED => 'Ожидает подтверждения',
                self::STATUS_CONFIRMED => 'Подтвержден',
                self::STATUS_REJECTED => 'Отклонен',
            ],
            'Opinion' => [
                self::OPINION_POSITIVE => 'Отзыв',
            //    '' => 'Позитивный отзыв',
//                null => 'Позитивный отзыв',
                self::OPINION_NEGATIVE => 'Отзыв',
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}
