<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $customer_name
 * @property string $customer_email
 * @property string $customer_phone
 * @property string $updated_at
 * @property string $created_at
 * @property string $total_due_bel
 * @property integer $status
 * @property string $note
 * @property string $address
 * @property string $custom_rate_rus_rubl
 * @property string $total_due_rus
 * @property string $custom_additional_rate
 *
 * @property User $user
 * @property OrderItem[] $orderItems
 */
class Order extends \yii\db\ActiveRecord {

    const STATUS_ADDED = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_COMPLETED = 10;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['total_due_bel', 'custom_rate_rus_rubl', 'total_due_rus', 'custom_additional_rate'], 'number'],
            [['note', 'address'], 'string'],
            [['customer_name'], 'string', 'max' => 255],
            [['customer_email'], 'string', 'max' => 45],
            [['customer_phone'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'customer_name' => 'Клиент',
            'customer_email' => 'Email клиента',
            'customer_phone' => 'Телефон клиента',
            'updated_at' => 'Обновлен',
            'created_at' => 'Создан',
            'total_due_bel' => 'Общая сумма в BYR',
            'status' => 'Статус',
            'note' => 'Примечание',
            'address' => 'Адрес доставки',
            'total_due_rus' => 'Общая сумма в RUS',
            'custom_rate_rus_rubl' => 'Курс рубля заказа',
            'custom_additional_rate' => '% доставки заказа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems() {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public static function getStatusesArray() {
        return [
            self::STATUS_ADDED => 'Ожидает подтверждения',
            self::STATUS_CONFIRMED => 'Подтвержден',
            self::STATUS_COMPLETED => 'Исполнен',
        ];
    }

    public static function itemAlias($type, $code = null) {
        $_items = [
            'Status' => [
                self::STATUS_ADDED => 'Ожидает подтверждения',
                self::STATUS_CONFIRMED => 'Подтвержден',
                self::STATUS_COMPLETED => 'Исполнен',
            ],
        ];

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');

            /* берем данные о курсе рубля и проценте доставки */
            $rate = Setting::find()->asArray()->one();
            $this->custom_rate_rus_rubl = $rate['rate_rus_rubl'];
            $this->custom_additional_rate = $rate['additional_rate'];
        }
        $this->updated_at = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }

   /* public static function recalculateOrder($id) {
        $order = Order::findOne(['id' => $id]);

        if ($order) {
            $toal_due_bel = 0;
            $toal_due_rus = 0;
            foreach ($order->orderItems as $i => $item) {
                //$toal_due_bel+=$item->item->bel_price;
                //$item->bel_price = ($item->item->rus_price +($item->item->rus_price*$order->custom_additional_rate/100))*$item->item->rus_price;
                $item->bel_price = Utilities::calculateBelPrice($item->rus_price, $order->custom_additional_rate, $item->rus_price);
                // $sql = 'UPDATE `item` SET `bel_price`=(rus_price+(rus_price*'.$this->additional_rate.'/100))*'.$this->rate_rus_rubl;
                $item->rus_price = $item->rus_price * $order->custom_rate_rus_rubl;
                $item->save();
                $toal_due_bel+=$item->bel_price;
                $toal_due_rus+=$item->rus_price;
            }
            $order->total_due_bel = $toal_due_bel;
            $order->total_due_rus = $toal_due_rus;
            $order->save();
        }
        return true;
    }*/
    
    
  /*  public static function recalculateOrderNew($id) { // использовать если цены приходят в деноменированых рублях
        $order = Order::findOne(['id' => $id]);

        if ($order) {
            $toal_due_bel = 0;
            $toal_due_rus = 0;
            foreach ($order->orderItems as $i => $item) {
                //$toal_due_bel+=$item->item->bel_price;
                //$item->bel_price = ($item->item->rus_price +($item->item->rus_price*$order->custom_additional_rate/100))*$item->item->rus_price;

                $bel_price = Utilities::calculateBelPrice($item->rus_price, $order->custom_additional_rate, $order->custom_rate_rus_rubl);
                 return ($rusPrice + ($rusPrice * $customAdditionalRate / 100)) * $rateRusRubl;
                $item->bel_price = (float) Utilities::currencyDenom($bel_price, false, false);
                // $sql = 'UPDATE `item` SET `bel_price`=(rus_price+(rus_price*'.$this->additional_rate.'/100))*'.$this->rate_rus_rubl;
                $item->total_bel_price = $item->bel_price*$item->qty;
                $item->total_rus_price = $item->rus_price*$item->qty;
                VarDumper::dump($bel_price, 10, true);
                VarDumper::dump($order, 10, true);
                VarDumper::dump($item, 10, true);
                die();

                $item->save();
                $toal_due_bel+=$item->total_bel_price;
                $toal_due_rus+=$item->total_rus_price;
            }
            $order->total_due_bel = $toal_due_bel;
            $order->total_due_rus = $toal_due_rus;
            $order->save();
        }
        return true;
    }*/

    public static function getOrdersSpecialDetails($status) {
        $res = Order::find()
                ->select(['COUNT(*) AS cnt, SUM(total_due_rus) as total_sum_rus'])
                ->where('status = ' . $status)
                ->asArray()
                ->all();
        if (!empty($res))
            return $res[0];
        else
            return ['cnt' => 0, 'total_sum_rus' => 0];
    }

}
