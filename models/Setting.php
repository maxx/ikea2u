<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property integer $rate_rus_rubl
 * @property integer $additional_rate
 * @property string $delivery_countdown
 */
class Setting extends \yii\db\ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'setting';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
        [['rate_rus_rubl', 'additional_rate'], 'required'],
        [['rate_rus_rubl', 'additional_rate'], 'integer'],
        [['delivery_countdown'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
        'id'=>'ID',
        'rate_rus_rubl'=>'Rate Rus Rubl',
        'additional_rate'=>'Additional Rate',
        'delivery_countdown'=>'Дата следующей доставки ( Final countdown )',
    ];
  }

  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $this->delivery_countdown=date('Y-m-d 19:00:00', strtotime($this->delivery_countdown));
      return TRUE;
    } else
      return false;
  }

  public function afterSave($insert, $changedAttributes)
  {
    set_time_limit(10000);
    ini_set('memory_limit', '2048M');
    $rate=\app\models\Setting::find()->asArray()->one();
    $rate_rus=$rate['rate_rus_rubl'];
    $rate_additional=$rate['additional_rate'];
    /*$items=\app\models\Item::find()
        ->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')
        ->all();

    foreach ($items as $item) {
      $item->bel_price=\app\models\IkeaItem::calcBelPrice($item->rus_price, $rate_additional, $rate_rus);
      $item->save();
    }
*/
	//($rusPrice + ($rusPrice * $customAdditionalRate / 100)) * $rateRusRubl;

        $sql = 'UPDATE `item` SET `bel_price`=(rus_price+(rus_price*'.$rate_additional.'/100))*'.$rate_rus;
        $x = \Yii::$app->db->createCommand($sql)->execute();
		
		$sql = 'UPDATE `item` SET `bel_price`=rus_price*(
		CASE 
WHEN bel_price<200000 THEN 1.5
WHEN bel_price<500000 THEN 1.4
WHEN bel_price<1500000 THEN 1.35
WHEN bel_price<3000000 THEN 1.3
WHEN bel_price<5000000 THEN 1.25
ELSE 1.2 END)*'.$rate_rus;
		//$price=$rus_price_total * $koef * $rate_rus;
		  $x = \Yii::$app->db->createCommand($sql)->execute();
		
        $sql = 'UPDATE `item` SET `bel_price`=round(bel_price, -3)';
        $x = \Yii::$app->db->createCommand($sql)->execute();
                
				
    return parent::afterSave($insert, $changedAttributes);
  }

}
