<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yz\shoppingcart\CartPositionInterface;
use yii\helpers\VarDumper;
use DiDom\Document;

class IkeaItem extends Model implements CartPositionInterface
{

  private $_rate_rus=null;
  private $_rate_additional=null;
  public $id=null;
  public $number; //ikea part number
  public $title;
  public $short_description;
  public $metric;
  public $middle_description;
  public $rus_price_total;
  public $bel_price_total;
  public $package_quantity;
  public $small_img_src;
  public $image_src;
  public $ikea_url_slug;

  use \yz\shoppingcart\CartPositionTrait;

  public function getPrice()
  {
    return $this->bel_price_total;
  }

  public function getId()
  {
    return $this->number;
  }

  public function __construct()
  {

    //$url='http://www.ikea.com/ru/ru/catalog/products/10192956/'; //URL to get content from..
    //    $str = self::getData($this->ikea_number_or_url); //Dumps the content

    $rate=Setting::find()->asArray()->one();
    $this->_rate_rus=$rate['rate_rus_rubl'];
    $this->_rate_additional=$rate['additional_rate'];
  }


  /*
   * 0р-20р 50%
20р-50р 40%
50р-150р 35%
150-300р 30% (как и было)
300-500р 25% (как и было)
От 500 20%     (как и было)
  */
  public static function getKoef($price)
  {
    $koef=1;
    if ($price < 200000) {
      $koef=1.5; //0-20
    } else if ($price < 500000) {
      $koef=1.4;//20-50
    } else if ($price < 1500000) {
      $koef=1.35; //50-150
    } else if ($price < 3000000) {
      $koef=1.3; //150-300
    } else if ($price < 5000000) {
      $koef=1.25; //300-500
    } else {
      $koef=1.2;
    }
    return $koef;
  }

  public static function calcBelPrice($rus_price_total, $rate_additional, $rate_rus)
  {
    $belPrice=Utilities::calculateBelPrice($rus_price_total, $rate_additional, $rate_rus);
    $koef=self::getKoef($belPrice);
    $price=$rus_price_total * $koef * $rate_rus;
    return round(ceil($price), -3); // round to nearest Thousand?
  }


  public function letsGo_OLD($htmlStr)
  {

    $rate=\app\models\Setting::find()->asArray()->one();
    $rate_rus=$rate['rate_rus_rubl'];
    $rate_additional=$rate['additional_rate'];

    $html=self::getData($htmlStr);

    $DOM=new \DOMDocument;
    @$DOM->loadHTML($html['data']);

    //название товара
    $item=$DOM->getElementById('name');
    if ($item) {
      $this->title=isset($item->nodeValue) ? trim($item->nodeValue) : null;

      //артикул
      $item=$DOM->getElementById('itemNumber');
      $this->number=isset($item->nodeValue) ? trim($item->nodeValue) : null;

      //краткое описание товара
      $item=$DOM->getElementById('type');
      $this->short_description=trim($item->nodeValue);

      //Среднее описание
      $item=$DOM->getElementById('salesArg');
      $this->middle_description=trim($item->nodeValue);

      /* размеры */
      $item=$DOM->getElementById('metric');
      $this->metric=$DOM->saveHTML($item);
      $this->metric=trim(preg_replace("/<\/?div[^>]*\>/i", "", $this->metric));

      //Цена Старая  (если на сайте перечеркнута старая цена и указана новая)
      $item=$DOM->getElementById('type');
      $prevPriceDom=$item->getElementsByTagName('span')->item(0);
      $prevPrice=0;
      if (isset($prevPriceDom->nodeValue))
        $prevPrice=filter_var($prevPriceDom->nodeValue, FILTER_SANITIZE_NUMBER_FLOAT);

      if ($prevPrice > 0) {
        $this->rus_price_total=$prevPrice;
      } else {
        //Цена
        $item=$DOM->getElementById('price1');
        $this->rus_price_total=filter_var($item->nodeValue, FILTER_SANITIZE_NUMBER_FLOAT);
      }

      //Кол-во в упаковке
      $item=$DOM->getElementById('storeformatpieces');
      $this->package_quantity=$item->nodeValue;


      $this->bel_price_total=\app\models\IkeaItem::calcBelPrice($this->rus_price_total, $rate_additional, $rate_rus);

      //маленькая картинка товара
      $item=$DOM->getElementById('img_prodInfo_lnk');
      if ($item)
        $this->small_img_src=$item->getAttribute('src');

      $item=$DOM->getElementById('productImg');
      $this->image_src=$item->getAttribute('src');
      $this->ikea_url_slug=str_replace('http://www.ikea.com', '', $htmlStr);

    } else {
      return false;
    }
  }

}
