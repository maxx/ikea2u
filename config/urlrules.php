<?php

return [
    //    'catalogue/<img_slug>' => 'site/img', //разобраться как работает и генеритьт картинки на стороне сервера
    'contacts' => 'site/contact',
    'catalog' => 'item/index',
    'winter' => 'item/winter',
    'strola' => 'item/strola',
    [
        'pattern' => 'about',
        'route' => 'page/view',
        'defaults' => ['name' => 'about'],
    ],
    [
        'pattern' => 'buy-in-office',
        'route' => 'page/view',
        'defaults' => ['name' => 'buy-in-office'],
    ],
    [
        'pattern' => 'promo',
        'route' => 'page/view',
        'defaults' => ['name' => 'promo'],
    ],    
];
