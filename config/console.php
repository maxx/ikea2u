<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');


$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/web_db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
       /* 'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            //зададим куда будут сохраняться наши файлы конфигураций RBAC
            'itemFile' => 'rbac/items.php',
            'assignmentFile' => 'rbac/assignments.php',
            'ruleFile' => 'rbac/rules.php'            
        ],*/
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
