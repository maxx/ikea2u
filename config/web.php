<?php

$params = require(__DIR__ . '/web_params.php');

$config = [
    'id' => 'ikea2u.by',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jghOJKgfiwe78df',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user', 'moder', 'admin'], //здесь прописываем роли
            'itemFile' => 'components/rbac/items.php', //Default path to items.php | NEW CONFIGURATIONS
            'assignmentFile' => 'components/rbac/assignments.php', //Default path to assignments.php | NEW CONFIGURATIONS
	     'ruleFile' => 'components/rbac/rules.php', //Default            
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' =>require(__DIR__ . '/urlrules.php'),
        ],
        'view' => array(
            'theme' => array(
                 'pathMap' => [
                     '@app/views' => '@app/themes/new/views',
                     '@app/components/views' => '@app/themes/new/components/views',
                     ],
                 'baseUrl' => '@web/themes/new',
                
            ),
        ),
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'shoppingCart',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                   // 'css' => [],
                   'css' => ['css/bootstrap.css'],
                ],
            /*  'yii\web\JqueryAsset' => [
              'sourcePath' => null, // do not publish the bundle
              'js' => [
              '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
              ]
              ], */
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logVars' => ['_GET', '_POST', '_FILES', '_SESSION'],
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/web_db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
