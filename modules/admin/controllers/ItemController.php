<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Item;
use app\models\IkeaOrderForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends DefaultController
{

  public function behaviors()
  {
    return [
        'verbs'=>[
            'class'=>VerbFilter::className(),
            'actions'=>[
                'delete'=>['post'],
            ],
        ],
    ];
  }

  /**
   * Lists all Item models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel=new Item();
    $dataProvider=$searchModel->search(Yii::$app->request->queryParams);

    /* $dataProvider = new ActiveDataProvider([
      'query' => Item::find(),
      ]); */


    return $this->render('index', [
        'dataProvider'=>$dataProvider,
        'searchModel'=>$searchModel
    ]);
  }

  /**
   * Displays a single Item model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
        'model'=>$this->findModel($id),
    ]);
  }

  /**
   * Creates a new Item model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model=new Item();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id'=>$model->id]);
    } else {
      return $this->render('create', [
          'model'=>$model,
      ]);
    }
  }

  public function actionAddFromIkea()
  {
    // Yii::$app->session['adminAddedTempItems'] = [];
    if (!isset(Yii::$app->session['adminAddedTempItems']))
      Yii::$app->session['adminAddedTempItems']=[];
    $model=new IkeaOrderForm();


    if ($model->load(Yii::$app->request->post())) {
      if ($model->validate()) {
        $x=\Yii::$app->session['adminAddedTempItems'];
        $x[$model->itemModel->number]=$model->itemModel;
        \Yii::$app->session['adminAddedTempItems']=$x;
        $this->refresh();
      }
    }

    return $this->render('addFromIkea', [
        'model'=>$model,
    ]);
  }

  public function actionDeleteFromIkea()
  {
    $x=\Yii::$app->session['adminAddedTempItems'];
    unset($x[$_POST['id']]);
    \Yii::$app->session['adminAddedTempItems']=$x;
    $res['code']=200;
    echo json_encode($res);
    Yii::$app->end();
  }

  public function actionClearFromIkea()
  {
    \Yii::$app->session['adminAddedTempItems']=[];
    $this->redirect('/admin/item/add-from-ikea');
  }

  public function actionCreateFromIkea()
  {
    if (!empty(\Yii::$app->session['adminAddedTempItems'])) {
      foreach (\Yii::$app->session['adminAddedTempItems'] as $ikeaItem) {
        $model=\app\models\Item::findOne(['number'=>$ikeaItem->number]);
        if (!$model) {
          $model=\app\models\Item::findOne(['ikea_url_slug'=>$ikeaItem->ikea_url_slug]);
          if (!$model)
            $model=new Item();
        }
        $model->number=$ikeaItem->number;
        $model->ikea_url_slug=$ikeaItem->ikea_url_slug;
        $model->title=$ikeaItem->title;
        $model->bel_price=$ikeaItem->bel_price_total;
        $model->rus_price=$ikeaItem->rus_price_total;
        $model->image_uri=$ikeaItem->image_src;
        $model->short_description=$ikeaItem->short_description;
        $model->full_description=$ikeaItem->middle_description;
        $model->qty_in_pack=((int)$ikeaItem->package_quantity > 0) ? $ikeaItem->package_quantity : 1;
        $model->metric=$ikeaItem->metric;
        $model->updated_at=date('Y-m-d H:i:s');
        if (!$model->save()) {
          print_r($model->getErrors());
          die();
        }
      }
      \Yii::$app->session['adminAddedTempItems']=[];
    }

    $this->redirect('/admin/item/index');
  }

  /**
   * Updates an existing Item model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model=$this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id'=>$model->id]);
    } else {
      return $this->render('update', [
          'model'=>$model,
      ]);
    }
  }

  /**
   * Deletes an existing Item model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  public function actionTostock()
  {
    $id=Yii::$app->getRequest()->getQueryParam('id');
    $model=$this->findModel($id);
    if ($model) {
      $modelItemStock=new \app\models\ItemStock();
      $modelItemStock->number=$model->number;
      $modelItemStock->price=\app\models\Utilities::Currency($model->bel_price);
      $modelItemStock->image_uri=$model->image_uri;
      $modelItemStock->title=$model->title;
      $modelItemStock->short_description=$model->short_description;
      $modelItemStock->full_description=$model->full_description;
      $modelItemStock->ikea_url_slug=$model->ikea_url_slug;
      $modelItemStock->metric=$model->metric;
      $modelItemStock->qty_in_pack=$model->qty_in_pack;
      $modelItemStock->key_features=$model->key_features;
      $modelItemStock->package_info=$model->package_info;
      $modelItemStock->care_inst=$model->care_inst;
      //$modelItemStock->attributes = $model->attributes;
      $modelItemStock->in_stock=1;
      if (!$modelItemStock->save()) {
        var_dump($modelItemStock->getErrors());
        die();
      }
    }
    $data['code']=200;

    return json_encode($data);
  }

  /**
   * Finds the Item model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Item the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model=Item::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
