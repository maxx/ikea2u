<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Order;
use yii\data\ActiveDataProvider;
use app\modules\admin\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Utilities;
use yii\helpers\VarDumper;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends DefaultController {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find(),
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $confirmedOrdersDetails = Order::getOrdersSpecialDetails(Order::STATUS_CONFIRMED);
        $addedOrdersDetails = Order::getOrdersSpecialDetails(Order::STATUS_ADDED);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'confirmedOrdersDetails' => $confirmedOrdersDetails,
                    'addedOrdersDetails' => $addedOrdersDetails,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (!empty($_POST['Item'])) {
            if (isset($_POST['Item']['id'])) {
                $item = \app\models\Item::findOne(['id' => $_POST['Item']['id']]);
                if ($item) {
                    $orderItem = \app\models\OrderItem::findOne(['order_id' => $id, 'item_id' => $item->id]);
                    if ($orderItem) {
                        $orderItem->qty = $orderItem->qty + 1;
                    } else {
                        $orderItem = new \app\models\OrderItem();
                        $orderItem->order_id = $id;
                        $orderItem->item_id = $item->id;
                        $orderItem->number = $item->number;
                        $orderItem->short_description = $item->short_description;
                        $orderItem->title = $item->title;
                        $orderItem->ikea_url_slug = $item->ikea_url_slug;
                        $orderItem->qty = 1;
                    }
                    $orderItem->rus_price = $item->rus_price;
                    $orderItem->bel_price = (float) Utilities::currencyDenom($item->bel_price, false, false);
                    $orderItem->total_bel_price = $orderItem->qty * $orderItem->bel_price;
                    $orderItem->total_rus_price = $orderItem->qty * $orderItem->rus_price;
                    $orderItem->save();
                }
                $model = $this->findModel($id);
                $total_due_rus = 0;
                $total_due_bel = 0;
                foreach ($model->orderItems as $orderItem) {
                    $total_due_rus += $orderItem->total_rus_price;
                    $total_due_bel += $orderItem->total_bel_price;
                }
                $model->total_due_rus = $total_due_rus;
                $model->total_due_bel = $total_due_bel;
                $model->save();
            }
            return $this->refresh();
        }

        if ($model->load(Yii::$app->request->post())) {

            $total_due_rus = $model->total_due_rus;
            $total_due_bel = $model->total_due_bel;
            if (!empty($_POST['OrderItem'])) {
                $total_due_rus = 0;
                $total_due_bel = 0;
                //\app\models\OrderItem::findOne(['order_id' => $id])->delete();
                foreach ($_POST['OrderItem'] as $item) {
                    //$modelItem = \app\models\Item::findOne(['id' => $item['id']]);
                    //if ($modelItem) {
                    $orderItem = \app\models\OrderItem::findOne(['id' => $item['id']]);
                    if (!$orderItem) {
                        $orderItem = new \app\models\OrderItem();
                    }

                    $orderItem->order_id = $id;
                    $orderItem->qty = $item['qty'];
                    $orderItem->total_bel_price = $orderItem->qty * $orderItem->bel_price;
                    $orderItem->total_rus_price = $orderItem->qty * $orderItem->rus_price;
                    if ($orderItem->save()) {
                        $total_due_rus += $orderItem->total_rus_price;
                        $total_due_bel += $orderItem->total_bel_price;
                    }
                    //}
                }
            }
            $model->total_due_rus = $total_due_rus;
            $model->total_due_bel = $total_due_bel;
            $model->save();


            //Order::recalculateOrder($id);
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->refresh();
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteItem($orderId, $itemId) {
        $res['code'] = '404';

        $model = \app\models\OrderItem::findOne(['order_id' => $orderId, 'id' => $itemId]);
        if ($model) {
            if ($model->delete()) {
                Order::recalculateOrderNew($orderId);
                $res['code'] = 200;
            }
        }
        $this->redirect(['update', 'id' => $orderId]);

        // return json_encode($res);
    }

    public function actionSearchItemsList() {
        $term = Yii::$app->getRequest()->getQueryParam('term');

        $data = \app\models\Item::find()
                ->select(['number as value', 'concat_ws(" ",number,title,short_description) as label', 'id as id', 'short_description', 'title', 'rus_price', 'bel_price'])
                ->where('number LIKE :number OR title LIKE :title', [':number' => '%' . $term . '%', ':title' => '%' . $term . '%'])
                ->asArray()
                ->all();

        return json_encode($data);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
