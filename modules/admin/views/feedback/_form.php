<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-2"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
    <div class="col-lg-2"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>

    <div class="col-lg-2"><?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?></div>
    <div class="col-lg-2"><?= $form->field($model, 'message')->textarea(['rows' => 6]) ?></div>
    <div class="col-lg-2"><?= $form->field($model, 'answer')->textarea(['rows' => 6]) ?></div>
    <div class="col-lg-2">
        <?=
        $form->field($model, 'opinion')->textInput()->dropDownList(
                \app\models\Feedback::itemAlias('Opinion'), ['prompt' => '']    // options
        );
        ?>
    </div>
    <div class="col-lg-2">
        <?=
        $form->field($model, 'status')->textInput()->dropDownList(
                \app\models\Feedback::itemAlias('Status'), ['prompt' => '']    // options
        );
        ?>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
