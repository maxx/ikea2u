<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'number',
            'price',
            //      'image_uri',
            'title',
            'short_description:ntext',
            [
                'attribute' => 'in_stock',
                'value' => 'in_stock',
                'filter' => array("1" => "Да", " " => "Нет"),
            ],
            'created_at',
            'updated_at',
            // 'ikea_url_slug:url',
            // 'metric:ntext',
            // 'qty_in_pack',
            // 'key_features:ntext',
            // 'package_info:ntext',
            // 'care_inst:ntext',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'],
        ],
    ]);
    ?>

</div>
