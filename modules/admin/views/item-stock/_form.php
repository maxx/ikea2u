<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'image_uri')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'in_stock')->checkBox([]) ?>
    <?= $form->field($model, 'full_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'ikea_url_slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metric')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'qty_in_pack')->textInput() ?>

    <?= $form->field($model, 'key_features')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'package_info')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'care_inst')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
