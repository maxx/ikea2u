<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="<?php echo Url::base() . '/js/shopping-cart.js'?>"></script>
    <script src="<?php echo Url::base() . '/js/admin/admin.js'?>"></script>

    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        
        <div class="container-full">
            <h1>Админка</h1>
            <?php
            NavBar::begin([
              /*  'brandLabel' => 'Ikea2u.by',
                'brandUrl' => Yii::$app->homeUrl,*/
                'options' => [
                    'class' => 'navbar navbar-default',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav'],
                'items' => [
                    ['label' => 'Заказы', 'url' => ['/admin/order/index']],
                    ['label' => 'Настройки сайта', 'url' => ['/admin/setting/index']],
                    ['label' => 'Товары', 'url' => ['/admin/item/index']],
                    ['label' => 'Товары В НАЛИЧИИ', 'url' => ['/admin/item-stock/index']],
                    ['label' => 'Страницы', 'url' => ['/admin/page/index']],
                    ['label' => 'Отзывы', 'url' => ['/admin/feedback/index']],
                    
                    Yii::$app->user->isGuest ?
                  ['label' => 'Вход', 'url' => ['/user/login']] :
                  ['label' => 'Выход (' . Yii::$app->user->identity->email . ')',
                  'url' => ['/user/logout'],
                  'linkOptions' => ['data-method' => 'post']], 
                ],
            ]);
            NavBar::end();
            ?>            

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; I2u.by <?= date('Y') ?></p>
            <p class="pull-right"></p>
        </div>
    </footer>

<?php $this->endBody() ?>
 
</body>
</html>
<?php $this->endPage() ?>
