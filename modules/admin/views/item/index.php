<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить товар c Ikea.ru', ['add-from-ikea'], ['class' => 'btn btn-success']) ?> |   <?= Html::a('Добавить товар(не пользоваться)', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'number',
            'title',
            'short_description:ntext',
            'bel_price',
            'rus_price',
            //      'image_uri',
            'fixed_bel_price',
            /*[
                'attribute' => 'in_stock',
                'value' => 'in_stock',
                'filter' => array("1" => "Да", " " => "Нет"),
            ],*/
            //'stock_price',
            //'created_at',
            'updated_at',
            // 'ikea_url_slug:url',
            // 'metric:ntext',
            // 'qty_in_pack',
            // 'key_features:ntext',
            // 'package_info:ntext',
            // 'care_inst:ntext',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{tostock}<br>{view}{update}',
                'buttons' => [
                    'tostock' => function ($url) {
                        return Html::a(
                                        'Добавить_в_наличие', $url, [
                                    'title' => 'Добавить_в_наличие',
                                    'class' => ' add-to-stock',
                                    'data-pjax' => '0',
                                        ]
                        );
                    },
                        ],
                    ],
                ],
            ]);
            ?>

</div>
