<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = 'Добавление товара с сайта Ikea на наш сайт';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-create">
    <h2>Чтобы товар с сайта Ikea появился на нашем сайте, нужно выполнить 2 шага.</h2>
    <h3>Шаг 1</h3>
    <p>Скопирйте ссылку на товар с сайта ikea и вставьте в поле ниже</p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'ikea-order-form']); ?>
            <div class="input-group">
                <?php
                echo $form->field($model, 'ikea_number_or_url', [
                    'template' => "{input}\n{hint}"
                ])->textInput(array('placeholder' => 'Ссылку вставлять сюда'))
                ?>
                <span class="input-group-btn">
                    <?= Html::submitButton('Взять товар с сайта Ikea', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </span>
            </div>
            <?php echo Html::error($model, 'ikea_number_or_url', ['class' => 'help-block']); //error?>                
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <h3>Шаг 2</h3>
    <p>Товары ниже могут быть добавлены на сайт, для этого нажмите кнопку "Добавить на сайт"</p>


    <table class="table table-striped table-hover ">
        <thead>
            <tr>
                <th>#</th><th>Артикул</th><th>Картинка</th>
                <th>Название</th><th>Краткое описание</th><th>Средне описание</th><th>Кол-во в упаковке</th>
                <th>Цена RUB</th><th>Цена бел.руб</th><th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach (\Yii::$app->session['adminAddedTempItems'] as $ikeaItem):
                ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $ikeaItem->number ?></td>
                    <td><img src="http://www.ikea.com/<?php echo $ikeaItem->small_img_src ?>"></td>
                    <td><?php echo $ikeaItem->title ?></td>
                    <td><?php echo $ikeaItem->short_description ?></td>
                    <td><?php echo $ikeaItem->middle_description ?></td>
                    <td><?php echo $ikeaItem->package_quantity ?></td>
                    <td><?php echo $ikeaItem->rus_price_total ?> rub</td>
                    <td><?php echo $ikeaItem->bel_price_total ?> б.р.</td>
                    <td><a href="<?php echo \yii\helpers\Url::toRoute('/admin/item/delete-from-ikea'); ?>" data-item="<?php echo $ikeaItem->number ?>" class="delete">Удалить</a></td>
                </tr>
                <?php
                $i++;
            endforeach;
            ?>
            <tr>
                <td colspan="7"><a href="<?php echo \yii\helpers\Url::toRoute('/admin/item/clear-from-ikea'); ?>">Очистить</a></td>
                <td colspan="5" style="text-align: right; font-weight: bold"><a href="<?php echo \yii\helpers\Url::toRoute('/admin/item/create-from-ikea'); ?>" class="btn btn-success" style="text-align: right;">Добавить на сайт</a></td>
            </tr>
        </tbody>
    </table> 

      

</div>
