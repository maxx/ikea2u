<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-lg-2">
        <?= $form->field($model, 'customer_name')->textInput(['maxlength' => 255]) ?>        
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'customer_email')->textInput(['maxlength' => 45]) ?>        
    </div>

    <div class="col-lg-2">
        <?= $form->field($model, 'customer_phone')->textInput(['maxlength' => 64]) ?>  
    </div>
    <div class="col-lg-2">
        <?=
        $form->field($model, 'status')->textInput()->dropDownList(
                \app\models\Order::getStatusesArray(), ['prompt' => '']    // options
        );
        ?>
    </div>

    <div class="col-lg-2">
        <?= $form->field($model, 'total_due')->textInput(['maxlength' => 12, 'readonly' => true]) ?>
    </div>


    <div class="clearfix"></div>

    <?php // echo  $form->field($model, 'updated_at')->textInput() ?>

    <?php // echo $form->field($model, 'created_at')->textInput() ?>
    <hr/>
    <table class="table table-striped table-hover ">
        <thead>
            <tr>
                <th>#</th>
                <th>Наименование</th>
                <th>Кол-во</th>
                <th>Общая стоимость(BYR)</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $str = '';
            foreach ($model->orderItems as $i => $item) :
                //echo  $item->item_number . ' ' . $item->itemNumber->title . ' ' . $item->qty . ' шт. ' . ' - ' . $item->bel_price . '<br/>';
                ?>
                <tr>
                    <td><?= $i ?>
                    
                     <?= Html::activeHiddenInput($item, "[$i]id", ['value'=>$item->item_id]) ?>
                    </td>
                    <td><?= $item->item->title ?></td>
                    <td><?= $form->field($item, "[$i]qty", ['template' => "{input}"])->textInput(['class' => 'item-qty']); ?></td>
                    <td><?= $form->field($item, "[$i]bel_price", ['template' => "{input}"])->textInput(['class' => 'bel-price-per-item', 'readonly' => true]); ?></td>
                    <td style="text-align: right"><a href="<?php echo \yii\helpers\Url::toRoute(['/admin/order/delete-item', 'orderId' => $model->id, 'itemId' => $item->item_id]); ?>" data-item="<?php echo $item->item->id ?>" class="">Удалить</a></td>
                </tr>                
            <?php endforeach; ?>

        </tbody>
    </table> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <hr/>
    <div class="row">

        <?php $form = ActiveForm::begin(['id' => 'add-item']); ?>

        <div class="col-lg-2">
            Начните вводить название товара:
            <?php

            use yii\web\JsExpression;
            use yii\jui\AutoComplete;

$model = new \app\models\Item();

            $url = '/admin/order/search-items-list';

            echo AutoComplete::widget([
                'name' => 'autocompleteField',
                'clientOptions' => [
                    'source' => $url,
                    'autoFill' => true,
                    'minLength' => '4',
                    'select' => new JsExpression("function( event, ui ) {
                            $('#item-id').val(ui.item.id);
                            $('#item-title').val(ui.item.title);
                            $('#item-short_description').val(ui.item.short_description);
                            $('#item-bel_price').val(ui.item.bel_price);
                            $('#item-rus_price').val(ui.item.rus_price);
                        }")
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255, 'readonly' => true]) ?>        
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'short_description')->textInput(['maxlength' => 45, 'readonly' => true]) ?>        
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'bel_price')->textInput(['maxlength' => 45, 'readonly' => true]) ?>        
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'rus_price')->textInput(['maxlength' => 45, 'readonly' => true]) ?>        
        </div>

        <?= Html::activeHiddenInput($model, 'id') ?>
        <div class="col-lg-2">
            <div class="form-group">
                <label class="control-label" style="display:block">&nbsp;</label>
                <?= Html::submitButton('Добавить товар', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

</div>
<?php /* ?>
  <script src="<?php echo Url::base() . '/js/admin/order.js'?>"></script>
  <?php */ ?>