<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'user_id',
            'customer_name',
            'customer_email:email',
            'customer_phone',
            [
                'header' => 'Детали заказа',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        $str .= $item->item->id . ' ' . $item->item->title . ' ' . $item->qty . ' шт. ' . ' - ' . $item->bel_price . '<br/>';
                    }

                    return $str;
                },
            ],
           
            // 'updated_at',
            // 'created_at',
            // 'total_due',
            // 'status',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
