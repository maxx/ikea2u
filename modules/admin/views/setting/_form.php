<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rate_rus_rubl')->textInput(['maxlength' => 10]) ?>
    <?= $form->field($model, 'additional_rate')->textInput(['maxlength' => 10]) ?>
    <br/>
    <div class="col-lg-6">
        <?= $form->field($model, 'delivery_countdown')->widget(DatePicker::className(), ['clientOptions' => [], 'dateFormat' => 'dd-MM-yyyy', ]) ?>
    </div>
<?php /*?>

        <strong>The final countdown</strong>
        We're leaving together 
        But still it's farewell 
        And maybe we'll come back 
        To earth, who can tell 
        I guess there is no one to blame 
        We're leaving ground(leaving ground) 
        Will things ever be the same again 
        It's a final countdown 

        The final countdown 
        We're heading for Venus 
        And still we stand tall 
        'Cause maybe they've seen us 
        And welcome us all  
        With so many light years to go 
        And thing to be found 
        I'm sure that we'll all miss her so 
        It's the final countdown 
        The final countdown 
        It's the final countdown 
        We're leaving together 
        The final countdown 
        We'll all miss her so 
        It's the final countdown


    </div>
    <div class="col-lg-6"><iframe width="520" height="315" src="//www.youtube.com/embed/9jK-NcRmVcw" frameborder="0" allowfullscreen=""></iframe></div>
<?php */?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
