<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    td:nth-child(7),td:nth-child(8){
        width: 10%;
    }
    td:nth-child(9),td:nth-child(10){
        width: 10%;
    }    
    
</style>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><strong>Кол-во новых заказов:</strong> <?php echo $addedOrdersDetails['cnt'] ?> на сумму <?php echo \app\models\Utilities::CurrencyRUR($addedOrdersDetails['total_sum_rus']) ?></p>
    <p><strong>Кол-во подвержденных заказов:</strong> <?php echo $confirmedOrdersDetails['cnt'] ?> </p>
    <hr/>
    <p>
        <?= Html::a('Создать заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            /* 'user_id', */
            'customer_name',
            'customer_email:email',
            'customer_phone',
            'address',
            'note',
           /* [
                'header' => 'Детали заказа',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        if (isset($item->item->number)) {
                            $id_num = $item->item->number ? $item->item->number : 'ID_' . $item->id;
                            $str .= $id_num . '  ' . $item->qty . ' шт. ' . ' - ' . $item->bel_price . '<br/>';
                        }
                    }
                    return $str;
                },
            ],*/
            [
                'header' => 'Название',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        if (isset($item->short_description)) {
                            $str .= $item->title.' '.$item->short_description . '<br/>';
                        }
                    }
                    return $str;
                },
            ],
            [
                'header' => 'Артикул',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        if (isset($item->number)) {
                            $str .=$item->number . '<br/>';
                        }
                    }
                    return $str;
                },
            ],
            [
                'header' => 'Кол-во',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        if (isset($item->qty)) {
                            $str .=$item->qty . '<br/>';
                        }
                    }
                    return $str;
                },
            ],
        [
                'header' => 'RUR',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        if (isset($item->number)) {
                           $str .= $item->total_rus_price . '<br/>';
                        }
                    }
                    return $str;
                },
            ],                        
            [
                'header' => 'BYN',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    foreach ($model->orderItems as $item) {
                        if (isset($item->number)) {
                           $str .= $item->total_bel_price . '<br/>';
                        }
                    }
                    return $str;
                },
            ],
   
            [
                'header' => 'Статус',
                'format' => 'raw',
                'value' => function ($model) {
                    return app\models\Order::itemAlias('Status', $model->status);
                },
            ],
             'updated_at',
             'created_at',
            // 'total_due_bel',
            // 'status',
            // 'note:ntext',
            // 'custom_rate_rus_rubl',
            // 'total_due_rus',
            // 'custom_additional_rate',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

    <hr>
    <h1>Export</h1>
    в разработке...
    <?php
    /*
      use kartik\export\ExportMenu;


      $gridColumns = [
      ['class' => 'yii\grid\SerialColumn'],
      'id',
      'user_id',
      'customer_name',
      'customer_email:email',
      'customer_phone',
      [
      'header' => 'Детали заказа',
      'format' => 'raw',
      'value' => function ($model) {
      $str = '';
      foreach ($model->orderItems as $item) {
      $str .= $item->item->id . ' ' . $item->item->title . ' ' . $item->qty . ' шт. ' . ' - ' . $item->bel_price . '<br/>';
      }

      return $str;
      },
      ],
      [
      'header' => 'Статус',
      'format' => 'raw',
      'value' => function ($model) {
      return app\models\Order::itemAlias('Status', $model->status);
      },
      ],
      ['class' => 'yii\grid\ActionColumn'],
      ];

      // Renders a export dropdown menu
      echo ExportMenu::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumns
      ]);

      // You can choose to render your own GridView separately
      echo \kartik\grid\GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $modell,
      'columns' => $gridColumns
      ]); */
    ?>

</div>
