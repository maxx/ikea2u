<?php

namespace app\modules\admin;

use Yii;
use yii\web\ForbiddenHttpException;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init() {

        if (!Yii::$app->user->can('dashboard')) {
            throw new ForbiddenHttpException('Access denied');
        }

        parent::init();
        //  \Yii::$app->view->theme->pathMap = ['@app/views' => '@app/modules/admin/views'];
        \Yii::$app->view->theme = new \yii\base\Theme([
            'pathMap' => ['@app/views' => '@app/modules/admin/views'],
            'baseUrl' => '@web/modules/admin',
        ]);


        //\Yii::$app->view->theme->baseUrl = '@web/adminlte';


        /* \Yii::$app->view->theme->pathMap = ['@app/views' => '@app/themes/admin/views'];
          \Yii::$app->view->theme->baseUrl = '@web/themes/admin';
          $this->layoutPath = Yii::getAlias('admin.views.layouts');
          $this->layout = 'main'; */
        // custom initialization code goes here
    }

}
