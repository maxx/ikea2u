<?php

namespace app\Entity;
class IkeaProductDataFromSearch extends AbstractIkeaProductData
{
  protected $_data;

  public function __construct($data)
  {
    $this->_data=$data;
  }

  public function getName(): string
  {
    return $this->_data['name'];
  }

  public function getNumber(): string
  {
    return $this->_data['id'];
  }

  public function getUrlSlug(): string
  {
    return $this->_data['pipUrl'];
  }

  public function getPrice()
  {
    return $this->_data['priceNumeral'];
  }

  public function getPriceSpan()
  {
    return !empty($this->_data['priceUnit']) ? $this->_data['priceUnit'] : '';
  }

  public function getImageUri()
  {
    return $this->_data['mainImageUrl'];
  }

  public function getImagesJSON()
  {
    //return $this->_data['id'];
  } //list of images in JSON format

  public function getShortDescription(): string
  {
    return $this->_data['mainImageAlt'];
  }

  public function getFullDescription(): string
  {
    return '';
  }

}