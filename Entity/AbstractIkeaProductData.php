<?php
namespace app\Entity;
abstract class AbstractIkeaProductData
{
  abstract public function getName(): string;

  abstract public function getNumber(): string;

  abstract public function getUrlSlug(): string;

  abstract public function getPrice();

  abstract public function getPriceSpan(); //4 шт, 2 м

  abstract public function getImageUri();

  abstract public function getImagesJSON(); //list of images in JSON format

  abstract public function getShortDescription(): string;

  abstract public function getFullDescription(): string;

}