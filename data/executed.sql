/*04.03.2016*/
ALTER TABLE `ikea2u`.`item` 
ADD COLUMN `is_manually_created` TINYINT(4) NULL DEFAULT NULL AFTER `care_inst`,
ADD COLUMN `in_stock` INT(11) NULL DEFAULT NULL AFTER `is_manually_created`,
ADD COLUMN `stock_price` DECIMAL(10,2) NULL DEFAULT NULL AFTER `in_stock`;
/*09.03.2016*/

CREATE TABLE IF NOT EXISTS `ikea2u`.`feedback` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `message` TEXT NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `place` VARCHAR(255) NULL DEFAULT NULL,
  `status` TINYINT(4) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
ALTER TABLE `ikea2u`.`feedback` 
ADD COLUMN `opinion` TINYINT(4) NULL DEFAULT NULL AFTER `place`,
ADD COLUMN `answer` TEXT NULL DEFAULT NULL AFTER `opinion`;