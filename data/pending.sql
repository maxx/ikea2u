/*18.04.2015*/
/* переделка категорий*/

ALTER TABLE `ikea2u`.`order_item` 
DROP FOREIGN KEY `fk_order_item_item1`;

ALTER TABLE `ikea2u`.`category_item` 
DROP FOREIGN KEY `fk_category_has_item_category1`,
DROP FOREIGN KEY `fk_category_has_item_item1`;

ALTER TABLE `ikea2u`.`order_item` 
ADD CONSTRAINT `fk_order_item_item1`
  FOREIGN KEY (`item_id`)
  REFERENCES `ikea2u`.`item` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

ALTER TABLE `ikea2u`.`category_item` 
ADD CONSTRAINT `fk_category_has_item_category1`
  FOREIGN KEY (`category_id`)
  REFERENCES `ikea2u`.`category` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_category_has_item_item1`
  FOREIGN KEY (`item_id`)
  REFERENCES `ikea2u`.`item` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;
  

/*
Executing SQL script in server

ERROR: Error 1825: Failed to add the foreign key constraint on table 'category_item'. Incorrect options in FOREIGN KEY constraint 'ikea2u/fk_category_has_item_category1'
*/
ALTER TABLE `ikea2u`.`category_item` 
CHANGE COLUMN `category_id` `category_id` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `item_id` `item_id` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `ikea2u`.`category_item` 
ADD CONSTRAINT `fk_category_has_item_category1`
  FOREIGN KEY (`category_id`)
  REFERENCES `ikea2u`.`category` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_category_has_item_item1`
  FOREIGN KEY (`item_id`)
  REFERENCES `ikea2u`.`item` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;


