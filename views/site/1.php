<!DOCTYPE html>
<html lang="ru-RU" dir="ltr">
<head>
  <meta charSet="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
  <meta name="robots" content="index, follow"/>
  <meta name="description"
        content="Большой выбор полотенец для кухни в интернет-магазине ИКЕА. Заказывайте кухонные полотенца по доступным ценам на нашем сайте с доставкой."/>
  <meta name="keywords" content="Кухонные полотенца по низкой цене в интернет-магазине ИКЕА"/>
  <meta property="og:title" content="Кухонные полотенца по низкой цене в интернет-магазине ИКЕА"/>
  <meta property="og:type" content="product.group"/>
  <meta property="og:url" content="https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/"/>
  <meta property="og:site_name" content="IKEA"/>
  <meta property="og:description"
        content="Большой выбор полотенец для кухни в интернет-магазине ИКЕА. Заказывайте кухонные полотенца по доступным ценам на нашем сайте с доставкой."/>
  <meta property="og:image" content="https://shop.static.ingka.ikea.com/category-images/Category_tea-towels.jpg"/>
  <title>Кухонные полотенца по низкой цене в интернет-магазине ИКЕА - IKEA</title>
  <link rel="canonical" href="https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/"/>
  <link rel="preload" href="https://www.ikea.com/ru/ru/product-lists/plp-main-542.js" as="script"/>
  <link rel="stylesheet" href="https://www.ikea.com/ru/ru/product-lists/plp-main-542.css"/>
  <link rel="stylesheet" href="/ru/ru/stylesheets/vendors.7e9ff7567c5737b8ab70.css"/>
  <link rel="stylesheet" href="/ru/ru/stylesheets/main.178f471b8184b3f5577f.css"/>
  <link rel="stylesheet"
        href="https://www.ikea.com/ru/ru/products/stylesheets/price-package-styles.513b574bd84c8a4745ed.css"/>
  <!-- 2021-09-09T14:11:44.696Z, Navigera 4fed225f -->

  <link rel="stylesheet" id="nav-styles"
        data-opti-web="true"
        data-opti-edge="false"
        data-opti-default="true"
        href="https://www.ikea.com/ru/ru/header-footer/styles/main.a9bf52448f3b18580abd.css">
  <link rel="stylesheet" href="https://www.ikea.com/global/assets/fonts/ru/fonts.e942dde8.css">


  <!-- 2021-09-09 13:39:07 CEST Search: refs/tags/v8.124.0-dfb4e4ee10bdb6a282d31325a508bb9b07296942 -->

  <script type="text/javascript" src="https://www.ikea.com/resources/2acc26ae98a30f9e47bbd5dca6ad17184ef84d02a8c7a"
          async></script>
  <script nomodule>
    var head = document.getElementsByTagName("head")[0];
    var link = document.createElement("link");
    link.rel = "stylesheet";
    link.href = "https://www.ikea.com/ru/ru/search/box-legacy.459c9495.css";
    head.appendChild(link);
  </script>

  <link rel="stylesheet" href="https://www.ikea.com/ru/ru/search/box.459c9495.css">


  <!-- RecommendationsStyle: version: ba3e9c0, site-folder: ru/ru -->
  <link rel="stylesheet" href="https://www.ikea.com/ru/ru/recommendations/panels/rec.0a488c57.css">
  <!-- /RecommendationsStyle -->
  <!-- gitVersion: 20210908200300, siteFolder: ru/ru -->
  <link rel="stylesheet" href="https://www.ikea.com/ru/ru/insp-feed/content-gallery-app/feed.e60f0cbc.css">


  <script src="https://www.ikea.com/ext/optimizelyjs/s/russia_web.js" async></script>

  <meta name="referrer" content="same-origin"/>
  <link rel="shortcut icon" href="https://www.ikea.com/ru/ru/static/favicon.838d8a3778e4d716eb72.ico">


  <!-- empty -->


  <script data-type="utag-data">var utag_data = {
      "site_platform": "m2",
      "visit_country": "ru",
      "visit_language": "ru",
      "page_type": "product listing",
      "site_section": "product listing>18851",
      "page_name": "no_page_name",
      "page_category_level": "chapter"
    }</script>
  <noscript>
    <style>.js-product-list {
            display: none
        }</style>
  </noscript>
</head>
<body data-is-range-page="true" class="product-listing-page"><!-- 2021-09-09T14:11:44.708Z, Navigera 4fed225f -->
<a href="#hnf-content" class="hnf-skip-to-content hnf-link">Перейти к основному контенту</a>
<div class="hnf-messages">

  <div id="value-proposition-message" data-config="{
    &quot;startdate&quot;: &quot;&quot;,
    &quot;enddate&quot;: &quot;&quot;
  }" class="hnf-message "></div>
  <div id="shoppable-app-message" class="hnf-message"></div>
</div>
<div class="hnf-header-hamburger hnf-page-container">
  <div class="hnf-page-container__inner">
    <div class="hnf-page-container__aside">
      <button type="button" class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" title="Меню" aria-label="Меню">
          <span class="hnf-btn__inner">
            <svg focusable="false" class="svg-icon  hnf-svg-icon" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd" d="M20 8H4V6h16v2zm0 5H4v-2h16v2zm0 5H4v-2h16v2z"/>
  </svg>
          </span>
      </button>
    </div>
  </div>
</div>
<header class="hnf-header">
  <div class="hnf-page-container">
    <div class="hnf-page-container__inner">
      <div class="hnf-header__container hnf-page-container__main">
        <div class="hnf-header__logo">
          <a href="https://www.ikea.com/ru/ru/" class="hnf-link" data-tracking-label="ikea-logo">
            <img src="https://www.ikea.com/ru/ru/static/ikea-logo.f7d9229f806b59ec64cb.svg" alt="ИКЕА" title="ИКЕА">
            <span class="hnf-header__sr-only">ИКЕА</span>
          </a>
        </div>
        <nav class="hnf-header__nav">
          <ul class="hnf-header__nav__main" data-header-links>

            <li>
              <a href="https://www.ikea.com/ru/ru/cat/tovary-products/" class="hnf-link" data-index="0"
                 data-products-link>Товары</a>
            </li>


            <li>
              <a href="https://www.ikea.com/ru/ru/rooms/" class="hnf-link" data-index="1">Комнаты</a>
            </li>


            <li>
              <a href="https://www.ikea.com/ru/ru/ideas/" class="hnf-link" data-index="2">Вдохновение</a>
            </li>


            <li>
              <a href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link" data-index="3">Услуги</a>
            </li>

          </ul>
        </nav>

        <div class="hnf-header__search">
          <div data-css-scope="search-box" data-namespace="search-box"
               data-version="refs/tags/v8.124.0-dfb4e4ee10bdb6a282d31325a508bb9b07296942"
               data-baseurl="https://www.ikea.com" data-locale="ru-RU">
            <div>
              <form role="search" action="https://www.ikea.com/ru/ru/search/" class="search-box ">
                <div class="search-wrapper">
                  <div class="search-field   ">
                    <svg class="svg-icon search-field__search-icon" height="24" width="24" aria-hidden="true">
                      <use href="/ru/ru/search/images/search-sprite.svg#search"></use>
                    </svg>
                    <input type="search" name="q" maxlength="150" class="search-field__input " spellcheck="false"
                           aria-label="Искать товары, новинки и вдохновение"
                           aria-placeholder="Искать товары, новинки и вдохновение" placeholder="Что вы ищете?"
                           autocapitalize="off" autocomplete="off" autocorrect="off"><span
                      class="search-box__button-wrapper"></span></div>
                </div>
              </form>
            </div>
          </div>

        </div>

        <ul class="hnf-header__icons" data-shopping-links>
          <li class="hnf-header__search-btn">
            <button type="button" class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" aria-expanded="false"
                    title="Поиск" aria-label="Поиск">
                <span class="hnf-btn__inner">
                  <svg focusable="false" class="svg-icon  hnf-svg-icon" width="24" height="24" viewBox="0 0 24 24"
                       fill="none" xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="M13.9804 15.3946c-1.0361.7502-2.3099 1.1925-3.6869 1.1925C6.8177 16.5871 4 13.7694 4 10.2935 4 6.8177 6.8177 4 10.2935 4c3.4759 0 6.2936 2.8177 6.2936 6.2935 0 1.377-.4423 2.6508-1.1925 3.6869l4.6016 4.6016-1.4142 1.4142-4.6016-4.6016zm.6067-5.1011c0 2.3713-1.9223 4.2936-4.2936 4.2936C7.9223 14.5871 6 12.6648 6 10.2935 6 7.9223 7.9223 6 10.2935 6c2.3713 0 4.2936 1.9223 4.2936 4.2935z"/>
  </svg>
                </span>
            </button>
          </li>
          <li class="hnf-header__order-tracking-link">
            <a class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary"
               href="https://order.ikea.com/ru/ru/purchases/lookup/" title="Отследить заказ"
               aria-label="Отследить заказ" rel="nofollow" data-tracking-label="order-tracking">
                <span class="hnf-btn__inner">
                  <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn__icon" width="24" height="24"
                       viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="M1 4h15v3h3.0246l3.9793 5.6848V18h-2.6567c-.4218 1.3056-1.6473 2.25-3.0933 2.25-1.446 0-2.6715-.9444-3.0932-2.25h-3.9044c-.4217 1.3056-1.6472 2.25-3.0932 2.25S4.4916 19.3056 4.0698 18H1V4zm3.0698 12c.4218-1.3056 1.6473-2.25 3.0933-2.25 1.446 0 2.6715.9444 3.0932 2.25H14V6H3v10h1.0698zM16 14.0007a3.24 3.24 0 0 1 1.2539-.2507c1.446 0 2.6715.9444 3.0933 2.25h.6567v-2.6848L17.9833 9H16v5.0007zM7.163 15.75c-.6903 0-1.25.5596-1.25 1.25s.5597 1.25 1.25 1.25c.6904 0 1.25-.5596 1.25-1.25s-.5596-1.25-1.25-1.25zm10.0909 0c-.6904 0-1.25.5596-1.25 1.25s.5596 1.25 1.25 1.25 1.25-.5596 1.25-1.25-.5596-1.25-1.25-1.25z"/>
  </svg>
                  <span class="hnf-btn__label">Отследить заказ</span>
                </span>
            </a>
          </li>
          <li class="hnf-header__profile-link">
            <a class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" href="https://www.ikea.com/ru/ru/profile/login/"
               title="Мой профиль" aria-label="Мой профиль" rel="nofollow" data-tracking-label="profile"
               data-profile-link>
                  <span class="hnf-btn__inner">
                    <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn__icon" width="24" height="24"
                         viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="M10.6724 6.4678c.2734-.2812.6804-.4707 1.3493-.4707.3971 0 .705.0838.9529.2225.241.1348.4379.3311.5934.6193l.0033.006c.1394.2541.237.6185.237 1.1403 0 .7856-.2046 1.2451-.4796 1.5278l-.0048.005c-.2759.2876-.679.4764-1.334.4764-.3857 0-.6962-.082-.956-.2241-.2388-.1344-.4342-.3293-.5888-.6147-.1454-.275-.2419-.652-.2419-1.1704 0-.7902.2035-1.2442.4692-1.5174zm1.3493-2.4717c-1.0834 0-2.054.3262-2.7838 1.0766-.7376.7583-1.0358 1.781-1.0358 2.9125 0 .7656.1431 1.483.4773 2.112l.0031.0058c.3249.602.785 1.084 1.3777 1.4154l.0062.0035c.5874.323 1.2368.4736 1.9235.4736 1.0818 0 2.0484-.3333 2.7755-1.0896.7406-.7627 1.044-1.786 1.044-2.9207 0-.7629-.1421-1.4784-.482-2.0996-.3247-.6006-.7844-1.0815-1.376-1.4125-.5858-.3276-1.2388-.477-1.9297-.477zM6.4691 16.8582c.2983-.5803.7228-1.0273 1.29-1.3572.5582-.3191 1.2834-.5049 2.2209-.5049h4.04c.9375 0 1.6626.1858 2.2209.5049.5672.3299.9917.7769 1.29 1.3572.3031.5896.4691 1.2936.4691 2.1379v1h2v-1c0-1.1122-.2205-2.1384-.6904-3.0523a5.3218 5.3218 0 0 0-2.0722-2.1769c-.9279-.5315-2.0157-.7708-3.2174-.7708H9.98c-1.1145 0-2.2483.212-3.2225.7737-.8982.5215-1.5928 1.2515-2.0671 2.174C4.2205 16.8577 4 17.8839 4 18.9961v1h2v-1c0-.8443.166-1.5483.4691-2.1379z"/>
  </svg>
                    <span class="hnf-btn__label">Мой профиль</span>
                  </span>
            </a>
          </li>
          <li class="hnf-header__shopping-list-link" data-shoppinglist-icon="favorite">
            <a class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" href="https://www.ikea.com/ru/ru/favourites/"
               title="Список покупок" aria-label="Список покупок" rel="nofollow" data-tracking-label="shopping-list">
                  <span class="hnf-btn__inner">
                    <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn__icon" width="24" height="24"
                         viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" clip-rule="evenodd"
        d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"/>
  </svg>
                    <span class="hnf-btn__label">Список покупок</span>
                  </span>
            </a>
          </li>
          <li class="hnf-header__shopping-cart-link">
            <a class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" href="https://www.ikea.com/ru/ru/shoppingcart/"
               title="Корзина" aria-label="Корзина" rel="nofollow" data-tracking-label="shopping-bag">
                  <span class="hnf-btn__inner js-shopping-cart-icon" data-market-code="ru-RU">
                    <span class="hnf-header__cart-counter" style="display:none"></span>
                      <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-svg-bag-default hnf-btn__icon" width="24"
                           height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="M10.9994 4h-.5621l-.2922.4802-3.357 5.517h-5.069l.3107 1.2425 1.6212 6.4851c.334 1.3355 1.5339 2.2724 2.9105 2.2724h10.8769c1.3766 0 2.5765-.9369 2.9104-2.2724l1.6213-6.4851.3106-1.2425h-5.0695l-3.3574-5.517L13.5618 4h-2.5624zm3.8707 5.9972L12.4376 6h-.8761L9.1292 9.9972h5.7409zm-9.2787 7.2425-1.3106-5.2425h15.4384l-1.3106 5.2425a1 1 0 0 1-.9701.7575H6.5615a1 1 0 0 1-.97-.7575z"/>
  </svg>
                    <span class="hnf-btn__label">Корзина</span>
                  </span>
            </a>
          </li>
          <li class="hnf-header__hamburger">
            <button type="button" class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" aria-expanded="false"
                    title="Меню" aria-label="Меню">
                <span class="hnf-btn__inner">
                  <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn__icon" width="24" height="24"
                       viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd" d="M20 8H4V6h16v2zm0 5H4v-2h16v2zm0 5H4v-2h16v2z"/>
  </svg>
                </span>
            </button>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>
<div id="hnf-header-filler"></div>

<aside class="hnf-menu hnf-menu--default hnf-menu--hidden" aria-hidden="true" data-title-more="Больше">
  <div class="hnf-menu__top">
    <div class="hnf-menu__close">
      <button type="button" class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" id="hnf-menu-close-btn"
              aria-expanded="true" title="Закрыть меню" aria-label="Закрыть меню">
          <span class="hnf-btn__inner">
            <svg focusable="false" class="svg-icon  hnf-svg-icon" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="m11.9997 13.4149 4.9498 4.9497 1.4142-1.4142-4.9498-4.9497 4.9498-4.9498-1.4142-1.4142-4.9498 4.9498L7.05 5.6367 5.6357 7.051l4.9498 4.9498-4.9497 4.9497L7.05 18.3646l4.9497-4.9497z"/>
  </svg>
          </span>
      </button>
    </div>
    <div class="hnf-menu__logo">
      <a href="https://www.ikea.com/ru/ru/" class="hnf-link" data-tracking-label="ikea-logo">
        <img src="https://www.ikea.com/ru/ru/static/ikea-logo.f7d9229f806b59ec64cb.svg" alt="ИКЕА" title="ИКЕА">
        <span class="hnf-menu__sr-only">ИКЕА</span>
      </a>
    </div>
    <div class="hnf-menu__search">
      <button type="button" class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" id="hnf-menu-search-btn"
              aria-expanded="false" title="Поиск">
          <span class="hnf-btn__inner">
            <svg focusable="false" class="svg-icon  hnf-svg-icon" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="M13.9804 15.3946c-1.0361.7502-2.3099 1.1925-3.6869 1.1925C6.8177 16.5871 4 13.7694 4 10.2935 4 6.8177 6.8177 4 10.2935 4c3.4759 0 6.2936 2.8177 6.2936 6.2935 0 1.377-.4423 2.6508-1.1925 3.6869l4.6016 4.6016-1.4142 1.4142-4.6016-4.6016zm.6067-5.1011c0 2.3713-1.9223 4.2936-4.2936 4.2936C7.9223 14.5871 6 12.6648 6 10.2935 6 7.9223 7.9223 6 10.2935 6c2.3713 0 4.2936 1.9223 4.2936 4.2935z"/>
  </svg>
            <span class="hnf-btn__label">Поиск</span>
          </span>
      </button>
    </div>
    <div class="hnf-menu__back hnf-menu__back--hidden">
      <button type="button" class="hnf-btn hnf-btn--small hnf-btn--icon-tertiary" id="hnf-menu-back-btn"
              aria-expanded="true" title="Назад" aria-label="Назад">
          <span class="hnf-btn__inner">
            <svg focusable="false" class="svg-icon  hnf-svg-icon" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="m3.999 11.9997 8 8.0011 1.4143-1.4141-5.5859-5.5866H20v-2H7.8273l5.5871-5.5868-1.4142-1.4143-8.0012 8.0007z"/>
  </svg>
          </span>
      </button>
    </div>
  </div>

  <div class="hnf-menu__container hnf-menu__container--pf hnf-menu__container--hidden">
    <nav class="hnf-menu__nav" aria-hidden="false">
      <ul class="hnf-menu__nav__main" data-main-links>
        <li class="hnf-menu__nav__main__category hnf-menu__nav__main__category--first">
          <a href="https://www.ikea.com/ru/ru/cat/hranenie-i-poryadok-st001/" class="hnf-link"
             data-tracking-label="st001" role="button" data-index="c-0">Хранение и порядок</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/mebel-fu001/" class="hnf-link" data-tracking-label="fu001"
             role="button" data-index="c-1">Мебель</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/krovati-i-matrasy-bm001/" class="hnf-link" data-tracking-label="bm001"
             role="button" data-index="c-2">Кровати и матрасы</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/kuhnya-i-bytovaya-tehnika-ka001/" class="hnf-link"
             data-tracking-label="ka001" role="button" data-index="c-3">Кухни и бытовая техника</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/vannaya-ba001/" class="hnf-link" data-tracking-label="ba001"
             role="button" data-index="c-4">Ванная</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/detskie-tovary-bc001/" class="hnf-link" data-tracking-label="bc001"
             role="button" data-index="c-5">Детские товары</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/tekstil-dlya-doma-tl001/" class="hnf-link" data-tracking-label="tl001"
             role="button" data-index="c-6">Текстиль для дома</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/kovry-i-napolnye-pokrytiya-rm001/" class="hnf-link"
             data-tracking-label="rm001" role="button" data-index="c-7">Ковры и напольные покрытия</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/dekor-dlya-doma-de001/" class="hnf-link" data-tracking-label="de001"
             role="button" data-index="c-8">Декор для дома</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/posuda-kt001/" class="hnf-link" data-tracking-label="kt001"
             role="button" data-index="c-9">Посуда</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/osveshchenie-li001/" class="hnf-link" data-tracking-label="li001"
             role="button" data-index="c-10">Освещение</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/stirka-i-uborka-lc001/" class="hnf-link" data-tracking-label="lc001"
             role="button" data-index="c-11">Стирка и уборка</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/balkon-i-sad-od001/" class="hnf-link" data-tracking-label="od001"
             role="button" data-index="c-12">Балкон и сад</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/rasteniya-i-kashpo-pp001/" class="hnf-link"
             data-tracking-label="pp001" role="button" data-index="c-13">Растения и кашпо</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/elektronika-he001/" class="hnf-link" data-tracking-label="he001"
             role="button" data-index="c-14">Электроника</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/tovary-dlya-remonta-hi001/" class="hnf-link"
             data-tracking-label="hi001" role="button" data-index="c-15">Товары для ремонта</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/tovary-dlya-zhivotnyh-pt001/" class="hnf-link"
             data-tracking-label="pt001" role="button" data-index="c-16">Товары для животных</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/eda-v-ikea-fb001/" class="hnf-link" data-tracking-label="fb001"
             role="button" data-index="c-17">Еда в ИКЕА</a>
        </li>
        <li class="hnf-menu__nav__main__category">
          <a href="https://www.ikea.com/ru/ru/cat/letnyaya-kollekciya-ss001/" class="hnf-link"
             data-tracking-label="ss001" role="button" data-index="c-18">Летняя коллекция</a>
        </li>
        <li class="hnf-menu__nav__main__category hnf-menu__nav__main__category--last">
          <a href="https://www.ikea.com/ru/ru/cat/zimnyaya-kollekciya-wt001/" class="hnf-link"
             data-tracking-label="wt001" role="button" data-index="c-19">Зимняя коллекция</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/rooms/" class="hnf-link" data-tracking-label="rooms" role="button"
             data-index="1">Комнаты</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/ideas/" class="hnf-link" data-tracking-label="ideas" role="button"
             data-index="2">Вдохновение</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link" data-tracking-label="customer-service"
             role="button" data-index="3">Услуги</a>
        </li>
      </ul>
      <ul class="hnf-menu__nav__aux" data-aux-links>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/dostavka-iz-internet-magazina-pub30f2c801"
             class="hnf-link">Доставка и самовывоз</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/faq/" class="hnf-link">Вопросы и ответы</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/kak-proverit-nalichie-tovara-pub5dbbeda0"
             class="hnf-link">Проверить наличие товара</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/faq/kak-kupit-onlain-pubb407c909" class="hnf-link">Как
            купить онлайн</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/profile/login/" class="hnf-link">Личный кабинет</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/ikea-business/" class="hnf-link">ИКЕА для бизнеса</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/ikea-family/" class="hnf-link">IKEA Family</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/campaigns/kvartiroteka/" class="hnf-link">Квартиротека</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/contact-us/" class="hnf-link">Контакты</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/stores/" class="hnf-link">Магазины</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/cat/eda-v-ikea-fb001/" class="hnf-link">Еда в ИКЕА</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/this-is-ikea/sustainable-everyday/" class="hnf-link">Люди и планета</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/services/pomosh-specialista-v-pokupke-onlain-pub6fb047c0"
             class="hnf-link">Помощь специалиста в покупке онлайн</a>
        </li>
      </ul>
    </nav>

    <div class="hnf-menu__nav-wrapper">
      <div class="hnf-loading">
        <div class="hnf-loading__ball-wrapper" aria-hidden="true">
          <div class="hnf-loading__ball hnf-loading__ball--active"></div>
        </div>
      </div>


      <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="1" aria-hidden="true">
        <span class="hnf-menu__heading">Комнаты</span>
        <ul data-tracking-label="rooms" data-menu-sub2-links
            class="hnf-menu__nav2__main hnf-menu__nav2__main--rooms-grid"
            aria-label="Подменю для Комнаты">
          <li>
            <a data-tracking-label="rooms/living-room" href="https://www.ikea.com/ru/ru/rooms/living-room/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Гостиная"
                                 data-src="https://www.ikea.com/images/seryi-divan-v-interere-gostinoi-3b41422c84f2a6ee5a1ec7d3424bcd5f.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Гостиная</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/bedroom" href="https://www.ikea.com/ru/ru/rooms/bedroom/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Спальня"
                                 data-src="https://www.ikea.com/images/spalnya-10b0be7a3743ab97d001c97d038aaf4d.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Спальня</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/kitchen" href="https://www.ikea.com/ru/ru/rooms/kitchen/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Кухня"
                                 data-src="https://www.ikea.com/images/kukhnya-otdelannaya-beloi-plitkoi-s-belymi-fasadami-veddinge-588d723470398c845beb548d470f9588.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Кухня</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/bathroom" href="https://www.ikea.com/ru/ru/rooms/bathroom/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Ванная"
                                 data-src="https://www.ikea.com/images/vannaya-oblicovannaya-zelenym-kafelem-belyi-shkaf-pod-rakovi-7e65306e9e5678708b55855391efa003.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Ванная</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/home-office" href="https://www.ikea.com/ru/ru/rooms/home-office/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Рабочий кабинет"
                                 data-src="https://www.ikea.com/images/kollazh-pismennyi-stol-stul-i-rabochie-tumby-20c617f0270ec76fe56315bdf860bdae.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Рабочий кабинет</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/childrens-room" href="https://www.ikea.com/ru/ru/rooms/childrens-room/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Детская"
                                 data-src="https://www.ikea.com/images/v-detskoi-spalne-stoit-svetlo-rozovaya-razdvizhnaya-krovat-m-0f25b8173637c10071923b684fee5729.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Детская</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/dining" href="https://www.ikea.com/ru/ru/rooms/dining/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Столовая"
                                 data-src="https://www.ikea.com/images/svetlaya-zona-stolovoi-obedennyi-stol-i-stulya-iz-bambuka-i--da1be86f56095b4a73cf57d1bfdc424f.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Столовая</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/hallway" href="https://www.ikea.com/ru/ru/rooms/hallway/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Прихожая"
                                 data-src="https://www.ikea.com/images/prikhozhaya-b2d7477bc49d086f8cbfe9bf9137cce7.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Прихожая</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="rooms/outdoor" href="https://www.ikea.com/ru/ru/rooms/outdoor/"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Дача и сад"
                                 data-src="https://www.ikea.com/images/interer-na-otkrytoi-terasse-s-mebelyu-ikea-dlya-balkona-i-sa-0ff7f06a2ed64d083bdc842ed67276da.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Дача и сад</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="ikea-business" href="https://www.ikea.com/ru/ru/ikea-business/"
               class="hnf-link" target="_blank" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="ИКЕА для бизнеса"
                                 data-src="https://www.ikea.com/images/lyudi-sidyat-za-stolom-i-obsuzhdayut-biznes-voprosy-50cb79e6f6be14cd48de44699d2ec272.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">ИКЕА для бизнеса</span>
            </a>
          </li>
          <li>
            <a data-tracking-label="kladovka-pub75997b90" href="https://www.ikea.com/ru/ru/rooms/kladovka-pub75997b90"
               class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Кладовка"
                                 data-src="https://www.ikea.com/images/kladovka-2af793f9ef74e1d6fb1b9b84e44a023b.jpg?f=xxxs"/>
                          </span>
              <span class="mr__nav__title">Кладовка</span>
            </a>
          </li>
        </ul>
      </nav>


      <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="2" aria-hidden="true">
        <span class="hnf-menu__heading">Вдохновение</span>
        <ul data-tracking-label="Вдохновение" data-menu-sub2-links class="hnf-menu__nav2__main"
            aria-label="Подменю для Вдохновение">
          <li><a data-tracking-label="ideas" href="https://www.ikea.com/ru/ru/ideas/" class="hnf-link">Идеи</a></li>
          <li><a data-tracking-label="campaigns/kvartiroteka"
                 href="https://www.ikea.com/ru/ru/campaigns/kvartiroteka/#/" class="hnf-link" target="_blank">Бесплатные
              дизайн-проекты в Квартиротеке</a></li>
          <li><a data-tracking-label="gotovye-resheniya-dlya-doma-pub345a2410"
                 href="https://www.ikea.com/ru/ru/rooms/gotovye-resheniya-dlya-doma-pub345a2410" class="hnf-link">Интерьеры
              комнат</a></li>
          <li><a data-tracking-label="https://ikea-family.ru/" href="https://ikea-family.ru/" class="hnf-link">Идеи
              наших покупателей</a></li>
          <li><a data-tracking-label="ideas/welcome-to-my-place"
                 href="https://www.ikea.com/ru/ru/ideas/welcome-to-my-place/" class="hnf-link">Идеи наших
              сотрудников</a></li>
          <li><a data-tracking-label="https://publications-ru-ru.ikea.com/ikea_catalogue_21/page/1"
                 href="https://publications-ru-ru.ikea.com/ikea_catalogue_21/page/1" class="hnf-link" target="_blank">Каталог
              2021</a></li>
        </ul>
      </nav>


      <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="3" aria-hidden="true">
        <span class="hnf-menu__heading">Услуги</span>
        <ul data-tracking-label="Услуги" data-menu-sub2-links class="hnf-menu__nav2__main"
            aria-label="Подменю для Услуги">
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Доставка и самовывоз</a></li>
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Подготовка к покупке</a></li>
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Планирование и обустройство дома</a></li>
          <li><a data-tracking-label="customer-service/services/assembly"
                 href="https://www.ikea.com/ru/ru/customer-service/services/assembly/" class="hnf-link">Сборка и
              установка</a></li>
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Оплата</a></li>
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Обмен и возврат</a></li>
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Заботимся о людях и планете</a></li>
          <li><a data-tracking-label="ikea-business" href="https://www.ikea.com/ru/ru/ikea-business/" class="hnf-link">ИКЕА
              для бизнеса</a></li>
          <li><a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
                 class="hnf-link">Все услуги и информация</a></li>
          <li><a data-tracking-label="customer-service/terms-conditions"
                 href="https://www.ikea.com/ru/ru/customer-service/terms-conditions/" class="hnf-link">Правила и
              положения</a></li>
        </ul>
      </nav>


    </div>
  </div>
  <div class="hnf-menu__container hnf-menu__container--default hnf-menu__container--hidden">
    <nav class="hnf-menu__nav" aria-hidden="false">
      <ul class="hnf-menu__nav__main" data-main-links>
        <li>
          <a href="https://www.ikea.com/ru/ru/cat/tovary-products/" class="hnf-link" data-tracking-label="products"
             role="button" data-index="0" data-products-link>Товары</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/rooms/" class="hnf-link" data-tracking-label="rooms" role="button"
             data-index="1">Комнаты</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/ideas/" class="hnf-link" data-tracking-label="ideas" role="button"
             data-index="2">Вдохновение</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link" data-tracking-label="customer-service"
             role="button" data-index="3">Услуги</a>
        </li>
      </ul>
      <ul class="hnf-menu__nav__aux" data-aux-links>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/dostavka-iz-internet-magazina-pub30f2c801"
             class="hnf-link">Доставка и самовывоз</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/faq/" class="hnf-link">Вопросы и ответы</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/kak-proverit-nalichie-tovara-pub5dbbeda0"
             class="hnf-link">Проверить наличие товара</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/faq/kak-kupit-onlain-pubb407c909" class="hnf-link">Как
            купить онлайн</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/profile/login/" class="hnf-link">Личный кабинет</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/ikea-business/" class="hnf-link">ИКЕА для бизнеса</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/ikea-family/" class="hnf-link">IKEA Family</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/campaigns/kvartiroteka/" class="hnf-link">Квартиротека</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/contact-us/" class="hnf-link">Контакты</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/stores/" class="hnf-link">Магазины</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/cat/eda-v-ikea-fb001/" class="hnf-link">Еда в ИКЕА</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/this-is-ikea/sustainable-everyday/" class="hnf-link">Люди и планета</a>
        </li>
        <li>
          <a href="https://www.ikea.com/ru/ru/customer-service/services/pomosh-specialista-v-pokupke-onlain-pub6fb047c0"
             class="hnf-link">Помощь специалиста в покупке онлайн</a>
        </li>
      </ul>
    </nav>


    <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="0" aria-hidden="true">
      <span class="hnf-menu__heading">Товары</span>
      <ul data-tracking-label="products" data-show-rv="1" data-menu-sub1-links class="hnf-menu__nav2__main"
          aria-label="Подменю для Товары">
        <li class="hnf-menu--highlight">
          <a data-tracking-label="offers/family-offers" href="https://www.ikea.com/ru/ru/offers/family-offers/"
             class="hnf-link">Специальные предложения для держателей карт IKEA Family</a>
        </li>
        <li class="hnf-menu--highlight">
          <a data-tracking-label="new" href="https://www.ikea.com/ru/ru/new/" class="hnf-link" role="button">Новинки</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Новинки</span>
            <ul data-tracking-label="products | new" class="hnf-menu__nav3__main" aria-label="Подменю для Новинки">
              <li><a data-tracking-label="new" href="https://www.ikea.com/ru/ru/new/" class="hnf-link">Смотреть все</a>
              </li>
              <li><a data-tracking-label="new/new-products" href="https://www.ikea.com/ru/ru/new/new-products/"
                     class="hnf-link">Смотреть товары</a></li>
              <li><a data-tracking-label="new/gallery" href="https://www.ikea.com/ru/ru/new/gallery/" class="hnf-link">Смотреть
                  галерею новинок</a></li>
              <li><a data-tracking-label="limited-collections-pub2a89e4f0"
                     href="https://www.ikea.com/ru/ru/new/limited-collections-pub2a89e4f0" class="hnf-link">Смотреть
                  лимитированные коллекции</a></li>
            </ul>
          </nav>
        </li>
        <li class="hnf-menu--highlight">
          <a data-tracking-label="tovary-po-dostupnym-cenam-pubde47a980"
             href="https://www.ikea.com/ru/ru/campaigns/tovary-po-dostupnym-cenam-pubde47a980" class="hnf-link">Товары
            по доступным ценам</a>
        </li>
        <li class="hnf-menu--highlight">
          <a data-tracking-label="product-guides/sustainable-products"
             href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/" class="hnf-link" role="button">Товары
            для экологичного образа жизни</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Товары для экологичного образа жизни</span>
            <ul data-tracking-label="products | product-guides/sustainable-products" class="hnf-menu__nav3__main"
                aria-label="Подменю для Товары для экологичного образа жизни">
              <li><a data-tracking-label="product-guides/sustainable-products/saving-water"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/saving-water/"
                     class="hnf-link">Как сэкономить дома воду</a></li>
              <li><a data-tracking-label="product-guides/sustainable-products/saving-energy"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/saving-energy/"
                     class="hnf-link">Экономьте энергию и помогайте природе</a></li>
              <li><a data-tracking-label="product-guides/sustainable-products/healthy-homes"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/healthy-homes/"
                     class="hnf-link">Как заботиться о здоровье дома</a></li>
              <li><a data-tracking-label="product-guides/sustainable-products/reducing-waste"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/reducing-waste/"
                     class="hnf-link">Как сократить количество мусора</a></li>
              <li><a data-tracking-label="product-guides/sustainable-products/sustainable-furniture"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/sustainable-furniture/"
                     class="hnf-link">Как выбрать мебель, которая прослужит дольше</a></li>
              <li><a data-tracking-label="product-guides/sustainable-products/sustainable-materials"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/sustainable-materials/"
                     class="hnf-link">Как выбрать более экологичные материалы</a></li>
              <li><a data-tracking-label="product-guides/sustainable-products/sustainable-food"
                     href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/sustainable-food/"
                     class="hnf-link">Как питаться экологичнее</a></li>
            </ul>
            <div class="hnf-menu__nav3__promobanner--wrapper">
              <a href="https://www.ikea.com/ru/ru/product-guides/sustainable-products/"
                 class="hnf-link hnf-link--white">
                <div class="hnf-menu__nav3__promobanner hnf-menu__nav3__promobanner--green">
                  <small class="hnf-menu__nav3__promobanner--eyebrow">Руководство</small>
                  <strong class="hnf-menu__nav3__promobanner--title">Просто жить осознанно</strong>
                  <span class="hnf-btn hnf-btn--icon-primary-inverse hnf-menu__nav3__promobanner--chevron"
                        id="hnf-menu__nav3__promobanner--button">
                                  <span class="hnf-btn__inner hnf-btn__inner--icon-primary-inverse">
                                    <span class="hnf-btn__copy">
                                      <svg focusable="false" class="svg-icon  hnf-svg-icon" width="24" height="24"
                                           viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
               <path fill-rule="evenodd" clip-rule="evenodd"
                     d="m20.0008 12.0001-8-8.001-1.4143 1.414L16.1727 11H4v2h12.1723l-5.5868 5.5866 1.4141 1.4142 8.0012-8.0007z"/>
              </svg>
                                      <span class="hnf-btn__label">Просто жить осознанно</span>
                                    </span>
                                  </span>
                                </span>
                </div>
              </a>
            </div>
          </nav>
        </li>
        <li class="hnf-menu--highlight">
          <a data-tracking-label="aksessuary-dlya-sozdaniya-uyuta-pub5e50c6e1"
             href="https://www.ikea.com/ru/ru/campaigns/aksessuary-dlya-sozdaniya-uyuta-pub5e50c6e1" class="hnf-link">Аксессуары
            для создания уюта</a>
        </li>
        <li class="hnf-menu--highlight">
          <a data-tracking-label="offers/last-chance" href="https://www.ikea.com/ru/ru/offers/last-chance/"
             class="hnf-link">Выходит из ассортимента</a>
        </li>
      </ul>
      <div class="hnf-loading">
        <div class="hnf-loading__ball-wrapper" aria-hidden="true">
          <div class="hnf-loading__ball hnf-loading__ball--active"></div>
        </div>
        <a data-tracking-label="st001" href="https://www.ikea.com/ru/ru/cat/hranenie-i-poryadok-st001/"
           class="hnf-link">Хранение и порядок</a>
        <a data-tracking-label="fu001" href="https://www.ikea.com/ru/ru/cat/mebel-fu001/" class="hnf-link">Мебель</a>
        <a data-tracking-label="bm001" href="https://www.ikea.com/ru/ru/cat/krovati-i-matrasy-bm001/" class="hnf-link">Кровати
          и матрасы</a>
        <a data-tracking-label="ka001" href="https://www.ikea.com/ru/ru/cat/kuhnya-i-bytovaya-tehnika-ka001/"
           class="hnf-link">Кухни и бытовая техника</a>
        <a data-tracking-label="ba001" href="https://www.ikea.com/ru/ru/cat/vannaya-ba001/" class="hnf-link">Ванная</a>
        <a data-tracking-label="bc001" href="https://www.ikea.com/ru/ru/cat/detskie-tovary-bc001/" class="hnf-link">Детские
          товары</a>
        <a data-tracking-label="tl001" href="https://www.ikea.com/ru/ru/cat/tekstil-dlya-doma-tl001/" class="hnf-link">Текстиль
          для дома</a>
        <a data-tracking-label="rm001" href="https://www.ikea.com/ru/ru/cat/kovry-i-napolnye-pokrytiya-rm001/"
           class="hnf-link">Ковры и напольные покрытия</a>
        <a data-tracking-label="de001" href="https://www.ikea.com/ru/ru/cat/dekor-dlya-doma-de001/" class="hnf-link">Декор
          для дома</a>
        <a data-tracking-label="kt001" href="https://www.ikea.com/ru/ru/cat/posuda-kt001/" class="hnf-link">Посуда</a>
        <a data-tracking-label="li001" href="https://www.ikea.com/ru/ru/cat/osveshchenie-li001/" class="hnf-link">Освещение</a>
        <a data-tracking-label="lc001" href="https://www.ikea.com/ru/ru/cat/stirka-i-uborka-lc001/" class="hnf-link">Стирка
          и уборка</a>
        <a data-tracking-label="od001" href="https://www.ikea.com/ru/ru/cat/balkon-i-sad-od001/" class="hnf-link">Балкон
          и сад</a>
        <a data-tracking-label="pp001" href="https://www.ikea.com/ru/ru/cat/rasteniya-i-kashpo-pp001/" class="hnf-link">Растения
          и кашпо</a>
        <a data-tracking-label="he001" href="https://www.ikea.com/ru/ru/cat/elektronika-he001/" class="hnf-link">Электроника</a>
        <a data-tracking-label="hi001" href="https://www.ikea.com/ru/ru/cat/tovary-dlya-remonta-hi001/"
           class="hnf-link">Товары для ремонта</a>
        <a data-tracking-label="pt001" href="https://www.ikea.com/ru/ru/cat/tovary-dlya-zhivotnyh-pt001/"
           class="hnf-link">Товары для животных</a>
        <a data-tracking-label="fb001" href="https://www.ikea.com/ru/ru/cat/eda-v-ikea-fb001/" class="hnf-link">Еда в
          ИКЕА</a>
        <a data-tracking-label="ss001" href="https://www.ikea.com/ru/ru/cat/letnyaya-kollekciya-ss001/"
           class="hnf-link">Летняя коллекция</a>
        <a data-tracking-label="wt001" href="https://www.ikea.com/ru/ru/cat/zimnyaya-kollekciya-wt001/"
           class="hnf-link">Зимняя коллекция</a>
      </div>
    </nav>


    <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="1" aria-hidden="true">
      <span class="hnf-menu__heading">Комнаты</span>
      <ul data-tracking-label="rooms" data-menu-sub2-links class="hnf-menu__nav2__main hnf-menu__nav2__main--rooms-grid"
          aria-label="Подменю для Комнаты">
        <li>
          <a data-tracking-label="rooms/living-room" href="https://www.ikea.com/ru/ru/rooms/living-room/"
             class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Гостиная"
                                 data-src="https://www.ikea.com/images/seryi-divan-v-interere-gostinoi-3b41422c84f2a6ee5a1ec7d3424bcd5f.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Гостиная</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/bedroom" href="https://www.ikea.com/ru/ru/rooms/bedroom/" class="hnf-link"
             class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Спальня"
                                 data-src="https://www.ikea.com/images/spalnya-10b0be7a3743ab97d001c97d038aaf4d.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Спальня</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/kitchen" href="https://www.ikea.com/ru/ru/rooms/kitchen/" class="hnf-link"
             class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Кухня"
                                 data-src="https://www.ikea.com/images/kukhnya-otdelannaya-beloi-plitkoi-s-belymi-fasadami-veddinge-588d723470398c845beb548d470f9588.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Кухня</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/bathroom" href="https://www.ikea.com/ru/ru/rooms/bathroom/" class="hnf-link"
             class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Ванная"
                                 data-src="https://www.ikea.com/images/vannaya-oblicovannaya-zelenym-kafelem-belyi-shkaf-pod-rakovi-7e65306e9e5678708b55855391efa003.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Ванная</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/home-office" href="https://www.ikea.com/ru/ru/rooms/home-office/"
             class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Рабочий кабинет"
                                 data-src="https://www.ikea.com/images/kollazh-pismennyi-stol-stul-i-rabochie-tumby-20c617f0270ec76fe56315bdf860bdae.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Рабочий кабинет</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/childrens-room" href="https://www.ikea.com/ru/ru/rooms/childrens-room/"
             class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Детская"
                                 data-src="https://www.ikea.com/images/v-detskoi-spalne-stoit-svetlo-rozovaya-razdvizhnaya-krovat-m-0f25b8173637c10071923b684fee5729.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Детская</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/dining" href="https://www.ikea.com/ru/ru/rooms/dining/" class="hnf-link"
             class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Столовая"
                                 data-src="https://www.ikea.com/images/svetlaya-zona-stolovoi-obedennyi-stol-i-stulya-iz-bambuka-i--da1be86f56095b4a73cf57d1bfdc424f.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Столовая</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/hallway" href="https://www.ikea.com/ru/ru/rooms/hallway/" class="hnf-link"
             class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Прихожая"
                                 data-src="https://www.ikea.com/images/prikhozhaya-b2d7477bc49d086f8cbfe9bf9137cce7.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Прихожая</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="rooms/outdoor" href="https://www.ikea.com/ru/ru/rooms/outdoor/" class="hnf-link"
             class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Дача и сад"
                                 data-src="https://www.ikea.com/images/interer-na-otkrytoi-terasse-s-mebelyu-ikea-dlya-balkona-i-sa-0ff7f06a2ed64d083bdc842ed67276da.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Дача и сад</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="ikea-business" href="https://www.ikea.com/ru/ru/ikea-business/" class="hnf-link"
             target="_blank" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="ИКЕА для бизнеса"
                                 data-src="https://www.ikea.com/images/lyudi-sidyat-za-stolom-i-obsuzhdayut-biznes-voprosy-50cb79e6f6be14cd48de44699d2ec272.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">ИКЕА для бизнеса</span>
          </a>
        </li>
        <li>
          <a data-tracking-label="kladovka-pub75997b90" href="https://www.ikea.com/ru/ru/rooms/kladovka-pub75997b90"
             class="hnf-link" class="rooms-grid-gap">
                          <span class="mr-aspect-ratio-image mr-aspect-ratio-image--wide">
                            <img class="mr-aspect-ratio-image__image" alt="Кладовка"
                                 data-src="https://www.ikea.com/images/kladovka-2af793f9ef74e1d6fb1b9b84e44a023b.jpg?f=xxxs"/>
                          </span>
            <span class="mr__nav__title">Кладовка</span>
          </a>
        </li>
      </ul>
    </nav>


    <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="2" aria-hidden="true">
      <span class="hnf-menu__heading">Вдохновение</span>
      <ul data-tracking-label="Вдохновение" data-menu-sub3-links class="hnf-menu__nav2__main"
          aria-label="Подменю для Вдохновение">
        <li>
          <a data-tracking-label="ideas" href="https://www.ikea.com/ru/ru/ideas/" class="hnf-link">Идеи</a>
        </li>
        <li>
          <a data-tracking-label="campaigns/kvartiroteka" href="https://www.ikea.com/ru/ru/campaigns/kvartiroteka/#/"
             class="hnf-link" target="_blank">Бесплатные дизайн-проекты в Квартиротеке</a>
        </li>
        <li>
          <a data-tracking-label="gotovye-resheniya-dlya-doma-pub345a2410"
             href="https://www.ikea.com/ru/ru/rooms/gotovye-resheniya-dlya-doma-pub345a2410" class="hnf-link">Интерьеры
            комнат</a>
        </li>
        <li>
          <a data-tracking-label="https://ikea-family.ru/" href="https://ikea-family.ru/" class="hnf-link">Идеи наших
            покупателей</a>
        </li>
        <li>
          <a data-tracking-label="ideas/welcome-to-my-place"
             href="https://www.ikea.com/ru/ru/ideas/welcome-to-my-place/" class="hnf-link">Идеи наших сотрудников</a>
        </li>
        <li>
          <a data-tracking-label="https://publications-ru-ru.ikea.com/ikea_catalogue_21/page/1"
             href="https://publications-ru-ru.ikea.com/ikea_catalogue_21/page/1" class="hnf-link" target="_blank">Каталог
            2021</a>
        </li>
      </ul>
    </nav>


    <nav class="hnf-menu__nav2 hnf-menu__nav2--hidden" data-index="3" aria-hidden="true">
      <span class="hnf-menu__heading">Услуги</span>
      <ul data-tracking-label="Услуги" data-menu-sub3-links class="hnf-menu__nav2__main"
          aria-label="Подменю для Услуги">
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link"
             role="button">Доставка и самовывоз</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Доставка и самовывоз</span>
            <ul data-tracking-label="customer-service | customer-service" class="hnf-menu__nav3__main"
                aria-label="Подменю для Доставка и самовывоз">
              <li><a data-tracking-label="dostavka-iz-internet-magazina-pub30f2c801"
                     href="https://www.ikea.com/ru/ru/customer-service/dostavka-iz-internet-magazina-pub30f2c801"
                     class="hnf-link">Доставка и самовывоз</a></li>
              <li><a data-tracking-label="otslezhivanie-zakazov-pub02f35ca1"
                     href="https://www.ikea.com/ru/ru/customer-service/services/delivery/otslezhivanie-zakazov-pub02f35ca1"
                     class="hnf-link">Отслеживание заказа</a></li>
              <li><a data-tracking-label="cancellation" href="https://order.ikea.com/ru/ru/cancellation/"
                     class="hnf-link">Отмена заказа</a></li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link"
             role="button">Подготовка к покупке</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Подготовка к покупке</span>
            <ul data-tracking-label="customer-service | customer-service" class="hnf-menu__nav3__main"
                aria-label="Подменю для Подготовка к покупке">
              <li><a data-tracking-label="kak-proverit-nalichie-tovara-pub5dbbeda0"
                     href="https://www.ikea.com/ru/ru/customer-service/kak-proverit-nalichie-tovara-pub5dbbeda0"
                     class="hnf-link">Проверка наличия товара</a></li>
              <li><a data-tracking-label="vse-dlya-legkogo-shopinga-pub5bed75a1"
                     href="https://www.ikea.com/ru/ru/customer-service/shopping-at-ikea/vse-dlya-legkogo-shopinga-pub5bed75a1"
                     class="hnf-link">Все для легкого шопинга</a></li>
              <li><a data-tracking-label="customer-service/catalogues"
                     href="https://www.ikea.com/ru/ru/customer-service/catalogues/" class="hnf-link">Каталог и
                  брошюры</a></li>
              <li><a data-tracking-label="customer-service/shopping-at-ikea/smaland"
                     href="https://www.ikea.com/ru/ru/customer-service/shopping-at-ikea/smaland/" class="hnf-link">ИКЕА
                  для детей</a></li>
              <li><a data-tracking-label="nasladites-tradicionnoi-shvedskoi-kukhnei-v-restorane-ikea-pubf6ba78e1"
                     href="https://www.ikea.com/ru/ru/customer-service/shopping-at-ikea/nasladites-tradicionnoi-shvedskoi-kukhnei-v-restorane-ikea-pubf6ba78e1"
                     class="hnf-link">Ресторан, бистро и Магазин Шведские продукты</a></li>
              <li><a data-tracking-label="rassrochka-pube9cc84b1"
                     href="https://www.ikea.com/ru/ru/customer-service/services/finance-options/rassrochka-pube9cc84b1"
                     class="hnf-link">Рассрочка и кредит</a></li>
              <li><a data-tracking-label="customer-service/faq" href="https://www.ikea.com/ru/ru/customer-service/faq/"
                     class="hnf-link">Вопросы и ответы</a></li>
              <li><a data-tracking-label="customer-service/contact-us"
                     href="https://www.ikea.com/ru/ru/customer-service/contact-us/" class="hnf-link">Как связаться с
                  ИКЕА</a></li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link"
             role="button">Планирование и обустройство дома</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Планирование  и обустройство дома</span>
            <ul data-tracking-label="customer-service | customer-service" class="hnf-menu__nav3__main"
                aria-label="Подменю для Планирование  и обустройство дома">
              <li><a data-tracking-label="zamer-pomesheniya-pubc059c302"
                     href="https://www.ikea.com/ru/ru/customer-service/services/zamer-pomesheniya-pubc059c302"
                     class="hnf-link">Замер помещения</a></li>
              <li><a data-tracking-label="campaigns/kvartiroteka"
                     href="https://www.ikea.com/ru/ru/campaigns/kvartiroteka/#/" class="hnf-link">Бесплатные
                  дизайн-проекты в Квартиротеке</a></li>
              <li><a data-tracking-label="planirovanie-pubb82b8cd1"
                     href="https://www.ikea.com/ru/ru/customer-service/planirovanie-pubb82b8cd1" class="hnf-link">Планирование
                  мебели</a></li>
              <li><a data-tracking-label="consult-pub1b4bb1a1"
                     href="https://www.ikea.com/ru/ru/customer-service/consult-pub1b4bb1a1" class="hnf-link">Дизайн-сервис
                  ИКЕА</a></li>
              <li><a data-tracking-label="rassrochka-pube9cc84b1"
                     href="https://www.ikea.com/ru/ru/customer-service/services/finance-options/rassrochka-pube9cc84b1"
                     class="hnf-link">Кредит в ИКЕА</a></li>
              <li><a data-tracking-label="poshiv-izdelii-pubf6770921"
                     href="https://www.ikea.com/ru/ru/customer-service/services/poshiv-izdelii-pubf6770921"
                     class="hnf-link">Пошив изделий из тканей ИКЕА</a></li>
              <li><a data-tracking-label="pechat-fotografii-pube875ab00"
                     href="https://www.ikea.com/ru/ru/customer-service/pechat-fotografii-pube875ab00" class="hnf-link">Печать
                  фотографий</a></li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="customer-service/services/assembly"
             href="https://www.ikea.com/ru/ru/customer-service/services/assembly/" class="hnf-link" role="button">Сборка
            и установка</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Сборка и установка</span>
            <ul data-tracking-label="customer-service | customer-service/services/assembly" class="hnf-menu__nav3__main"
                aria-label="Подменю для Сборка и установка">
              <li><a data-tracking-label="zamer-pomesheniya-pubc059c302"
                     href="https://www.ikea.com/ru/ru/customer-service/services/zamer-pomesheniya-pubc059c302"
                     class="hnf-link">Замер помещения</a></li>
              <li><a data-tracking-label="customer-service/services/assembly"
                     href="https://www.ikea.com/ru/ru/customer-service/services/assembly/" class="hnf-link">Сборка и
                  установка</a></li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link"
             role="button">Оплата</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Оплата</span>
            <ul data-tracking-label="customer-service | customer-service" class="hnf-menu__nav3__main"
                aria-label="Подменю для Оплата">
              <li><a data-tracking-label="oplachivaite-pokupki-legko-i-udobno-pubb1535691"
                     href="https://www.ikea.com/ru/ru/customer-service/shopping-at-ikea/oplachivaite-pokupki-legko-i-udobno-pubb1535691"
                     class="hnf-link">Способы оплаты</a></li>
              <li><a data-tracking-label="kredit-v-ikea-pub1a0ad620"
                     href="https://www.ikea.com/ru/ru/customer-service/services/finance-options/kredit-v-ikea-pub1a0ad620"
                     class="hnf-link">Рассрочка и кредит</a></li>
              <li><a data-tracking-label="podarochnaya-karta-pub64a734f1"
                     href="https://www.ikea.com/ru/ru/customer-service/services/finance-options/podarochnaya-karta-pub64a734f1"
                     class="hnf-link">Подарочная карта ИКЕА</a></li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link"
             role="button">Обмен и возврат</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Обмен и возврат</span>
            <ul data-tracking-label="customer-service | customer-service" class="hnf-menu__nav3__main"
                aria-label="Подменю для Обмен и возврат">
              <li><a data-tracking-label="customer-service/returns-claims"
                     href="https://www.ikea.com/ru/ru/customer-service/returns-claims/" class="hnf-link">Обмен и
                  возврат</a></li>
              <li><a data-tracking-label="customer-service/returns-claims/guarantee"
                     href="https://www.ikea.com/ru/ru/customer-service/returns-claims/guarantee/" class="hnf-link">Гарантия</a>
              </li>
              <li><a data-tracking-label="zayavka-na-remont-bytovoi-tekhniki-pubb6362fb1"
                     href="https://www.ikea.com/ru/ru/customer-service/returns-claims/zayavka-na-remont-bytovoi-tekhniki-pubb6362fb1"
                     class="hnf-link">Заявка на ремонт</a></li>
              <li><a data-tracking-label="zakazat-nedostayushuyu-detal-pub27420fd0"
                     href="https://www.ikea.com/ru/ru/this-is-ikea/sustainable-everyday/zakazat-nedostayushuyu-detal-pub27420fd0"
                     class="hnf-link">Заказ запчастей и фурнитуры</a></li>
              <li><a data-tracking-label="customer-service/contact-us"
                     href="https://www.ikea.com/ru/ru/customer-service/contact-us/" class="hnf-link">Как связаться с
                  ИКЕА</a></li>
              <li><a data-tracking-label="customer-service/buy-back"
                     href="https://www.ikea.com/ru/ru/customer-service/buy-back/" class="hnf-link">Выкуп мебели ИКЕА</a>
              </li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/" class="hnf-link"
             role="button">Заботимся о людях и планете</a>
          <nav class="hnf-menu__nav3 hnf-menu__nav3--hidden" aria-hidden="true">
            <span class="hnf-menu__heading">Заботимся о людях и планете</span>
            <ul data-tracking-label="customer-service | customer-service" class="hnf-menu__nav3__main"
                aria-label="Подменю для Заботимся о людях и планете">
              <li><a data-tracking-label="zakazat-nedostayushuyu-detal-pub27420fd0"
                     href="https://www.ikea.com/ru/ru/this-is-ikea/sustainable-everyday/zakazat-nedostayushuyu-detal-pub27420fd0"
                     class="hnf-link">Заказ фурнитуры</a></li>
              <li><a data-tracking-label="https://youdo.com/IKEA" href="https://youdo.com/IKEA" class="hnf-link"
                     target="_blank">Ремонт/обновление мебели (YouDo)</a></li>
              <li><a data-tracking-label="customer-service/buy-back"
                     href="https://www.ikea.com/ru/ru/customer-service/buy-back/" class="hnf-link">Выкуп мебели ИКЕА</a>
              </li>
              <li><a data-tracking-label="https://ikea-family.ru/ads"
                     href="https://ikea-family.ru/ads?_ga&#x3D;2.177297511.315556877.1600170491-1787946151.1600170491"
                     class="hnf-link" target="_blank">Доска объявлений по продаже или покупке мебели ИКЕА</a></li>
              <li><a data-tracking-label="pererabotka-lamp-i-batareek-pub4f856940"
                     href="https://www.ikea.com/ru/ru/customer-service/services/pererabotka-lamp-i-batareek-pub4f856940"
                     class="hnf-link">Переработка лампочек и батареек</a></li>
              <li><a data-tracking-label="pererabotka-tekstilya-pub217fed90"
                     href="https://www.ikea.com/ru/ru/customer-service/services/pererabotka-tekstilya-pub217fed90"
                     class="hnf-link">Переработка текстиля</a></li>
              <li><a data-tracking-label="customer-service/charity"
                     href="https://www.ikea.com/ru/ru/customer-service/charity/#/" class="hnf-link">Передача мебели на
                  благотворительность</a></li>
              <li><a data-tracking-label="furniture-take-back-pub4ca978c0"
                     href="https://www.ikea.com/ru/ru/this-is-ikea/sustainable-everyday/circular-concept/furniture-take-back-pub4ca978c0"
                     class="hnf-link">Прием мебели на переработку</a></li>
            </ul>
          </nav>
        </li>
        <li>
          <a data-tracking-label="ikea-business" href="https://www.ikea.com/ru/ru/ikea-business/" class="hnf-link">ИКЕА
            для бизнеса</a>
        </li>
        <li>
          <a data-tracking-label="customer-service" href="https://www.ikea.com/ru/ru/customer-service/"
             class="hnf-link">Все услуги и информация</a>
        </li>
        <li>
          <a data-tracking-label="customer-service/terms-conditions"
             href="https://www.ikea.com/ru/ru/customer-service/terms-conditions/" class="hnf-link">Правила и
            положения</a>
        </li>
      </ul>
    </nav>


    <div class="hnf-menu__rv hnf-menu__rv--hidden" aria-hidden="true">
      <span class="hnf-menu__rv__heading">Вы недавно смотрели:</span>
      <div class="hnf-menu__rv__list"></div>
    </div>
  </div>

  <div class="hnf-menu__alternate">
    <a class="hnf-link hnf-btn hnf-btn-change-country" href="https://www.ikea.com">
      <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn-change-country__globe" width="24" height="24"
           viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd"
              d="M13.7467 18.1766C12.9482 19.7737 12.2151 20 12 20c-.2151 0-.9482-.2263-1.7467-1.8234-.3065-.6131-.5745-1.3473-.7831-2.1766h5.0596c-.2086.8293-.4766 1.5635-.7831 2.1766zM14.8885 14h-5.777A17.7354 17.7354 0 0 1 9 12c0-.6949.0392-1.3641.1115-2h5.777c.0723.6359.1115 1.3051.1115 2 0 .6949-.0392 1.3641-.1115 2zm1.6955 2c-.2658 1.2166-.6492 2.307-1.1213 3.2138A8.0347 8.0347 0 0 0 18.9297 16H16.584zm3.164-2H16.9c.0656-.6462.1-1.3151.1-2 0-.6849-.0344-1.3538-.1-2h2.848A8.0156 8.0156 0 0 1 20 12a8.0156 8.0156 0 0 1-.252 2zm-.8183-6a8.035 8.035 0 0 0-3.467-3.2138c.4721.9068.8555 1.9972 1.1213 3.2138h2.3457zm-4.3999 0c-.2086-.8293-.4766-1.5635-.7831-2.1766C12.9482 4.2264 12.2151 4 12 4c-.2151 0-.9482.2263-1.7467 1.8234-.3065.613-.5745 1.3473-.7831 2.1766h5.0596zM7.416 8c.2658-1.2166.6491-2.307 1.1213-3.2138A8.035 8.035 0 0 0 5.0703 8H7.416zm-3.164 2A8.0147 8.0147 0 0 0 4 12c0 .6906.0875 1.3608.252 2H7.1a19.829 19.829 0 0 1-.1-2c0-.6849.0344-1.3538.1-2H4.252zm3.164 6H5.0704a8.0347 8.0347 0 0 0 3.467 3.2138C8.0651 18.307 7.6818 17.2166 7.4161 16zM22 12c0-5.5229-4.4772-10-10-10C6.4771 2 2 6.4771 2 12c0 5.5228 4.4771 10 10 10 5.5228 0 10-4.4772 10-10z"/>
      </svg>
      Выбрать страну
    </a>
  </div>
</aside>
<div class="hnf-overlay"></div>
<div id="hnf-content" tabindex="-1"></div>
<script type="application/json" id="plp-page-meta-data">{
    "correctUrl": "/ru/ru/cat/kuhonnye-polotenca-18851/"
  }</script>
<div class="plp-page-container">
  <div class="plp-page-container__inner">
    <div class="plp-page-container__main"><a href="#product-list-skip" class="plp-skip-to-product-list">Перейти к списку
        товаров</a>
      <div class="bc-breadcrumb ">
        <nav role="navigation" aria-label="Breadcrumb" class="bc-breadcrumb__nav">
          <ol class="bc-breadcrumb__list" data-breadcrumb-links>
            <li class="bc-breadcrumb__list-item">
              <a href="https://www.ikea.com/ru/ru/cat/tovary-products/"
                 class="bc-breadcrumb__link bc-link bc-link--black"
                 data-tracking-label="products"><span>Товары</span></a>
            </li>
            <li class="bc-breadcrumb__list-item">
              <a href="https://www.ikea.com/ru/ru/cat/posuda-kt001/" class="bc-breadcrumb__link bc-link bc-link--black"
                 data-tracking-label="products | kt001"><span>Посуда</span></a>
            </li>
            <li class="bc-breadcrumb__list-item">
              <a href="https://www.ikea.com/ru/ru/cat/tekstil-dlya-kuhni-18850/"
                 class="bc-breadcrumb__link bc-link bc-link--black"
                 data-tracking-label="products | kt001 | 18850"><span>Текстиль для кухни</span></a>
            </li>
            <li class="bc-breadcrumb__list-item">
              <a href="https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/"
                 class="bc-breadcrumb__link bc-link bc-link--black"
                 data-tracking-label="products | kt001 | 18850 | 18851"><span>Кухонные полотенца</span></a>
            </li>

          </ol>
        </nav>
      </div>


      <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [
            {
              "@type": "ListItem",
              "position": 1,
              "name": "Товары",
              "item": "https://www.ikea.com/ru/ru/cat/tovary-products/"
            },
            {
              "@type": "ListItem",
              "position": 2,
              "name": "Посуда",
              "item": "https://www.ikea.com/ru/ru/cat/posuda-kt001/"
            },
            {
              "@type": "ListItem",
              "position": 3,
              "name": "Текстиль для кухни",
              "item": "https://www.ikea.com/ru/ru/cat/tekstil-dlya-kuhni-18850/"
            },
            {
              "@type": "ListItem",
              "position": 4,
              "name": "Кухонные полотенца",
              "item": "https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/"
            }
          ]
        }
      </script>
      <div class="plp-page-title"><h1 class="plp-page-title__title">Кухонные полотенца</h1></div>
      <div data-range-slot="range-introduction" class="plp-range-introduction-slot-wrapper">
        <div class="pub__designSystemBase" data-pub-ga="">
          <div class="gd8xc1c v1gpv7o0 v18by0fb">
            <section class="gd8xc1c v19w64p3 v18by0fb">
              <div id="2be47550-fbac-11e9-b4b8-15d8c805cdf3">
                <div data-pub-id="2be47550-fbac-11e9-b4b8-15d8c805cdf3" data-pub-type="text"
                     class="c1lpg3h5 i1ycpxq9 pub__designSystemText t91kxqv w1fdzi2f">
                  <p>Наши кухонные полотенца можно использовать, чтобы насухо вытирать <a
                      href="https://www.ikea.com/ru/ru/cat/stolovaya-posuda-18860/">столовую посуду</a> и <a
                      href="https://www.ikea.com/ru/ru/cat/kastryuli-i-kovshi-20633/">кастрюли</a>. Они практичные и
                    совсем не скучные. Цветные полотенца с узорами и принтами помогут придать вашей кухне неповторимый
                    облик.</p>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <a name="product-list-skip"></a>
      <div class="plp-main-container">
        <div class="js-product-list"
             data-category="{&quot;id&quot;:&quot;18851&quot;,&quot;canonical&quot;:&quot;https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/&quot;,&quot;enableRoomView&quot;:false,&quot;type&quot;:&quot;chapter&quot;,&quot;totalCount&quot;:11}"
             data-custom-compact="{&quot;source&quot;:&quot;plp&quot;}">
          <div class="catalog-product-list">
            <div class="plp-filter plp-clearfix">
              <div class="plp-filter-bar">
                <div class="plp-overflow-carousel">
                  <button type="button"
                          class="plp-btn plp-btn--xsmall plp-btn--icon-primary-inverse plp-overflow-carousel__button plp-overflow-carousel__button--hidden"
                          id="plp-overflow-carousel__default-prev-btn"
                          aria-controls="plp-overflow-carousel__default-prev-btn" aria-label="See previous items"><span
                      class="plp-btn__inner"><svg focusable="false" viewBox="0 0 24 24"
                                                  class="plp-svg-icon plp-btn__icon" aria-hidden="true"><path
                          fill-rule="evenodd" clip-rule="evenodd"
                          d="m8.4004 12.0007 5.785 5.7857 1.4143-1.4141-4.3711-4.3716 4.3711-4.3717-1.4143-1.4142-5.785 5.7859z"></path></svg></span>
                  </button>
                  <div class="plp-overflow-carousel__content">
                    <button type="button" disabled=""
                            class="plp-btn plp-btn--small plp-btn--secondary plp-compare-button"
                            aria-label="Активировать выбор сравнения товаров"><span class="plp-btn__inner"><span
                          class="plp-btn__label">Сравнить</span></span></button>
                  </div>
                  <button type="button"
                          class="plp-btn plp-btn--xsmall plp-btn--icon-primary-inverse plp-overflow-carousel__button plp-overflow-carousel__button--right plp-overflow-carousel__button--hidden"
                          id="plp-overflow-carousel__default-next-btn"
                          aria-controls="plp-overflow-carousel__default-next-btn" aria-label="See next items"><span
                      class="plp-btn__inner"><svg focusable="false" viewBox="0 0 24 24"
                                                  class="plp-svg-icon plp-btn__icon" aria-hidden="true"><path
                          fill-rule="evenodd" clip-rule="evenodd"
                          d="m15.5996 12.0007-5.785 5.7857-1.4143-1.4141 4.3711-4.3716L8.4003 7.629l1.4143-1.4142 5.785 5.7859z"></path></svg></span>
                  </button>
                </div>
              </div>
              <div class="plp-filter-information"><span
                  class="plp-filter-information__total-count">11 товаров(-а)</span>
                <div class="plp-filter-information__text-toggle" aria-label="Вид изображения">
                  <div class="plp-toggle plp-filter-information__button" type="transparent" aria-label="Товары">
                    <button class="plp-toggle__button plp-toggle__button--active" aria-pressed="true"><span
                        class="plp-toggle__button-label">Товары</span></button>
                    <button class="plp-toggle__button" aria-pressed="false"><span class="plp-toggle__button-label">В интерьере</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="plp-product-list">
              <div class="plp-product-list__products">
                <div class="plp-fragment-wrapper">
                  <div class="range-revamp-product-compact" data-ref-id="60476349" data-product-number="60476349"
                       data-product-type="ART" data-price="199" data-currency="RUB" data-product-name="RINNIG РИННИГ"
                       data-product-compact="" data-cs-capture="">
                    <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                            aria-pressed="false" aria-label="Добавить в список покупок">
                      <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                      </svg>
                    </button>
                    <a
                      href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom-60476349/"
                      aria-label="RINNIG РИННИГ - Полотенце кухонное">
                      <div class="range-revamp-product-compact__image-wrapper"><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image"
                            alt="RINNIG РИННИГ Полотенце кухонное, белый/темно-серый/с рисунком, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=xxs"/></span><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                            sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                            data-src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=xxs"
                            data-src-set="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=u 80w
      "/></span></div>
                    </a>
                    <div class="range-revamp-product-compact__bottom-wrapper"><a
                        href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom-60476349/">
                        <div class="range-revamp-compact-price-package">
                          <div class="range-revamp-compact-price-package__additional-info">
                            <div class="range-revamp-header-section">
                              <div class="range-revamp-header-section__title--small notranslate">RINNIG РИННИГ</div>
                              <div class="range-revamp-header-section__description"><span
                                  class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                  class="range-revamp-header-section__description-measurement"
                                  data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                              </div>
                            </div>
                          </div>
                          <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                          <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">199</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 4 шт</span></span>
                          </div>
                          <span class="range-revamp-compact-price-package__variations-disclaimer"
                                data-product-compact-gpr="">Другие варианты</span></div>
                      </a>
                      <button type="button"
                              class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                              aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg
                            focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon range-revamp-btn__icon"
                            aria-hidden="true"><path fill-rule="evenodd" clip-rule="evenodd"
                                                     d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="plp-fragment-wrapper">
                  <div class="range-revamp-product-compact" data-ref-id="70476358" data-product-number="70476358"
                       data-product-type="ART" data-price="229" data-currency="RUB" data-product-name="RINNIG РИННИГ"
                       data-product-compact="" data-cs-capture="">
                    <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                            aria-pressed="false" aria-label="Добавить в список покупок">
                      <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                      </svg>
                    </button>
                    <a
                      href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom-70476358/"
                      aria-label="RINNIG РИННИГ - Полотенце кухонное">
                      <div class="range-revamp-product-compact__image-wrapper"><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image"
                            alt="RINNIG РИННИГ Полотенце кухонное, бел/зелен/с рисунком, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=xxs"/></span><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                            sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                            data-src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=xxs"
                            data-src-set="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=u 80w
      "/></span></div>
                    </a>
                    <div class="range-revamp-product-compact__bottom-wrapper"><a
                        href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom-70476358/">
                        <div class="range-revamp-compact-price-package">
                          <div class="range-revamp-compact-price-package__additional-info">
                            <div class="range-revamp-header-section">
                              <div class="range-revamp-header-section__title--small notranslate">RINNIG РИННИГ</div>
                              <div class="range-revamp-header-section__description"><span
                                  class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                  class="range-revamp-header-section__description-measurement"
                                  data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                              </div>
                            </div>
                          </div>
                          <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                          <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">229</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 4 шт</span></span>
                          </div>
                          <span class="range-revamp-compact-price-package__variations-disclaimer"
                                data-product-compact-gpr="">Другие варианты</span></div>
                      </a>
                      <button type="button"
                              class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                              aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg
                            focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon range-revamp-btn__icon"
                            aria-hidden="true"><path fill-rule="evenodd" clip-rule="evenodd"
                                                     d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="plp-fragment-wrapper">
                  <div class="range-revamp-product-compact" data-ref-id="60484014" data-product-number="60484014"
                       data-product-type="ART" data-price="39" data-currency="RUB"
                       data-product-name="HILDEGUN ХИЛЬДЕГУН" data-product-compact="" data-cs-capture="">
                    <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                            aria-pressed="false" aria-label="Добавить в список покупок">
                      <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                      </svg>
                    </button>
                    <a href="https://www.ikea.com/ru/ru/p/hildegun-hildegun-polotence-kuhonnoe-krasnyy-60484014/"
                       aria-label="HILDEGUN ХИЛЬДЕГУН - Полотенце кухонное">
                      <div class="range-revamp-product-compact__image-wrapper"><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image"
                            alt="HILDEGUN ХИЛЬДЕГУН Полотенце кухонное, красный, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=xxs"/></span><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                            sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                            data-src="https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=xxs"
                            data-src-set="
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=u 80w
      "/></span></div>
                    </a>
                    <div class="range-revamp-product-compact__bottom-wrapper"><a
                        href="https://www.ikea.com/ru/ru/p/hildegun-hildegun-polotence-kuhonnoe-krasnyy-60484014/">
                        <div class="range-revamp-compact-price-package">
                          <div class="range-revamp-compact-price-package__additional-info">
                            <div class="range-revamp-header-section">
                              <div class="range-revamp-header-section__title--small notranslate">HILDEGUN ХИЛЬДЕГУН
                              </div>
                              <div class="range-revamp-header-section__description"><span
                                  class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                  class="range-revamp-header-section__description-measurement"
                                  data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                              </div>
                            </div>
                          </div>
                          <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                          <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    range-revamp-price--highlight
    
    
    
  "><span class="range-revamp-price__integer">39</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span></span></div>
                        </div>
                      </a>
                      <button type="button"
                              class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                              aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg
                            focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon range-revamp-btn__icon"
                            aria-hidden="true"><path fill-rule="evenodd" clip-rule="evenodd"
                                                     d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="plp-fragment-wrapper">
                  <div class="range-revamp-product-compact" data-ref-id="70464416" data-product-number="70464416"
                       data-product-type="ART" data-price="299" data-currency="RUB"
                       data-product-name="SANDVIVA САНДВИВА" data-product-compact="" data-cs-capture="">
                    <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                            aria-pressed="false" aria-label="Добавить в список покупок">
                      <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                      </svg>
                    </button>
                    <a href="https://www.ikea.com/ru/ru/p/sandviva-sandviva-polotence-kuhonnoe-siniy-70464416/"
                       aria-label="SANDVIVA САНДВИВА - Полотенце кухонное">
                      <div class="range-revamp-product-compact__image-wrapper"><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image"
                            alt="SANDVIVA САНДВИВА Полотенце кухонное, синий, 40x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=xxs"/></span><span
                          class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                            loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                            sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                            src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                            data-src="https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=xxs"
                            data-src-set="
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=u 80w
      "/></span></div>
                    </a>
                    <div class="range-revamp-product-compact__bottom-wrapper"><a
                        href="https://www.ikea.com/ru/ru/p/sandviva-sandviva-polotence-kuhonnoe-siniy-70464416/">
                        <div class="range-revamp-compact-price-package">
                          <div class="range-revamp-compact-price-package__additional-info">
                            <div class="range-revamp-header-section">
                              <div class="range-revamp-header-section__title--small notranslate">SANDVIVA САНДВИВА</div>
                              <div class="range-revamp-header-section__description"><span
                                  class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                  class="range-revamp-header-section__description-measurement"
                                  data-ga-action="measurements_header_click" data-ga-label="40x60 см">40x60 см</span>
                              </div>
                            </div>
                          </div>
                          <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                          <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">299</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 2 шт</span></span>
                          </div>
                        </div>
                      </a>
                      <button type="button"
                              class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                              aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg
                            focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon range-revamp-btn__icon"
                            aria-hidden="true"><path fill-rule="evenodd" clip-rule="evenodd"
                                                     d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
                <div class="plp-product-card-placeholder">
                  <div class="plp-product-card-placeholder__image"></div>
                  <div class="plp-product-card-placeholder__name"></div>
                  <div class="plp-product-card-placeholder__label"></div>
                  <div class="plp-product-card-placeholder__price"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <noscript class="noscript-compact-fragments">
          <div class="plp-product-list">
            <div class="plp-product-list__products">
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="60476349" data-product-number="60476349"
                     data-product-type="ART" data-price="199" data-currency="RUB" data-product-name="RINNIG РИННИГ"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a
                    href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom-60476349/"
                    aria-label="RINNIG РИННИГ - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="RINNIG РИННИГ Полотенце кухонное, белый/темно-серый/с рисунком, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0813216_pe772338_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom__0918268_pe786441_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-belyy-temno-seryy-s-risunkom-60476349/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">RINNIG РИННИГ</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">199</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 4 шт</span></span>
                        </div>
                        <span class="range-revamp-compact-price-package__variations-disclaimer"
                              data-product-compact-gpr="">Другие варианты</span></div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="70476358" data-product-number="70476358"
                     data-product-type="ART" data-price="229" data-currency="RUB" data-product-name="RINNIG РИННИГ"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom-70476358/"
                     aria-label="RINNIG РИННИГ - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="RINNIG РИННИГ Полотенце кухонное, бел/зелен/с рисунком, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0813215_pe772339_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom__0918272_pe786442_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/rinnig-rinnig-polotence-kuhonnoe-bel-zelen-s-risunkom-70476358/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">RINNIG РИННИГ</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">229</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 4 шт</span></span>
                        </div>
                        <span class="range-revamp-compact-price-package__variations-disclaimer"
                              data-product-compact-gpr="">Другие варианты</span></div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="60484014" data-product-number="60484014"
                     data-product-type="ART" data-price="39" data-currency="RUB" data-product-name="HILDEGUN ХИЛЬДЕГУН"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/hildegun-hildegun-polotence-kuhonnoe-krasnyy-60484014/"
                     aria-label="HILDEGUN ХИЛЬДЕГУН - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="HILDEGUN ХИЛЬДЕГУН Полотенце кухонное, красный, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956033_pe804444_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/hildegun-polotence-kuhonnoe-krasnyy__0956034_pe804445_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/hildegun-hildegun-polotence-kuhonnoe-krasnyy-60484014/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">HILDEGUN ХИЛЬДЕГУН</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    range-revamp-price--highlight
    
    
    
  "><span class="range-revamp-price__integer">39</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span></span></div>
                      </div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="70464416" data-product-number="70464416"
                     data-product-type="ART" data-price="299" data-currency="RUB" data-product-name="SANDVIVA САНДВИВА"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/sandviva-sandviva-polotence-kuhonnoe-siniy-70464416/"
                     aria-label="SANDVIVA САНДВИВА - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="SANDVIVA САНДВИВА Полотенце кухонное, синий, 40x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0789504_pe764006_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893540_pe782342_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/sandviva-sandviva-polotence-kuhonnoe-siniy-70464416/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">SANDVIVA САНДВИВА</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="40x60 см">40x60 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">299</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 2 шт</span></span>
                        </div>
                      </div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="50493047" data-product-number="50493047"
                     data-product-type="ART" data-price="249" data-currency="RUB" data-product-name="TORVFLY ТОРВФЛЮ"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a
                    href="https://www.ikea.com/ru/ru/p/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy-50493047/"
                    aria-label="TORVFLY ТОРВФЛЮ - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="TORVFLY ТОРВФЛЮ Полотенце кухонное, с рисунком/оранжевый, 45x60 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956226_pe804626_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956226_pe804626_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956226_pe804626_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956226_pe804626_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956226_pe804626_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956227_pe804628_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956227_pe804628_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956227_pe804628_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956227_pe804628_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy__0956227_pe804628_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-oranzhevyy-50493047/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__product-highlights"><span
                            class="range-revamp-product-highlight range-revamp-product-highlight__new">Новинка</span>
                        </div>
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">TORVFLY ТОРВФЛЮ</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="45x60 см">45x60 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">249</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 4 шт</span></span>
                        </div>
                      </div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="00174514" data-product-number="00174514"
                     data-product-type="ART" data-price="149" data-currency="RUB" data-product-name="ANNSOFI АННСОФИ"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/annsofi-annsofi-polotence-kuhonnoe-raznye-cveta-00174514/"
                     aria-label="ANNSOFI АННСОФИ - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="ANNSOFI АННСОФИ Полотенце кухонное, разные цвета, 50x70 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/annsofi-polotence-kuhonnoe-raznye-cveta__0328835_pe519760_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/annsofi-polotence-kuhonnoe-raznye-cveta__0328835_pe519760_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/annsofi-polotence-kuhonnoe-raznye-cveta__0328835_pe519760_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/annsofi-polotence-kuhonnoe-raznye-cveta__0328835_pe519760_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/annsofi-polotence-kuhonnoe-raznye-cveta__0328835_pe519760_s5.jpg?f=xxs"/></span>
                    </div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/annsofi-annsofi-polotence-kuhonnoe-raznye-cveta-00174514/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">ANNSOFI АННСОФИ</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="50x70 см">50x70 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">149</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span></span></div>
                      </div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="40479594" data-product-number="40479594"
                     data-product-type="ART" data-price="699" data-currency="RUB"
                     data-product-name="MARIATHERES МАРИАТЕРЕС" data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/mariatheres-mariateres-polotence-kuhonnoe-seryy-40479594/"
                     aria-label="MARIATHERES МАРИАТЕРЕС - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="MARIATHERES МАРИАТЕРЕС Полотенце кухонное, серый, 50x70 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918026_pe786094_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918026_pe786094_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918026_pe786094_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918026_pe786094_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918026_pe786094_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918027_pe786120_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918027_pe786120_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918027_pe786120_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918027_pe786120_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-seryy__0918027_pe786120_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/mariatheres-mariateres-polotence-kuhonnoe-seryy-40479594/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">MARIATHERES МАРИАТЕРЕС
                            </div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="50x70 см">50x70 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">699</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 2 шт</span></span>
                        </div>
                      </div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="60464389" data-product-number="60464389"
                     data-product-type="ART" data-price="219" data-currency="RUB" data-product-name="SANDVIVA САНДВИВА"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/sandviva-sandviva-polotence-kuhonnoe-siniy-60464389/"
                     aria-label="SANDVIVA САНДВИВА - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="SANDVIVA САНДВИВА Полотенце кухонное, синий, 35x35 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0790746_pe764371_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0790746_pe764371_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0790746_pe764371_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0790746_pe764371_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0790746_pe764371_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893538_pe782338_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893538_pe782338_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893538_pe782338_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893538_pe782338_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/sandviva-polotence-kuhonnoe-siniy__0893538_pe782338_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/sandviva-sandviva-polotence-kuhonnoe-siniy-60464389/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">SANDVIVA САНДВИВА</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="35x35 см">35x35 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">219</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 2 шт</span></span>
                        </div>
                      </div>
                    </a>
                    <button type="button"
                            class="range-revamp-btn range-revamp-btn--small range-revamp-btn--icon-emphasised range-revamp-product-compact__add-to-cart-button"
                            aria-label="Добавить в корзину"><span class="range-revamp-btn__inner"><svg focusable="false"
                                                                                                       viewBox="0 0 24 24"
                                                                                                       class="range-revamp-svg-icon range-revamp-btn__icon"
                                                                                                       aria-hidden="true"><path
                            fill-rule="evenodd" clip-rule="evenodd"
                            d="M10.4372 4H10.9993H12.0003H12.9996H13.5616L13.8538 4.48014L17.2112 9.99713H21H22.2806L21.9702 11.2396L21.5303 13H19.4688L19.7194 11.9971H4.28079L5.59143 17.2397C5.70272 17.6848 6.1027 17.9971 6.56157 17.9971H15V19.9971H6.56157C5.18496 19.9971 3.98502 19.0602 3.65114 17.7247L2.02987 11.2397L1.71924 9.99713H3.00002H6.78793L10.145 4.48017L10.4372 4ZM12.4375 6L14.87 9.99713H9.12911L11.5614 6H12.0003H12.4375ZM17.9961 16V14H19.9961V16H21.9961V18H19.9961V20H17.9961V18H15.9961V16H17.9961Z"></path></svg></span>
                    </button>
                  </div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="80365331" data-product-number="80365331"
                     data-product-type="ART" data-price="399" data-currency="RUB" data-product-name="TROLLPIL ТРОЛЛЬПИЛ"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a href="https://www.ikea.com/ru/ru/p/trollpil-trollpil-polotence-kuhonnoe-belyy-zelenyy-80365331/"
                     aria-label="TROLLPIL ТРОЛЛЬПИЛ - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="TROLLPIL ТРОЛЛЬПИЛ Полотенце кухонное, белый/зеленый, 50x70 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0596170_pe676459_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0596170_pe676459_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0596170_pe676459_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0596170_pe676459_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0596170_pe676459_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0895197_pe676461_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0895197_pe676461_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0895197_pe676461_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0895197_pe676461_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/trollpil-polotence-kuhonnoe-belyy-zelenyy__0895197_pe676461_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/trollpil-trollpil-polotence-kuhonnoe-belyy-zelenyy-80365331/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">TROLLPIL ТРОЛЛЬПИЛ</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="50x70 см">50x70 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">399</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 2 шт</span></span>
                        </div>
                        <span class="range-revamp-compact-price-package__last-chance">Выходит из ассортимента</span>
                      </div>
                    </a></div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="30493067" data-product-number="30493067"
                     data-product-type="ART" data-price="229" data-currency="RUB" data-product-name="TORVFLY ТОРВФЛЮ"
                     data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a
                    href="https://www.ikea.com/ru/ru/p/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy-30493067/"
                    aria-label="TORVFLY ТОРВФЛЮ - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="TORVFLY ТОРВФЛЮ Полотенце кухонное, с рисунком/зеленый, 30x40 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956222_pe804622_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956222_pe804622_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956222_pe804622_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956222_pe804622_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956222_pe804622_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956223_pe804623_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956223_pe804623_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956223_pe804623_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956223_pe804623_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy__0956223_pe804623_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/torvfly-torvflyu-polotence-kuhonnoe-s-risunkom-zelenyy-30493067/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__product-highlights"><span
                            class="range-revamp-product-highlight range-revamp-product-highlight__new">Новинка</span>
                        </div>
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">TORVFLY ТОРВФЛЮ</div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="30x40 см">30x40 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">229</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 4 шт</span></span>
                        </div>
                      </div>
                    </a></div>
                </div>
              </div>
              <div class="plp-fragment-wrapper">
                <div class="range-revamp-product-compact" data-ref-id="20479585" data-product-number="20479585"
                     data-product-type="ART" data-price="499" data-currency="RUB"
                     data-product-name="MARIATHERES МАРИАТЕРЕС" data-product-compact="" data-cs-capture="">
                  <button class="
    range-revamp-toggle-button range-revamp-toggle-button--icon-only range-revamp-toggle-button--transparent range-revamp-product-compact__add-to-list-button"
                          aria-pressed="false" aria-label="Добавить в список покупок">
                    <svg focusable="false" viewBox="0 0 24 24" class="range-revamp-svg-icon" aria-hidden="true">
                      <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.336 5.52055C14.2336 3.62376 17.3096 3.62401 19.2069 5.52129C20.2067 6.52115 20.6796 7.85005 20.6259 9.15761C20.6151 12.2138 18.4184 14.8654 16.4892 16.6366C15.4926 17.5517 14.5004 18.2923 13.7593 18.8036C13.3879 19.0598 13.0771 19.2601 12.8574 19.3973C12.7475 19.466 12.6601 19.519 12.5992 19.5555C12.5687 19.5737 12.5448 19.5879 12.5279 19.5978L12.5079 19.6094L12.502 19.6129L12.5001 19.614C12.5001 19.614 12.4989 19.6147 11.9999 18.748C11.501 19.6147 11.5005 19.6144 11.5005 19.6144L11.4979 19.6129L11.4919 19.6094L11.472 19.5978C11.4551 19.5879 11.4312 19.5737 11.4007 19.5555C11.3397 19.519 11.2524 19.466 11.1425 19.3973C10.9227 19.2601 10.612 19.0598 10.2405 18.8036C9.49947 18.2923 8.50726 17.5517 7.51063 16.6366C5.58146 14.8654 3.38477 12.2139 3.37399 9.15765C3.32024 7.85008 3.79314 6.52117 4.79301 5.52129C6.69054 3.62376 9.76704 3.62376 11.6646 5.52129L11.9993 5.856L12.3353 5.52129L12.336 5.52055ZM11.9999 18.748L11.5005 19.6144L11.9999 19.9019L12.4989 19.6147L11.9999 18.748ZM11.9999 17.573C12.1727 17.462 12.384 17.3226 12.6236 17.1573C13.3125 16.6821 14.2267 15.9988 15.1366 15.1634C17.0157 13.4381 18.6259 11.2919 18.6259 9.13506V9.11213L18.627 9.08922C18.6626 8.31221 18.3844 7.52727 17.7926 6.9355C16.6762 5.81903 14.866 5.81902 13.7495 6.9355L13.7481 6.93689L11.9965 8.68166L10.2504 6.9355C9.13387 5.81903 7.3237 5.81903 6.20722 6.9355C5.61546 7.52727 5.33724 8.31221 5.3729 9.08922L5.37395 9.11213V9.13507C5.37395 11.2919 6.98418 13.4381 8.86325 15.1634C9.77312 15.9988 10.6874 16.6821 11.3762 17.1573C11.6159 17.3226 11.8271 17.462 11.9999 17.573Z"></path>
                    </svg>
                  </button>
                  <a
                    href="https://www.ikea.com/ru/ru/p/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy-20479585/"
                    aria-label="MARIATHERES МАРИАТЕРЕС - Полотенце кухонное">
                    <div class="range-revamp-product-compact__image-wrapper"><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image"
                          alt="MARIATHERES МАРИАТЕРЕС Полотенце кухонное, полоска/серый бежевый, 50x70 см" srcSet="
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918036_pe786100_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918036_pe786100_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918036_pe786100_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918036_pe786100_s5.jpg?f=u 80w
      " sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918036_pe786100_s5.jpg?f=xxs"/></span><span
                        class="range-revamp-aspect-ratio-image range-revamp-aspect-ratio-image--square range-revamp-product-compact__image-hover range-revamp-product-compact__image-hover--inactive"><img
                          loading="lazy" class="range-revamp-aspect-ratio-image__image" alt="" srcSet=""
                          sizes="(max-width: 400px) 80px, (max-width: 1450px) 160px, 300px"
                          src="data:image/gif;base64,R0lGODdhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                          data-src="https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918037_pe786101_s5.jpg?f=xxs"
                          data-src-set="
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918037_pe786101_s5.jpg?f=m 600w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918037_pe786101_s5.jpg?f=xxs 300w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918037_pe786101_s5.jpg?f=xxxs 160w,
      https://www.ikea.com/ru/ru/images/products/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy__0918037_pe786101_s5.jpg?f=u 80w
      "/></span></div>
                  </a>
                  <div class="range-revamp-product-compact__bottom-wrapper"><a
                      href="https://www.ikea.com/ru/ru/p/mariatheres-mariateres-polotence-kuhonnoe-poloska-seryy-bezhevyy-20479585/">
                      <div class="range-revamp-compact-price-package">
                        <div class="range-revamp-compact-price-package__additional-info">
                          <div class="range-revamp-header-section">
                            <div class="range-revamp-header-section__title--small notranslate">MARIATHERES МАРИАТЕРЕС
                            </div>
                            <div class="range-revamp-header-section__description"><span
                                class="range-revamp-header-section__description-text">Полотенце кухонное</span><span
                                class="range-revamp-header-section__description-measurement"
                                data-ga-action="measurements_header_click" data-ga-label="50x70 см">50x70 см</span>
                            </div>
                          </div>
                        </div>
                        <div class="range-revamp-compact-price-package__previous-price-wrapper"></div>
                        <div class="range-revamp-compact-price-package__price-wrapper"><span class="range-revamp-price
    
    
    
    
  "><span class="range-revamp-price__integer">499</span><span class="range-revamp-price__currency-symbol range-revamp-price__currency-symbol--trailing
        range-revamp-price__currency-symbol--superscript"> ₽</span><span class="range-revamp-price__unit">/ 2 шт</span></span>
                        </div>
                      </div>
                    </a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="catalog-product-list__total-count" aria-live="assertive">Отображается 11 из 11</div>
        </noscript>
        <div class="plp-bottom-nav">
          <div class="vn-carousel">
            <div class="vn__wrapper">
              <nav role="navigation" class="vn__nav vn-3-grid" data-visual-navigation-links>
                <a href="https://www.ikea.com/ru/ru/cat/kuhonnye-polotenca-18851/"
                   class="vn-link vn__nav__link vn-3-grid-gap" data-tracking-label="products | kt001 | 18850 | 18851">
              <span class="vn-aspect-ratio-image vn-aspect-ratio-image--wide">
                <img class="vn-aspect-ratio-image__image" alt="Кухонные полотенца"
                     src="https://www.ikea.com/global/assets/navigation/images/tea-towels-18851.jpeg?imwidth=500"/>
              </span>
                  <span class="vn__nav__title" aria-hidden="true">Кухонные полотенца</span>
                </a>
                <a href="https://www.ikea.com/ru/ru/cat/povarskie-fartuki-18852/"
                   class="vn-link vn__nav__link vn-3-grid-gap" data-tracking-label="products | kt001 | 18850 | 18852">
              <span class="vn-aspect-ratio-image vn-aspect-ratio-image--wide">
                <img class="vn-aspect-ratio-image__image" alt="Поварские фартуки"
                     src="https://www.ikea.com/global/assets/navigation/images/aprons-18852.jpeg?imwidth=500"/>
              </span>
                  <span class="vn__nav__title" aria-hidden="true">Поварские фартуки</span>
                </a>
                <a href="https://www.ikea.com/ru/ru/cat/prihvatki-i-varezhki-prihvatki-20537/"
                   class="vn-link vn__nav__link vn-3-grid-gap" data-tracking-label="products | kt001 | 18850 | 20537">
              <span class="vn-aspect-ratio-image vn-aspect-ratio-image--wide">
                <img class="vn-aspect-ratio-image__image" alt="Прихватки и варежки-прихватки"
                     src="https://www.ikea.com/global/assets/navigation/images/pot-holders-oven-gloves-20537.jpeg?imwidth=500"/>
              </span>
                  <span class="vn__nav__title" aria-hidden="true">Прихватки и варежки-прихватки</span>
                </a>
              </nav>
            </div>
            <div class="vn-scroll-indicator">
              <div class="vn-scroll-indicator__bar" tabindex="0" style="transform: scaleX(0.25) translateX(0%);"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- 2021-09-09T14:11:44.802Z, Navigera 4fed225f -->
<div data-recently-viewed="true"></div>
<div class="hnf-sidebar-actions">
  <div class="hnf-sidebar-actions__inner">
    <div class="hnf-sidebar-actions__aside">
      <div class="hnf-sidebar-actions__bar" id="sidebar">
        <button type="button"
                class="hnf-btn hnf-btn--small hnf-leading-icon hnf-btn--primary hnf-btn--expanding hnf-sidebar-actions__back-to-top expanding-button"
                id="btn-back-to-top" tabindex="-1">
            <span class="hnf-btn__inner">
              <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn__icon" width="24" height="24"
                   viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
   <path fill-rule="evenodd" clip-rule="evenodd"
         d="m12.0001 6.9394 8.0007 8.0013-1.4142 1.4141L12 9.7678l-6.5869 6.586-1.414-1.4143 8.001-8z"/>
  </svg>
              <span class="hnf-btn__label">Наверх</span>
            </span>
        </button>
        <button type="button"
                class="hnf-btn hnf-btn--small hnf-leading-icon hnf-btn--primary hnf-btn--expanding hnf-sidebar-actions__share-to-socialmedia js-btn-hnf-share-to-socialmedia expanding-button expanding-button--visible"
                data-service="socialSharingPanel" role="button">
            <span class="hnf-btn__inner">
              <svg focusable="false" class="svg-icon  hnf-svg-icon hnf-btn__icon" width="24" height="24"
                   viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
   <path d="m11 7.8294-3.242 3.242-1.4143-1.4142L12 4l5.6575 5.6572-1.4143 1.4142L13 7.8284V16h-2V7.8294z"/>
    <path d="M2 21.0029h20v-9h-2v7H4v-7H2v9z"/>
  </svg>
              <span class="hnf-btn__label">Поделиться</span>
            </span>
        </button>
      </div>
    </div>
  </div>
</div>
<footer class="hnf-footer" role="contentinfo">
  <h-include-lazy src="https://www.ikea.com/ru/ru/header-footer/footer-lazy-fragment.html"></h-include-lazy>
</footer>
<div class="hnf-banner hnf-banner--light hnf-banner--bottom hnf-no-print js-cookie-info__panel"
     data-extra-class="storybook-container--min-height" enterTimer="0" exitTimer="0">
  <div class="hnf-banner__container">
    <div class="hnf-banner__content">
      <p>Сайт ИКЕА использует cookie, чтобы сделать пользование сайтом проще. <a
          href="https://www.ikea.com/ru/ru/customer-service/cookie-policy-pubb579c23b" class="hnf-banner__link link">Узнайте
          больше про использование cookie.</a></p>
    </div>
    <div class="hnf-banner__actions">
      <button type="button" class="hnf-btn hnf-btn--primary hnf-banner__action-btn js-cookie-info__accept-button">
            <span class="hnf-btn__inner">
              <span class="hnf-btn__label">Ок</span>
            </span>
      </button>
    </div>
  </div>
</div>
<!-- 2021-09-09T14:11:44.706Z, Navigera 4fed225f -->
<script
  src="https://polyfill.ikea.net/v3/polyfill.min.js?features=IntersectionObserver%2CObject.freeze%2Cdefault%2Ces2015%2Ces2016%2Ces2017%2Ces2018%2Ces2019%2Cfetch&amp;flags=gated"></script>
<script>this.customElements || document.write('<script src="https://www.ikea.com/ru/ru/header-footer/javascripts/document-register-element.js"><\x2fscript>');</script>
<script id="nav-script-common" data-domain="www.ikea.com" data-site-folder="ru/ru" data-release="4fed225f"
        data-sentry="false" data-intext="false"
        src="https://www.ikea.com/ru/ru/header-footer/javascripts/common.6bc336373479a5b4079a.js"></script>
<script src="https://www.ikea.com/ru/ru/header-footer/javascripts/main.b4b4160139933d957d9e.js"></script>


<script defer="defer" src="https://www.ikea.com/ru/ru/analytics/scripts/ga-main.fbdc64d3.js" async></script>
<script defer="defer" src="https://www.ikea.com/ru/ru/analytics/scripts/ga-targeting.fbdc64d3.js" async></script>
<script defer="defer" src="https://www.ikea.com/ru/ru/analytics/scripts/boomerang.min.js?fbdc64d3867d4046ec1f"
        async></script>
<script>window.isLandingPage =
    document.referrer.split('/')[2] !== location.host ? true : false;
  window.analyticsQueue = window.analyticsQueue || [];
  window.sendEvent = function (evt) {
    window.analyticsQueue.push(evt);
  };

  window.ikea.analytics = {
    observeProducts: [],
  };</script>


<!-- 2021-09-09 13:39:07 CEST Search: refs/tags/v8.124.0-dfb4e4ee10bdb6a282d31325a508bb9b07296942 -->
<link rel="preconnect" href="https://sik.search.blue.cdtapps.com" crossorigin>

<script type="module" src="https://www.ikea.com/ru/ru/search/shared.459c9495.js"></script>


<script type="module" src="https://www.ikea.com/ru/ru/search/search_vendors.459c9495.js"></script>


<script nomodule src="https://www.ikea.com/ru/ru/search/box-legacy.459c9495.js"></script>


<script type="module" src="https://www.ikea.com/ru/ru/search/box.459c9495.js"></script>


<script type="text/javascript"
        src="https://www.ikea.com/ru/ru/token-service/token-service-f1ed8a4ed28c0cb7cb30.js"></script>
<!-- RecommendationsScript: version: ba3e9c0, site-folder: ru/ru -->
<script>if (window && window.ikea && window.ikea.sentry && window.ikea.sentry.register) {
    window.ikea.sentry.register('5583489', '10bb11cc3ff548e295b5e960579d8042', 'recommendations', 'ba3e9c0', 0.1, [/\/[a-z0-9]{2}\/[a-z0-9]{2}\/recommendations\/panels\//]);
  }</script>
<script defer="defer" type="module" src="https://www.ikea.com/ru/ru/recommendations/panels/events.97b63e12.js"></script>
<script defer="defer" type="module" src="https://www.ikea.com/ru/ru/recommendations/panels/rec.97b63e12.js"></script>
<!-- /RecommendationsScript -->
<!-- gitVersion: refs/tags/v0.1.8, siteFolder: ru/ru -->
<script defer="defer" src="https://www.ikea.com/ru/ru/purchase-agent/cart-tmp/vendor.cb2751c0.js"></script>
<script defer="defer"
        src="https://www.ikea.com/ru/ru/purchase-agent/cart-tmp/purchase-middleware-base.44ac1f06.js"></script>
<div id="list-agent"></div>


<script>
  (function () {
    var text = {
      "Add all available to cart": "Добавить все доступные товары в корзину",
      "Add all products to cart": "Добавить все товары в корзину",
      "Add to another list": "Добавить в другой список",
      "Aisle": "Ряд",
      "All items moved to list success": "Все доступные предметы были перемещены из {sourceListName} к {destinationListName}",
      "And": "и",
      "Before discounts": "До скидок",
      "Bin": "Место",
      "Buy in Store": "Купить в магазине",
      "Buy Online": "Купить онлайн",
      "Collected": "Отобрано",
      "Contact store personel for more information": "Обратитесь к сотруднику магазина, чтобы узнать больше",
      "Create an account": "Создать учетную запись",
      "Create list": "Создать  новый список",
      "Create list confirmation button": "Ок",
      "Create list modal title": "Дайте название вашему новому списку",
      "Create list placeholder": "Название списка",
      "Create list success": "Список {listName} был создан",
      "Default list name": "Список покупок",
      "Delete list button label": "Удалите этот список избранного",
      "Discounts": "Скидки",
      "Edit list": "Изменить список",
      "Email": "E-mail",
      "Empty inspiration collection": "Пустой",
      "Empty landing page body": "Какие ваши любимые блюда? Сохраните и организуйте лучшие фрагменты вашего будущего дома здесь, пока вы не будете к ним готовы.",
      "Empty landing page title": "Ваш список пуст!",
      "Empty list body text": "Какие ваши любимые блюда? Лучшие части вашего будущего дома будут в безопасности, пока вы не будете к ним готовы.",
      "Empty list button label": "Очистите этот список избранного",
      "Empty list title text": "{listName} пусто!",
      "EMPTY_NAME": "У списка должно быть название",
      "Family price per item": "Цена IKEA Family за единицу товара:",
      "Favourites description": "Вот все твои любимые вещи. Товары можно классифицировать по категориям в разных коллекциях. У вдохновляющих образов есть своя коллекция вверху.",
      "GENERIC_ERROR": "Что-то пошло не так. Пожалуйста, попробуйте еще раз позже.",
      "Get Inspired": "Вдохновляйтесь",
      "Get session error": "Что-то пошло не так во время восстановления сеанса. Пожалуйста, очистите файлы cookie вашего браузера и повторите попытку.",
      "Hide included articles": "Скрыть артикулы комбинации",
      "IKEA Family price": "Цена IKEA Family",
      "Including VAT": "Включая НДС",
      "Input field help message": "Укажите уникальное имя списка из менее чем {длина} персонажи",
      "Inspiration": "Вдохновение",
      "Inspiration feed title": "Больше идей и вдохновения",
      "insufficient stock": "Недостаточное количество",
      "INVALID_ITEM_NUMBER": "Товар недоступен для покупки, поэтому его нельзя добавить в корзину",
      "Item moved to list success": "{itemName} был перемещен из {sourceListName} к {destinationListName}",
      "Item number": "Артикул",
      "Item was added to the cart": "{name} теперь в корзине",
      "Item was added to the list": "{productName} добавлен в {listName}",
      "Item was removed": "Удалено: {name} ",
      "Item was removed from generic list": "{productName} удален из списка избранного",
      "Item was removed from lists": "{productName} был удален из списка {listNames}",
      "Items not available online": "Некоторые товары не доступны для покупки онлайн",
      "Items not available online explanation": "Либо их можно купить только в магазине, либо они вышли из ассортимента с тех пор, как вы их добавили.",
      "Items were added to the cart": "Товары были добавлены в корзину",
      "Items were added to the list": "{count} элементы были добавлены в {listName}",
      "Items were removed": "Удаленные товары: {count}",
      "List was removed": "Удалено: {name}",
      "Lists were removed": "Списки {count} были удалены",
      "Loading": "Загрузка...",
      "Log in": "Вход",
      "Log in promo description prefix": "Данный список не будет сохранен. ",
      "Log in promo description prefix with inspirations": "Эти коллекции носят временный характер.",
      "Log in promo description suffix": "чтобы сохранить ваш список.",
      "Log in promo description suffix with inspirations": "чтобы убедиться, что ваши любимые все еще будут здесь, когда вы вернетесь.",
      "Log in promo heading": "Сохраните свои списки желаний",
      "Log in promo heading with inspirations": "Оставь свои любимые",
      "Log in promo link anchor": "Зарегистрируйтесь или войдите в систему,",
      "Manage lists": "Управляйте вашими {link}",
      "Manage lists link": "Списками покупок",
      "MAX_CREATE_LIMIT": "Вы не можете создать больше 100 списков.",
      "MAX_NAME_LENGTH": "Длина имени списка не может быть больше {длина}",
      "modal_close": "Закрыто",
      "Move all items to a new list error": "Что-то пошло не так при перемещении всех предметов из {sourceListName} к {destinationListName}",
      "Move all items to another wishlist": "Переместить все элементы в другой список избранного",
      "Move all to list modal title": "Переместить все в какой список пожеланий?",
      "Move item to a new list error": "Что то пошло не так во время движения {itemName} от {sourceListName} к {destinationListName}",
      "Move item to list modal title": "Где {itemName} уходишь?",
      "Move to a new list": "Перейти в новый список пожеланий",
      "Move to another list": "Переместить в другой список",
      "Move to cart": "Добавить в корзину",
      "NAME_CONFLICT": "Такое название уже существует.",
      "NETWORK_ERROR": "Не удалось связаться с сервером. Пожалуйста, проверьте ваше соединение с интернетом и попробуйте снова.",
      "New article": "Новинка",
      "New lower price": "Новая самая низкая цена",
      "New lower price per item": "Новая самая низкая цена за единицу товара:",
      "No favourite images saved yet?": "Любимые изображения еще не сохранены?",
      "No items in the list": "Этот список кажется пустым",
      "No store selected": "Магазин не выбран",
      "Not available for online purchase": "Недоступно для покупки онлайн",
      "only x items available": "В наличии: {stock}",
      "Open context menu for list": "Открыть контекстное меню для списка",
      "Open context menu for product": "Открыть контекстное меню для товара {itemNo}",
      "Out of stock": "Нет в наличии",
      "Packet": "упаковка",
      "Pickup at department": "Забрать в магазине: {department}",
      "Price per item": "Цена за единицу товара:",
      "Price without vat": "Цена без НДС",
      "Print": "Напечатать",
      "Product eco excluded": "Without eco contribution",
      "Product eco fee": "Eco fee",
      "Product eco fee eco": "Мебель",
      "Product eco fee information eco": "Since May 1, 2013, an eco-participation, is set up for the management of used furniture (furniture, bedding and bed bases). From 1 October 2018, an eco-participation also applies to quilts, pillows, bolsters, sleeping bags. These amounts are entirely donated to Eco-furniture, an organization approved by the State, and devoted to the development of collection, recycling and recovery solutions for these used products. Framed by law, eco-participation applies equally to all new furniture, bedding and bedsteads, duvets and pillows throughout France. The amount is defined according to a scale common to all manufacturers and distributors and corresponds to the cost of waste management of these used products.",
      "Product eco fee information weee": "Since November 15, 2006, all electrical and electrical products sold to households are subject to the eco-participation WEEE which is added to the selling price. This contribution will be used to finance the recycling of waste electrical and electronic equipment (WEEE).",
      "Product eco fee participation eco": "Furniture eco contribution",
      "Product eco fee participation weee": "WEEE eco contribution",
      "Product eco fee readmore": "More about eco-participation",
      "Product eco fee total": "Итоговая цена",
      "Product eco fee weee": "WEEE",
      "Quantity": "Количество",
      "Receive items header": "Делиться-значит заботиться",
      "Receive items sub header": "Кто-то прислал тебе красивые вещи, которые, по их мнению, тебе стоит посмотреть.",
      "Regular price": "Цена:",
      "Regular price per item": "Цена за единицу товара:",
      "Reload": "Перезагрузить",
      "Remove from wish list": "Удалить из списка желаний",
      "Remove from wish list label": "Удалить из этого списка пожеланий",
      "Remove product": "Удалить",
      "Remove product from": "Удалить {product} от",
      "Rename list confirmation button": "Ок",
      "Rename list label": "Переименовать",
      "Rename list title": "Есть имя получше?",
      "Save it here until you're ready": "Сохраните на будущее",
      "Save item in list": "Сохранить {name} в списке",
      "Save items in list": "Сохранение товаров в списке",
      "Save product in list": "Сохранить товар в списке",
      "Select closest": "Выбрать ближайший",
      "Select list": "Выбрать список",
      "Selected list": "Ваш список",
      "services_upsell_taskrabbit_description": "Get your items unboxed and assembled by TaskRabbit.",
      "services_upsell_taskrabbit_read_more": "Читать далее",
      "Share": "Поделиться",
      "Share list body": "Вы собираетесь поделиться снимком этого списка избранного. Никакие изменения, внесенные в этот список позже, не будут видны другим пользователям, если вы не поделитесь новым снимком.",
      "Share list button label": "Поделиться списком",
      "Share list by email default body": "Привет!\nПосмотри на мой список избранного {listName}.\nЕго можно найти здесь\n{path}",
      "Share list by email default subject": "Посмотрите мои списки избранного в ИКЕА",
      "Share list by email label": "Email this wish list",
      "Share list by link default copy text": "Ссылка на снимок из списка {listName} была скопирована в буфер обмена",
      "Share list by link fail": "Не удалось скопировать ссылку в буфер обмена",
      "Share list by link label": "Скопировать ссылку",
      "Share list title": "Поделитесь своим списком избранного",
      "Show": "Показать",
      "Show included articles": "Показать артикулы комбинации",
      "Store": "Магазин",
      "Store location": "Расположение в магазине",
      "Subtotal": "Промежуточный итог",
      "TEST_KEY": "{listName} был добавлен",
      "Total": "Общая стоимость",
      "Total Weight": "Общий вес",
      "UNAVAILABLE_STOCK": "Невозможно получить информацию о наличии",
      "Undo": "Отмена",
      "VAT": "НДС",
      "View cart": "Посмотреть",
      "Want to buy these articles online?": "Хотите купить эти товары онлайн?",
      "Webpage title": "Список покупок",
      "Weight per article": "Вес за единицу товара",
      "Wishlist has been emptied": "Этот список избранного пуст",
      "Wishlist Summary Headline": "Ваш список ",
      "x images": "{количество} изображения",
      "x in stock": "{stock} в наличии",
      "x items": "{quantity} шт.",
      "x pcs": "{quantity} шт.",
      "Your favourites": "Ваши любимые блюда",
      "Your lists": "Ваш список",
      "Your wish lists": "Избранное"
    };
    window.ikea = window.ikea || {};
    window.ikea.favourites = window.ikea.favourites || {};
    window.ikea.favourites.text = window.ikea.favourites.text || text;
    window.ikea.favourites.agentLoaded = true;
  })();
</script>
<script async defer src="https://www.ikea.com/ru/ru/favourites/agent/agent-refs-tags-v0-7-39.6d48b821.js"></script>


<!-- igift-agent-disabled -->

<!-- Feed gitVersion: 20210908200300, siteFolder: ru/ru -->
<script>
  if (window && window.ikea && window.ikea.sentry && window.ikea.sentry.register) {
    window.ikea.sentry.register('5244394', '4def75a18cb34ab59e68a690be123979', 'feed', '20210908200300', 0.1, [/\/[a-z0-9]{2}\/[a-z0-9]{2}\/insp-feed\/content-gallery-app\//]);
  }
</script>
<script defer="defer" src="https://www.ikea.com/ru/ru/insp-feed/content-gallery-app/feed.e60f0cbc.js"></script>
<script src="https://www.ikea.com/ru/ru/products/javascripts/price-package-scripts.4250367595707afbc020.js"></script>
<script type="text/javascript">window["optimizelyDatafile"] = {
    "version": "4",
    "rollouts": [],
    "typedAudiences": [],
    "anonymizeIP": true,
    "projectId": "18201302387",
    "variables": [],
    "featureFlags": [],
    "experiments": [{
      "status": "Running",
      "audienceIds": [],
      "variations": [{"variables": [], "id": "20589160669", "key": "a"}, {
        "variables": [],
        "id": "20597290444",
        "key": "b"
      }],
      "id": "20599110505",
      "key": "plp-907_high_margin_breakouts",
      "layerId": "20600940859",
      "trafficAllocation": [{"entityId": "20589160669", "endOfRange": 1000}, {
        "entityId": "",
        "endOfRange": 5000
      }, {"entityId": "20597290444", "endOfRange": 6000}, {"entityId": "", "endOfRange": 10000}],
      "forcedVariations": {}
    }],
    "audiences": [{
      "conditions": "[\"or\", {\"match\": \"exact\", \"name\": \"$opt_dummy_attribute\", \"type\": \"custom_attribute\", \"value\": \"$opt_dummy_value\"}]",
      "id": "$opt_dummy_audience",
      "name": "Optimizely-Generated Audience for Backwards Compatibility"
    }],
    "groups": [],
    "sdkKey": "B55RSZvdcuDQ8kD1YxvvN",
    "environmentKey": "production",
    "attributes": [{"id": "18219820812", "key": "marketCode"}, {
      "id": "18716282466",
      "key": "site"
    }, {"id": "18724591472", "key": "environment"}, {"id": "18743753042", "key": "url"}, {
      "id": "18749011620",
      "key": "browser"
    }, {"id": "18754351030", "key": "position"}, {"id": "18764370003", "key": "device"}, {
      "id": "18779380060",
      "key": "market"
    }, {"id": "19402671474", "key": "plp-environment"}, {
      "id": "19406141378",
      "key": "plp-marketCode"
    }, {"id": "19406563217", "key": "plp-browser"}, {"id": "19408102758", "key": "plp-device"}, {
      "id": "19425931423",
      "key": "plp-position"
    }, {"id": "19433182009", "key": "plp-market"}, {"id": "19459171443", "key": "plp-site"}, {
      "id": "19477101023",
      "key": "plp-url"
    }, {"id": "19862691877", "key": "plp-category-id"}, {"id": "20191982540", "key": "plp-category-type"}],
    "botFiltering": true,
    "accountId": "11701662245",
    "events": [{
      "experimentIds": [],
      "id": "20262589732",
      "key": "plp_enhanced_product_add_to_favourites"
    }, {"experimentIds": [], "id": "20272779632", "key": "plp_variant_click_card"}, {
      "experimentIds": ["20599110505"],
      "id": "20282776408",
      "key": "plp_product_add_to_cart"
    }, {"experimentIds": [], "id": "20286425907", "key": "plp_filter_clear_all"}, {
      "experimentIds": [],
      "id": "20286851473",
      "key": "plp_activate_experiment"
    }, {"experimentIds": [], "id": "20292354803", "key": "plp_visit_page_plp"}, {
      "experimentIds": [],
      "id": "20292483563",
      "key": "plp_sort_selected"
    }, {"experimentIds": [], "id": "20294664853", "key": "plp_filter_deselected"}, {
      "experimentIds": [],
      "id": "20294664874",
      "key": "plp_planner_click_through"
    }, {"experimentIds": [], "id": "20296595467", "key": "plp_filter_selected"}, {
      "experimentIds": [],
      "id": "20296813297",
      "key": "plp_compare_activate"
    }, {"experimentIds": [], "id": "20300463897", "key": "plp_shoppable_image_product_click"}, {
      "experimentIds": [],
      "id": "20303695510",
      "key": "plp_sort_open"
    }, {"experimentIds": [], "id": "20303863636", "key": "plp_more_filters_clicked"}, {
      "experimentIds": [],
      "id": "20305277698",
      "key": "plp_compare_navigate"
    }, {"experimentIds": [], "id": "20305277701", "key": "plp_enhanced_product_click_through"}, {
      "experimentIds": [],
      "id": "20307665112",
      "key": "plp_applied_filter_deselected"
    }, {"experimentIds": [], "id": "20308100707", "key": "plp_shoppable_image_click"}, {
      "experimentIds": [],
      "id": "20311305394",
      "key": "plp_filter_open"
    }, {"experimentIds": [], "id": "20311682614", "key": "plp_variant_view"}, {
      "experimentIds": [],
      "id": "20311791546",
      "key": "plp_product_add_to_favourites"
    }, {"experimentIds": [], "id": "20313374285", "key": "plp_variant_click"}, {
      "experimentIds": [],
      "id": "20314080646",
      "key": "plp_applied_filter_clear_all"
    }, {"experimentIds": [], "id": "20315483428", "key": "plp_element_visible"}, {
      "experimentIds": [],
      "id": "20316400045",
      "key": "plp_enhanced_product_add_to_cart"
    }, {
      "experimentIds": ["20599110505"],
      "id": "20317224557",
      "key": "plp_callout_click_through"
    }, {
      "experimentIds": ["20599110505"],
      "id": "20319083610",
      "key": "plp_product_click_through"
    }, {"experimentIds": [], "id": "20330152316", "key": "plp_bottom_navigation_click"}, {
      "experimentIds": [],
      "id": "20333695755",
      "key": "plp_quick_filter_click"
    }, {"experimentIds": [], "id": "20355940487", "key": "plp_filter_description_click"}],
    "revision": "657"
  }</script>
<script src="https://www.ikea.com/ru/ru/product-lists/plp-main-542.js"></script>
<!-- 2021-09-09T14:11:44.707Z, Navigera 4fed225f -->


<script
  src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/jquery-3.6.0.min.511668957aa5cefd510feebf.js"></script>

<script type="text/javascript">
  function loadM2Script(url) {
    var script = document.createElement("script");
    script.id = "m2-script";
    script.src = url;
    script.dataset.account = "ru_RU-m2";

    document.head.appendChild(script);
  }

  if (!window.setCookie) {
    window.setCookie = function (name, value, expires, path, domain, secure) {
      var curCookie =
        name +
        "=" +
        escape(value) +
        (expires ? "; expires=" + expires.toGMTString() : "") +
        (path ? "; path=" + path : "") +
        (domain ? "; domain=" + domain : "") +
        (secure ? "; secure" : "");
      document.cookie = curCookie;
    };
  }

  if (!window.getCookie) {
    window.getCookie = function (name) {
      var dc = document.cookie;
      var prefix = name + "=";
      var begin = dc.indexOf("; " + prefix);
      if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
      } else begin += 2;
      var end = document.cookie.indexOf(";", begin);
      if (end == -1) end = dc.length;
      return unescape(dc.substring(begin + prefix.length, end));
    };
  }

  var localScriptVersion = getCookie("localScriptVersion");

  if (!localScriptVersion) {
    var expires = new Date();
    expires.setMinutes(expires.getMinutes() + 15);
    localScriptVersion = Date.now();
    setCookie(
      "localScriptVersion",
      localScriptVersion,
      expires,
      "/",
      ".ikea.com",
      ""
    );
  }

  loadM2Script("https://ikeasources.ru/b/m2.js?" + localScriptVersion);
</script>

<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" defer="true"></script>
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/criteo.14db5008c1e0e6b4fe821449.js"
        defer="true"></script>

<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/rtb/rtbhouse.5bef33031a27c7e386d7e50b.js"
        defer></script>

<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/adform.8991872b11df1c67e6b0a368.js"
        defer></script>

<script async src="//ikearu.api.useinsider.com/ins.js?id=10002692" defer></script>
<script>
  if (window.Checkout) {
    window.addEventListener('purchase', function () {
      window.insider_object = {"page": {"type": "Confirmation"}};
    })
  }
</script>

<!--implemented on October 8th  owner Dmitry Sherback-->
<script type="text/javascript">
  var wamid = "4669"; /* WAM Account ID */
  var typ = "1"; /* Event Type default 1 */
  (function () {
    var w = document.createElement("script");
    w.type = "text/javascript";
    w.src =
      document.location.protocol +
      "//cstatic.weborama.fr/js/wam/customers/wamfactory_dpm.wildcard.min.js?rnd=" +
      new Date().getTime();
    w.async = true;
    var body = document.getElementsByTagName("script")[0];
    body.parentNode.insertBefore(w, body);
  })();
  (function () {
    "use strict";
    var spent_time = 0;
    var count_spent_time = setInterval(function () {
      spent_time += 30;
      if (spent_time > 120) {
        clearInterval(count_spent_time);
        return;
      }
      if (window.wamf && window.wamf.eventSend) {
        window.wamf.eventSend("2", "spent_time", String(spent_time));
      } else console.error('window.wamf.eventSend is not defined');
    }, 30000);
  })();
</script>
<!--implemented on 26 September owner Dmitry Sherback-->

<!-- CONVERSION TAG -->
<script type="text/javascript" src="https://cstatic.weborama.fr/js/advertiserv2/adperf_conversion.js" defer></script>
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/weborama.fda4b0d10bd881825ecb471b.js"
        defer></script>
<!--end of weborama code-->

<!-- VK -->
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/vkontakte.279e6cb8e1c29368fcf6bbfc.js"
        defer></script>
<noscript>
  <img src="https://vk.com/rtrg?p=VK-RTRG-278069-hL59B" style="position: fixed; left: -999px" alt=""/>
</noscript>
<!-- VK End -->

<!-- DoubleClick -->
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/doubleclick.8ad270d71c9091bb917fc85a.js"
        defer></script>

<!-- Facebook Global -->
<script
  src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/facebook-global.636f7decdab1954938ee32d9.js"
  defer></script>

<!-- Rating@Mail.ru counter (Mytarget) -->
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/mytarget.76a4e8d60cefbbf74e41ea1d.js"
        defer></script>
<noscript>
  <img src="https://top-fwz1.mail.ru/counter?id=3056918;js=na" style="position: absolute; left: -999px" alt=""/>
</noscript>
<!-- Rating@Mail.ru counter (Mytarget) End -->

<!-- Soloway -->
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/soloway.1a8e955f8ecd4d50ec354965.js"
        defer></script>
<!-- Soloway End -->

<!-- Yandex -->
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/yandex.4d1c67ab145469bc2aa04d6c.js"
        defer></script>
<!-- Yandex End -->

<!-- Salesforce -->
<script type="text/javascript">
  window.Krux || ((Krux = function () {
    Krux.q.push(arguments);
  }).q = []);
</script>
<script type="text/javascript" defer
        src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/salesforce.1138f77d041447cad4cb28dd.js"></script>

<!-- Salesforce -->

<!-- Acstat -->
<script src="https://www.ikea.com/ru/ru/local-extensions/header-footer/scripts/acstat.03623d7e9de7e272e3fdebf2.js"
        defer></script>
<!-- Acstat End-->

<!-- Recreate Cart Solution -->

<script type="text/javascript"
        src="https://www.ikea.com/sbNAkT/G/g/o8EoEr-1Bw/hu5fkGNQc1au/QGtpNgFaBQ/Y0/AwNhI5aHM"></script>
</body>
</html>
  