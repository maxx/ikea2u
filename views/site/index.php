<?php
/* @var $this yii\web\View */
$this->title = 'Доставка мебели и предметов интерьера на дом по городу Гомелю под заказ без предоплаты';
?>

<div class="site-index">
    <!--
        <div class="jumbotron">
           Баннер
        </div>
    -->
    <div class="body-content">

        <div class="row">
            <div class="col-lg-8">
                <p>
                    <?php echo isset($page->body) ? $page->body : '' ?>
                </p>              
               <?php foreach ($categories as $category): ?>
                    <div class="productCategoryContainer">
                        <span class="header"><?php echo $category->title ?></span>
                        <div class="textContainer">
                            <?php foreach ($category->children()->all() as $subcategory): ?>
                                <a href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id' => $subcategory->id]); ?>"><?php echo $subcategory->title ?></a>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>


            <div class="col-lg-4">
                <div id="ok_group_widget"></div>
                <div id="vk_groups"></div>
            </div>
        </div>

        <script>
            !function (d, id, did, st) {
                var js = d.createElement("script");
                js.src = "http://connect.ok.ru/connect.js";
                js.onload = js.onreadystatechange = function () {
                    if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                        if (!this.executed) {
                            this.executed = true;
                            setTimeout(function () {
                                OK.CONNECT.insertGroupWidget(id, did, st);
                            }, 0);
                        }
                    }
                }
                d.documentElement.appendChild(js);
            }(document, "ok_group_widget", "57501799677996", "{width:360,height:335}");
        </script>

        <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>            
        <script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: 0, width: "360"}, 80053860);
        </script>        



    </div>
</div>
