<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  <?php
  /* $cs = \Yii::$app->getClientScript();
    $cs->coreScriptPosition = CClientScript::POS_BEGIN;
    $cs->registerCoreScript('jquery', CClientScript::POS_BEGIN);
    $cs->registerScriptFile(\Yii::$app->baseUrl . '/js/jquery-ui-1.8.18.custom.min.js', CClientScript::POS_BEGIN);
    $cs->registerScriptFile(\Yii::$app->baseUrl . '/js/shopping-cart.js', CClientScript::POS_BEGIN);
   *
   */
  ?>

  <?php $this->head() ?>
</head>
<body>


<?php $this->beginBody() ?>
<div class="wrap">

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong>Товар добавлен в корзину</strong><br>
    <a href="/cart">перейти к оформлению</a>
  </div>

  <div class="header-wrapper">
    <div class="row">
      <div class="col-lg-2">
        <div class="bs-component">
          <h1>i2u.by</h1>
          <div class="col-lg-7">
            <?php

            use app\components\SearchFormWidget;
            use app\components\DeliveryCountdownWidget;

            ?>
            <?= SearchFormWidget::widget() ?>


          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="row top20">
          <p class="">Доставка мебели и предметов интерьера на дом по городу Гомелю под заказ без предоплаты</p>
        </div>

      </div>
      <div class="col-lg-2">
        <div class="bs-component">
          <div class="row top20">
            <p>Звоните нам: <br/>c 9.00 до 21.00<br/> БЕЗ ВЫХОДНЫХ</p>
            <p><a href="<?php echo \yii\helpers\Url::toRoute('/site/contact'); ?>">Не дозвонились?</a></p>

          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="bs-component">
          <div class="row top20">
            <p class="text-danger"><strong>tel. офис: +375(44)7-111-242 </strong></p>
            <p class="text-danger"><strong>tel./viber: +375(29)658-90-75</strong></p>

            <p class="text-danger">Icq: 419737021</p>
            <p>E-mail: 6589075@gmail.com</p>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="bs-component">
          <div class="row top20">
            <?php echo \app\components\ShoppingCartWidget::widget(); ?>
          </div>
        </div>
      </div>

      <?= DeliveryCountdownWidget::widget() ?>
    </div>
  </div>

  <div class="container">
    <?php
    NavBar::begin([
      /*  'brandLabel' => 'Ikea2u.by',
        'brandUrl' => Yii::$app->homeUrl, */
        'options'=>[
            'class'=>'navbar navbar-default',
        ],
    ]);
    echo Nav::widget([
        'options'=>['class'=>'nav navbar-nav'],
        'items'=>[
            ['label'=>'Главная', 'url'=>['/site/index']],
          //    !Yii::$app->user->isGuest ? ['label' => 'Каталог', 'url' => ['/item/index']]:'',
            ['label'=>'Каталог', 'url'=>['/item/index']],
            //['label'=>'Зимняя коллекция', 'url'=>['/winter'], 'options'=>['class'=>'winter']],
            ['label'=>'В наличии', 'url'=>['/item/instock']],
            ['label'=>'Акции', 'url'=>['/page/view', 'name'=>'promo']],
            ['label'=>'Сертификаты', 'url'=>['/page/view', 'name'=>'certificate']],
          //  ['label' => 'Корзина', 'url' => ['/cart/index']],
            ['label'=>'Оплата и доставка', 'url'=>['/page/view', 'name'=>'pay']],
          // ['label' => 'Заказать в кредит', 'url' => ['/page/view', 'name' => 'credit']],

            ['label'=>'О нас', 'url'=>['/page/view', 'name'=>'about']],
          //    !Yii::$app->user->isGuest ? ['label' => 'Сделать Заказ', 'url' => ['/cart'], 'options' => ['style' => 'background-color:#dd5600 ']] : '',
          //                      ['label' => 'Оплата', 'url' => ['/site/about']],
//                        ['label' => 'Доставка', 'url' => ['/site/about']],
          //['label' => 'Товары в наличии', 'url' => ['/site/about3']],
            ['label'=>'Отзывы', 'url'=>['/feedback']],
          //['label' => 'О нас', 'url' => ['/page/view', 'name' => 'about']],
            ['label'=>'Как добраться', 'url'=>['/page/view', 'name'=>'location']],
            ['label'=>'Остались вопросы?', 'url'=>['/site/contact']],
            Yii::$app->user->isGuest ? ['label'=>'', 'url'=>['/user/login']] : ['label'=>'Админка ', 'url'=>['/admin'],],
            Yii::$app->user->isGuest ?
              /* ['label' => 'Вход', 'url' => ['/user/login']] */ '' :
                ['label'=>'Выход (' . Yii::$app->user->identity->email . ')',
                    'url'=>['/user/logout'],
                    'linkOptions'=>['data-method'=>'post']],
        ],
    ]);
    NavBar::end();
    ?>

    <?=
    Breadcrumbs::widget([
        'links'=>isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])
    ?>
    <?= $content ?>
  </div>
</div>

<footer class="footer">
  <div class="container">
    <p class="pull-left">&copy; I2u.by <?= date('Y') ?></p>
    <p class="pull-right">ИП Кусенков В.В. <br/>
      УНП 491139768 <br/>
      г.Гомель <br/>
      Свидетельство ОГРНИП 491139768 выдано 22.07.2014г. ГГИК Администрация Советского района г.Гомеля. <br/>
      Регистрация интернет-магазина в торговом реестре №302945 от 22.01.2016г.</p>
  </div>
</footer>
<?php $this->endBody() ?>
<!-- Start SiteHeart code -->
<script>
  (function () {
    var widget_id = 748758;
    _shcp = [{widget_id: widget_id}];
    var lang = (navigator.language || navigator.systemLanguage
      || navigator.userLanguage || "en")
      .substr(0, 2).toLowerCase();
    var url = "widget.siteheart.com/widget/sh/" + widget_id + "/" + lang + "/widget.js";
    var hcc = document.createElement("script");
    hcc.type = "text/javascript";
    hcc.async = true;
    hcc.src = ("https:" == document.location.protocol ? "https" : "http")
      + "://" + url;
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hcc, s.nextSibling);
  })();
</script>
<!-- End SiteHeart code -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function () {
      try {
        w.yaCounter29925139 = new Ya.Metrika({
          id: 29925139,
          webvisor: true,
          clickmap: true,
          trackLinks: true,
          accurateTrackBounce: true
        });
      } catch (e) {
      }
    });

    var n = d.getElementsByTagName("script")[0],
      s = d.createElement("script"),
      f = function () {
        n.parentNode.insertBefore(s, n);
      };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
      d.addEventListener("DOMContentLoaded", f, false);
    } else {
      f();
    }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
  <div><img src="//mc.yandex.ru/watch/29925139" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


<script src="<?php echo Url::base() . '/js/jquery.scrollUp.min.js' ?>"></script>
<script src="<?php echo Url::base() . '/js/jquery.countdown.min.js' ?>"></script>
<script src="<?php echo Url::base() . '/js/shopping-cart.js' ?>"></script>
<script src="<?php echo Url::base() . '/js/main.js' ?>"></script>
</body>
</html>
<?php $this->endPage() ?>
