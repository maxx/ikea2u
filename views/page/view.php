<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
       <?php echo $model->body?>
    

</div>
