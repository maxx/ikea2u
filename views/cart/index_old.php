<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Заказ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>


    <?php else: ?>

        <p>
            Если у вас есть вопросы или предложения, напишите нам! 
        </p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'ikea-order-form']); ?>
              <div class="input-group">
                      <?php
                      echo $form->field($model, 'ikea_number_or_url', [
                          'template' => "{input}\n{hint}"
                      ])
                      ?>
                    <span class="input-group-btn">
                        <?= Html::submitButton('Добавить товар', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </span>
                </div>
<?php    echo Html::error($model,'ikea_number_or_url', ['class' => 'help-block']); //error?>                
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
        <?php if (!Yii::$app->cart->getIsEmpty()):?>
        
            <h2>Ваша корзина</h2>

        
        


<div class="bs-component">

    
    <?php
    $form = ActiveForm::begin([
                'id' => 'cart-form',
            ])
    ?>
            <table class="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th>#</th><th>Артикул</th><th>Картинка</th>
                        <th>Название</th><th>Краткое описание</th><th>Средне описание</th><th>Кол-во в упаковке</th>
                        <th>Цена RUB</th><th>Цена бел.руб</th><th>Кол-во</th><th>Сумма</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=1;
                    foreach (\Yii::$app->cart->getPositions() as $ikeaItem):?>
                        <tr>
                            <td><?php echo $i?></td>
                            <td><?php echo $ikeaItem->number ?></td>
                            <td><img src="http://www.ikea.com/<?php echo $ikeaItem->small_img_src ?>"></td>
                            <td><?php echo $ikeaItem->title ?></td>
                            <td><?php echo $ikeaItem->short_description ?></td>
                            <td><?php echo $ikeaItem->middle_description ?></td>
                            <td><?php echo $ikeaItem->package_quantity ?></td>
                            <td><?php echo $ikeaItem->rus_price_total ?> rub</td>
                            <td><?php echo $ikeaItem->bel_price_total ?> б.р.</td>
                            <td><input type="text" id="item_<?php echo $ikeaItem->number?>" name="items[<?php echo $ikeaItem->number?>]" value="<?php echo $ikeaItem->getQuantity()?>" class="qty" size="3" maxlength="10"></td>
                            <td><?php echo $ikeaItem->getCost() ?></td>
                            <td><a href="<?php echo \yii\helpers\Url::toRoute('/cart/delete');?>" data-item="<?php echo $ikeaItem->number?>" class="delete">Удалить</a></td>
                        </tr>
                    <?php 
                    $i++;
                    endforeach;?>
                    <tr>
                        <td colspan="7"><a href="<?php echo \yii\helpers\Url::toRoute('/cart/clear');?>">Очистить корзину</a></td>
                        <td colspan="5" style="text-align: right; font-weight: bold">Итого: <span class="cart-cost"><?php echo \Yii::$app->cart->cost?></span> бел. рублей</td>
                    </tr>
                    <tr>
                        <td colspan="11">
                             <?= Html::submitButton('Обновить корзину', ['class' => 'btn btn-primary']) ?></td>
                        <td colspan=""><a href="<?php echo \yii\helpers\Url::toRoute('/cart/checkout');?>" class="btn btn-primary" style="text-align: right;">Перейти к оформлению</a></td>
                    </tr>                    
                </tbody>
            </table> 

    <?php  ActiveForm::end() ?>
            <?php else: ?>
    
    
        </div>    
    <?php endif;?>


</div>
