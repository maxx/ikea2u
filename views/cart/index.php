<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Заказ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>


    <?php else: ?>

        <p>
            Если у вас есть вопросы или предложения, напишите нам! 
        </p>



    <?php endif; ?>
    <?php if (!Yii::$app->cart->getIsEmpty()): ?>

        <h2>Ваша корзина</h2>
        <div class="bs-component">
            <?php
            $form = ActiveForm::begin([
                        'id' => 'cart-form',
                    ])
            ?>
            <table class="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th>#</th><th>Артикул</th><th>Картинка</th>
                        <th>Название</th><th>Краткое описание</th><th>Кол-во в упаковке</th>
                        <th>Цена бел.руб</th><th>Кол-во</th><th>Сумма</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach (\Yii::$app->cart->getPositions() as $ikeaItem):
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $ikeaItem->number ?></td>
                            <td style=""><img width="100" src="http://www.ikea.com/<?php echo $ikeaItem->image_uri ?>"></td>
                            <td><?php echo $ikeaItem->title ?></td>
                            <td><?php echo $ikeaItem->short_description ?></td>
                            <td><?php echo isset($ikeaItem->qty_in_pack) ? $ikeaItem->qty_in_pack : '' ?></td>
                            <td><?php echo app\models\Utilities::Currency($ikeaItem->getPrice()) ?> </td>
                            <td><input type="text" id="item_<?php echo $ikeaItem->id ?>" name="items[<?php echo $ikeaItem->id ?>]" value="<?php echo $ikeaItem->getQuantity() ?>" class="qty" size="3" maxlength="10"></td>
                            <td><?php echo app\models\Utilities::Currency($ikeaItem->getCost()) ?></td>
                            <td style="text-align: right"><a href="<?php echo \yii\helpers\Url::toRoute('/cart/delete'); ?>" data-item="<?php echo $ikeaItem->id ?>" class="delete">Удалить</a></td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                    <tr>
                        <td colspan="7"><a href="<?php echo \yii\helpers\Url::toRoute('/cart/clear'); ?>">Очистить корзину</a></td>
                        <td colspan="3" style="text-align: right; font-weight: bold">Итого: <span class="cart-cost"><?php echo app\models\Utilities::Currency(\Yii::$app->cart->cost, false) ?></span> бел. рублей</td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <?= Html::submitButton('Обновить корзину', ['class' => 'btn btn-primary']) ?></td>
                        <td colspan=""><a href="<?php echo \yii\helpers\Url::toRoute('/cart/checkout'); ?>" class="btn btn-primary" style="text-align: right;">Перейти к оформлению</a></td>
                    </tr>                    
                </tbody>
            </table> 

            <?php ActiveForm::end() ?>
        <?php else: ?>


        </div>    
    <?php endif; ?>


</div>
