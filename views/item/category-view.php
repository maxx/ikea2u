<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =  $root->title;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $root->title;
?>
<div class="page-index">

    <h2><?php echo $root->title ?></h2>

    <?php foreach ($categories as $category): ?>
        <div class="productCategoryContainer">
            <span class="header"><?php echo $category->title ?></span>
            <div class="textContainer">
                <?php foreach ($category->children()->all() as $subcategory): ?>
                    <a href="<?php echo \yii\helpers\Url::toRoute(['/category/view', 'id' => $subcategory->id]); ?>"><?php echo $subcategory->title ?></a>
                <?php endforeach ?>
            </div>
        </div>
    <?php endforeach ?>

    <div class="clearfix"></div>

    <div class="row">
        <?php foreach ($items as $item): ?>
            <?= $this->render('_itemCell', ['item' => $item]) ?>
        <?php endforeach ?>    
    </div>

</div>

