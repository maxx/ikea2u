<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
a, a:link, a:active, a:visited {
    color: #3399fd;
    
}
.productCategoryContainer .header {
    color: #444;
    display: block;
    font-size: 1.15em;
    font-weight: bold;
    padding: 0.62em 0;
}

</style>
<div class="page-index">
    
    <h2>Категории</h2>
    
    <?php foreach ($categories as $category): ?>
        <div class="productCategoryContainer">
            <span class="header"><?php echo $category->title ?></span>
            <div class="textContainer">
                <?php foreach ($category->children()->all() as $subcategory): ?>
                    <a href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id'=>$subcategory->id]); ?>"><?php echo $subcategory->title ?></a>
                <?php endforeach ?>
            </div>
        </div>
    <?php endforeach ?>
        
</div>
