
<?php if ($item->getPrice()>1):?>
<div class="col-sm-5 col-md-2 item-wrapper">
    <div class="thumbnail">
        <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>">
            <?php // if ($this->beginCache($item->image_uri.'_small?v=1')): ?>
                <img src="<?php echo $item->image_uri ?>" alt=".." width="300">
                <?php // $this->endCache(); ?>
            <?php endif; ?>
        </a>
        <div class="caption">
            <h4><a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>"><?php echo $item->title ?></a></h4>
            <p><?php echo $item->short_description ?></p>


            <p> 
                <strong>Цена: </strong> <?php echo \app\models\Utilities::Currency($item->getPrice()) ?>
            </p>

            <a href="/cart/add" class="btn btn-default add-to-cart" role="button" data-item-id="<?php echo $item->id ?>" style="margin: 0px 0px">Купить</a>
        </div>
    </div>
</div>

<?php endif;?>