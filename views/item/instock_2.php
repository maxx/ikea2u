<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Товары в наличии';

$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'В наличии';
?>
<div class="page-index">
    <h2>В наличии</h2>
    
        <?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_list',
]);
    ?>
</div>
