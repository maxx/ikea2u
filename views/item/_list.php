    <div class="col-sm-5 col-md-2 item-wrapper">
        <div class="thumbnail">
            <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $model->id]); ?>">
                <?php // if ($this->beginCache($model->image_uri . '_small?v=1')): ?>
                    <img src="<?php echo $model->image_uri ?>" alt=".." width="300">
                    <?php // $this->endCache(); ?>
                <?php endif; ?>
            </a>
            <div class="caption">
                <h4><a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $model->id]); ?>"><?php echo $model->title ?></a></h4>
                <p><?php echo $model->short_description ?></p>
                <p>арт: <?php echo $model->number ?></p>
                <p> 
                    <strong>Цена: </strong> <?php echo \app\models\Utilities::Currency($model->stock_price) ?>
                </p>

            </div>

        </div>
    </div>