<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Товары в наличии';

$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'В наличии';
?>
<div class="page-index">
    <h2>В наличии</h2>
    
        <?php foreach ($items as $item):

            ?>

            <?php if ($item->stock_price > 1): ?>
                <div class="col-sm-5 col-md-2 item-wrapper">
                    <div class="thumbnail">
                        <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>">
                            <?php if ($this->beginCache($item->image_uri . '_small')): ?>
                                <img src="http://www.ikea.com<?php echo $item->image_uri ?>" alt=".." width="300">
                                <?php $this->endCache(); ?>
                            <?php endif; ?>
                        </a>
                        <div class="caption">
                            <h4><a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>"><?php echo $item->title ?></a></h4>
                            <p><?php echo $item->short_description ?></p>
                            <p> 
                                <strong>Цена: </strong> <?php echo \app\models\Utilities::Currency($item->stock_price) ?>
                            </p>

                        </div>

                    </div>
                </div>

            <?php endif; ?>
        <?php endforeach ?>    
    
    <?php
    // display pagination
    echo LinkPager::widget([
        'pagination' => $pages,
    ]);
    ?>
</div>
