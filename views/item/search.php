<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поиск';
$this->params['breadcrumbs'][] = ['label' => 'Товары' , 'url' => ['index']];
$this->params['breadcrumbs'][] = 'поиск';
?>
<div class="page-index">

    <h2>поиск</h2>
 
    <div class="clearfix"></div>

    <div class="row">
        <?php foreach ($items as $item): ?>
            <div class="col-sm-5 col-md-2 item-wrapper">
                <div class="thumbnail">
                    <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>">
                        <img src="http://www.ikea.com/<?php echo $item->image_uri ?>" alt=".." width="300">
                    </a>
                    <div class="caption">
                        <h4><a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>"><?php echo $item->title ?></a></h4>
                        <p><?php echo $item->short_description ?></p>
                      

                        <p> 
                            <strong>Цена: </strong> <?php echo \app\models\Utilities::Currency($item->getPrice()) ?>
                            </p>
                            
                            <a href="/cart/add" class="btn btn-default add-to-cart" role="button" data-item-id="<?php echo $item->id ?>" style="margin: 0px 0px">Купить</a>
                    </div>
                </div>
            </div>
        <?php endforeach ?>    
    </div>
    <?php
    // display pagination
echo LinkPager::widget([
    'pagination' => $pages,
]);
    ?>

</div>

