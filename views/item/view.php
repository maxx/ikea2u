<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .product-information span span {
        color: #fe980f;

        font-family: "Roboto",sans-serif;
        font-size: 30px;
        font-weight: 700;
        margin-right: 20px;
        margin-top: 0;
    }
</style>
<div class="item-view">
    <?php /* ?>
      <h1><?= Html::encode($this->title) ?></h1>
      <div class="col-sm-5 col-md-8 item-wrapper">
      <div class="thumbnail">
      <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $model->id]); ?>">
      <img src="http://www.ikea.com/<?php echo $model->image_uri ?>" alt=".." width="300">
      </a>
      <div class="caption">
      <h4><a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $model->id]); ?>"><?php echo $model->title ?></a></h4>
      <p><?php echo $model->short_description ?></p>



      <strong>Цена: </strong> <?php echo \app\models\Utilities::Currency($model->getPrice()) ?>
      </p>
      <a href="/cart/add" class="btn btn-default add-to-cart" role="button" style="float: right;" data-item-id="<?php echo $model->id ?>">Купить</a>
      <input type="text" name="" value="1" class="qty" size="3" maxlength="10" style="float: right; margin-right: 10px; " >

      <div class="clearfix"></div>
      <?php if ($model->metric): ?>
      <p class="metric"><strong><small>Размеры:</small></strong><br/>
      <small><?php echo $model->metric ?></small>
      </p>
      <?php endif; ?>

      </div>
      </div>
      </div>
      <?php */ ?>


    <div class="product-details"><!--product-details-->
        <div class="col-sm-5">
            <div class="view-product">
                <?php if ($this->beginCache($model->image_uri . '_big')): ?>
                    <img src="<?php echo $model->image_uri ?>" alt=".." width="400">
                    <?php $this->endCache(); ?>
                <?php endif; ?>

            </div>

        </div>
        <div class="col-sm-7">
            <div class="product-information"><!--/product-information-->
                <h2><?php echo $model->title ?></h2>
                <p>Артикул: <?php echo $model->number; ?></p>

                <span>
                    <span><?php echo \app\models\Utilities::Currency($model->getPrice()) ?></span>
                </span>
                <p> </p>
                <p><b>Наличие:</b> доступно к заказу</p>
                <?php if ($model->metric): ?>
                    <p class="metric"><strong>Размеры:</strong><br/>
                        <?php echo $model->metric ?>
                    </p>
                <?php endif; ?>
                <p><a href="http://www.ikea.com/<?php echo $model->ikea_url_slug ?>"  target="_blank">Подробнее о товаре...</a></p>
                <label>Количество:</label>
                <input type="text" value="1" class="qty" max="999" maxlength="4">
                <a href="/cart/add" class="btn btn-default add-to-cart" data-item-id="<?php echo $model->id ?>">
                    <i class="fa fa-shopping-cart"></i>
                    Добавить в корзину
                </a>
            </div><!--/product-information-->
        </div>
    </div>

</div>
