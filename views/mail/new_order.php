<h1>Заказ № <?php echo $order->id?></h1>
<p>Фио: <?php echo $order->customer_name?></p>
<p>Email: <?php echo $order->customer_email?></p>
<p>Телефон: <?php echo $order->customer_phone?></p>
<p>Адрес доставки: <?php echo $order->address?></p>
<p>Комментарии к заказу: <?php echo $order->note?></p>

<br/>
<p><strong>Детали заказа:</strong></p>
<?php
$str = '';
foreach ($order->orderItems as $item) {
    $str .= $item->item->id . ' ' . $item->item->title . ' ' . $item->qty . ' шт. ' . ' - ' . \app\models\Utilities::Currency($item->bel_price) . '<br/>';
}
?>
<p><?php echo $str?></p>

<p><strong>Сумма в RUR: <?php echo \app\models\Utilities::CurrencyRUR($order->total_due_rus)?></strong></p>
<p><strong>Сумма в BYR <?php echo \app\models\Utilities::Currency($order->total_due_bel)?></strong></p>