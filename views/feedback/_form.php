<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\Feedback;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <?=
                $form->field($model, 'opinion')
                ->radioList(
                        Feedback::itemAlias('Opinion'), [
                    'item' => function($index, $label, $name, $checked, $value) {

                        $return = '<label class="modal-radio">';
                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                        $return .= '<i></i>';
                        $return .= '<span>' . $label . '</span>';
                        $return .= '</label>';

                        return $return;
                    }
                        ]
                )
                ->label(false);
        ?>
        <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Добавить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    
<?php ActiveForm::end(); ?>

</div>
