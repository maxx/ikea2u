<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\IkeaOrderForm;
use yz\shoppingcart\ShoppingCart;
use \app\models\CategoryItem;
use yii\data\ActiveDataProvider;
use app\models\Feedback;
use yii\helpers\VarDumper;
use DiDom\Document;
use yii\log\Logger;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{

  public function behaviors()
  {
    return [
        'access'=>[
            'class'=>AccessControl::className(),
            'only'=>['logout'],
            'rules'=>[
                [
                    'actions'=>['logout'],
                    'allow'=>true,
                    'roles'=>['@'],
                ],
            ],
        ],
        'verbs'=>[
            'class'=>VerbFilter::className(),
            'actions'=>[
                'logout'=>['post'],
            ],
        ],
    ];
  }

  public function actions()
  {
    return [
        'error'=>[
            'class'=>'yii\web\ErrorAction',
        ],
        'captcha'=>[
            'class'=>'yii\captcha\CaptchaAction',
            'fixedVerifyCode'=>YII_ENV_TEST ? 'testme' : null,
        ],
    ];
  }

  public function actionIndex()
  {

    $items=\app\models\Item::find()->orderBy('RAND()')->limit(10)->all();

    $page=\app\models\Page::findOne(['name'=>'main']);
    $root=\app\models\Category::findOne(['title'=>'ikea_categories']);
    $categories=$root->children(1)->all();

    //last 2 positive feedback
    $dataProviderFeedback=new ActiveDataProvider([
        'query'=>Feedback::find()
            ->where(['status'=>Feedback::STATUS_CONFIRMED])
            ->orderBy('created_at DESC')
            ->limit(2),
        'pagination'=>false,
    ]);

    $feedbackCount=Feedback::find()
        ->select(['COUNT(*) AS cnt'])
        ->where(['status'=>Feedback::STATUS_CONFIRMED])
        ->count();

    return $this->render('index', [
        'page'=>$page,
        'items'=>$items,
        'categories'=>$categories,
        'dataProviderFeedback'=>$dataProviderFeedback,
        'feedbackCount'=>$feedbackCount]);
  }

  public function actionIndexNew()
  {

    $items=\app\models\Item::find()->orderBy('RAND()')->limit(10)->all();

    $page=\app\models\Page::findOne(['name'=>'main']);
    $root=\app\models\Category::findOne(['title'=>'ikea_categories']);
    $categories=$root->children(1)->all();
    $this->layout=false;
    return $this->render('indexNew', ['page'=>$page, 'items'=>$items, 'categories'=>$categories,]);
  }

  public function actionContact()
  {
    $model=new ContactForm();
    /*if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
        Yii::$app->session->setFlash('contactFormSubmitted');

        return $this->refresh();
    } else {
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }*/
    return $this->render('contact', [
        'model'=>$model,
    ]);
  }

  public function actionAbout()
  {
    return $this->render('about');
  }


  public function getAllLinks($url, $i=0, $results=array(), $went=array())
  {

    $opts=array('http'=>array('header'=>'Accept-Charset: UTF-8, *;q=0'));
    $context=stream_context_create($opts);

    $html=file_get_contents($url, false, $context);
    $dom=new \DOMDocument();
    @ $dom->loadHTML($html);

    $xp=new \DOMXPath($dom);
    //$tags = $xp->query('//a[@class="inner"]');
    $tags=$dom->getElementsByTagName('a');

    foreach ($tags as $tag) {
      $url=$tag->getAttribute('href');
      $id=$tag->getAttribute('id');
      if (strpos($id, "txt") !== false) {
        if (!isset($results[$url])) {
          $results[$url]['url']=$url;
          $results[$url]['text']=trim($tag->nodeValue);
        }
      }
    }

    $images=array();
    $context=stream_context_create(array(
        'http'=>array('ignore_errors'=>true),
    ));


    foreach ($results as $link) {

      $html=file_get_contents('http://www.ikea.com' . $link, false, $context);
      $dom=new \DOMDocument();
      @ $dom->loadHTML($html);

      $xp=new \DOMXPath($dom);
      //$tags = $xp->query('//a[@class="inner"]');
      $tags=$dom->getElementsByTagName('img');
      foreach ($tags as $tag) {
        $class=$tag->getAttribute('class');
        $src=$tag->getAttribute('src');

        if (strpos($class, "prodImg") !== false) {
          $images[$src]=$src;
        }
      }
    }
    var_dump($results, $images);
    die();
    return $results;
  }

  public function actionImg()
  {
    $img_slug=Yii::$app->getRequest()->getQueryParam('img_slug');
    $url='http://www.ikea.com' . $img_slug;
    var_dump($url);
    die();

    $imageSrc=Yii::app()->cache->get('remoteImgUrl' . $url);

    if ($imageSrc === false) {
      //$header_response = get_headers($url, 1);
      $response_code=Utilities::getUrlResponseStatus($url);
      if ($response_code !== 200) {
        //if (strpos($header_response[0], "404") !== false) {
        $url='http://musictradecompany.ru/catalogue/' . $_GET['cat_number'] . $type . '.jpg';
        //$header_response = get_headers($url, 1);
        $response_code=Utilities::getUrlResponseStatus($url);
        if ($response_code !== 200) {
          //  if (strpos($header_response[0], "404") !== false) {
          $url='images/nophoto_' . $_GET['type'] . '.jpg';
        }
      }

      //Yii::app()->cache->delete('remoteImgUrl'.$url);

      $imageSrc=file_get_contents($url);
      Yii::app()->cache->set('remoteImgUrl' . $url, $imageSrc, 604800); //week
    }

    header("Cache-Control: private, max-age=10800, pre-check=10800");
    header("Pragma: private");
    header("Expires: " . date(DATE_RFC822, strtotime(" 2 day")));

    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
      // if the browser has a cached version of this image, send 304
      header('Last-Modified: ' . $_SERVER['HTTP_IF_MODIFIED_SINCE'], true, 304);
    }

    header('Content-Type: image/jpeg');
    echo $imageSrc;
    Yii::app()->end();
  }

  //0wBe94Mjglgw9B5A
  public function actionCleanBase($key)
  {
    if ($key == '0wBe94Mjglgw9B5A232') {
      $sql='TRUNCATE TABLE item;';
      $res=\Yii::$app->db->createCommand($sql)->execute();
      var_dump($sql, $res);
      $sql='TRUNCATE TABLE category;';
      $res=\Yii::$app->db->createCommand($sql)->execute();
      var_dump($sql, $res);
      $sql='TRUNCATE TABLE category_item;';
      $res=\Yii::$app->db->createCommand($sql)->execute();
      var_dump($sql, $res);
    }
    Yii::$app->end();
  }


  public function actionPCustomCats($suburl)
  { //http://i2u.by/site/p-custom-cats?suburl=catalog/categories/departments/living_room/

    if ($suburl) {
      $root=\app\models\Category::findOne(['title'=>'ikea_categories']);
      $cats=$root->children(1)->all();

      //$site = 'http://www.ikea.com/ru/ru/catalog/allproducts/';
      $site='https://www.ikea.com/ru/ru/' . $suburl;

      $curl=curl_init();
      curl_setopt($curl, CURLOPT_URL, $site);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HEADER, false);

      $html=curl_exec($curl);
      $info=curl_getinfo($curl);

      curl_close($curl);
      $document=new Document();
      //libxml_use_internal_errors(true);
      $document->loadHTML($html);

      // $document = new Document($site, true);
      //$posts = $document->find('.productCategoryContainer a');
      $posts=$document->find('.productsFilterLinks a');
      foreach ($posts as $post) {
        $url=$post->attr('href');
        $urlSlug=dirname($url) . '/';
        $preparedUrlSlug=parse_url($urlSlug);
        $title=$post->text();
        $exist=\app\models\Category::findOne(['ikea_url_slug'=>$preparedUrlSlug['path']]);

        if ($exist) {
          $node=\app\models\Category::findOne(['ikea_url_slug'=>$url]);
          if (!$node) {
            $newCat=new \app\models\Category(['title'=>$title, 'ikea_url_slug'=>$url]);
            $newCat->appendTo($exist);
          }
        }
      }

      die('CUSTOM SUBCATEGORIES  have been created');
    } else {
      die('NEED SUBURL');
    }
  }


  //временно отключил из-за смены дизайна страниц
  public function actionPItem_()
  {

    set_time_limit(10000);
    ini_set('memory_limit', '2048M');

    try {
      // $rate = \app\models\Setting::find()->asArray()->one();
      //$rate_rus = $rate['rate_rus_rubl'];
      //$rate_additional = $rate['additional_rate'];

      //  $items = \app\models\Item::find()->where(['not', ['ikea_url_slug' => null]])->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')->limit(1000)->all();
      //    $items = \app\models\Item::find()->where(['not', ['number' => null]])->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')->limit(1000)->all();
      //$items = \app\models\Item::find()->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')->limit(1000)->all();
      $items=\app\models\Item::find()
          ->where(['image_uri'=>null])
          ->orWhere(['image_uri'=>''])
          ->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')
          ->all();

      //$items = \app\models\Item::find()->where(['<', 'bel_price', 1])->all();
      //$items = \app\models\Item::find()->where(['<', 'bel_price', 1])->orWhere([ 'number' => null])->orWhere([ 'number' => ''])->all();
      $i=0;
      $f=0;
      $deleted=0;
      $ikeaPageBlock=0;
      foreach ($items as $item) {
        $f++;
        $url=$item->ikea_url_slug;
        if ($url) {
          //$url = '/ru/ru/catalog/products/00111994/';
          $site='http://www.ikea.com/' . $url;


          $site=str_replace('com//', 'com/', $site);
          if (strpos($site, '00111994') === false && strpos($site, '30384624') === false) {
            $document=new Document($site, true);
            //$rightBar = $document->find('#rightNavInfoDiv #price1');
            // VarDumper::dump($document, 10, true);
            //die();

            $pageTitleObj=$document->find('title');
            $pageTitle=isset($pageTitleObj[0]) ? $pageTitleObj[0]->text() : null;

            $metaItemIdObj=$document->find('meta[name=partnumber]');
            $metaItemId=isset($metaItemIdObj[0]) ? $metaItemIdObj[0]->getAttribute('content') : null;

            //attention because item WILL BE DELETED
            if (!$metaItemId || (strpos($pageTitle, 'Technical Error') === false)) {

              $titleObj=$document->find('#name');
              $title=isset($titleObj[0]) ? $titleObj[0]->text() : null;
              // $shortDescObj = $document->find('#rightNavInfoDiv #type');
              //$shortDesc = isset($shortDescObj[0]) ? $shortDescObj[0]->text() : null;

              $fullDescObj=$document->find('#salesArg');
              $fullDesc=isset($fullDescObj[0]) ? $fullDescObj[0]->text() : null;

              $metricObj=$document->find('#metric');
              $metric=isset($metricObj[0]) ? $metricObj[0]->text() : null;

              $qtyInPackObj=$document->find('#storeformatpieces');
              $qtyInPack=isset($qtyInPackObj[0]) ? $qtyInPackObj[0]->text() : null;

              $imgObj=$document->find('#productImg');
              $imgSrc=isset($imgObj[0]) ? $imgObj[0]->attr('src') : null;

              //$divPrice = $document->find('#price1');
              //$priceText = isset($divPrice[0]) ? $divPrice[0]->text() : null;

              $itemNumberObj=$document->find('#rightNavInfoDiv #itemNumber');
              $itemNumber=isset($itemNumberObj[0]) ? $itemNumberObj[0]->text() : null;

              if ($imgSrc) {
                //$price = substr($priceText, 0, strpos($priceText, ".–"));
                //$price = filter_var($priceText, FILTER_SANITIZE_NUMBER_INT);
                //$item->rus_price = $price;
                //$item->bel_price = \app\models\IkeaItem::calcBelPrice($price, $rate_additional, $rate_rus);
                //$item->title = trim($title);
                $item->image_uri=$imgSrc;
                //$item->short_description = trim($shortDesc);
                $item->full_description=trim(str_replace('Подробнее', '', $fullDesc));
                $item->metric=trim($metric);
                //$item->qty_in_pack = ((int) $qtyInPack > 0) ? $qtyInPack : 1;
                $item->updated_at=date('Y-m-d H:i:s');
                if ($itemNumber) {
                  //$item->number = $itemNumber;
                }

                if (!$item->save()) {
                  var_dump($item->getErrors());
                  die();
                } else {
                  $i++;
                }
              } else {
                var_dump($item->number);
                //var_dump($item->title);
                //  $item->ikea_url_slug = null;
                // $item->save();
                /* if ($item->delete()) {
                  $deleted++;
                  } */
              }
            } else {
              $ikeaPageBlock++;
              // $res = $item->delete(); //attention because item WILL BE DELETED

            }
          } else {

          }
        }
      }
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      die();
    }


    $arr=array();
    $arr['success']=$i;
    $arr['attempts']=$f;
    $arr['deleted']=$deleted;
    $arr['ikeaPageBlock']=$ikeaPageBlock;
    print_r(json_encode($arr));
    die();
  }

  //SQL для АЛьберта
  //INSERT INTO `ikea2u`.`item` (`number`, `bel_price`, `rus_price`, `image_uri`, `title`, `short_description`, `full_description`, `qty_in_pack`) VALUES ('303.846.24', '291000', '599', 'https://www.ikea.com/PIAimages/0515306_PE639886_S5.JPG?f=s', 'Альберт', 'Данный стеллаж не только служит местом для хранения вещей, но и помогает освежить и украсить интерьер. Модуль не займёт много места в комнате. Вы можете использовать его для зонирования пространства в доме или разместить сувениры, цветы в кашпо и другую домашнюю утварь.', 'Упаковка и фасовка\r\nВ боксе 1 шт\r\nФасовка по 1 шт\r\nРазмер упаковки 159 см × 14,5 см × 6,9 см\r\nГабариты и вес\r\nРазмер 159 см × 14,5 см × 6,9 см\r\nВес брутто 6.4 кг\r\nОсобенности\r\nШирина, мм 640\r\nГлубина, мм 280\r\nВысота, мм 1590\r\nМатериал корпуса Массив\r\nЦвет мебели Коричневый\r\nМатериал Дерево', '1');


  public function actionPItemNumber()
  {

    set_time_limit(10000);
    ini_set('memory_limit', '2048M');
    try {
      $rate=\app\models\Setting::find()->asArray()->one();
      $rate_rus=$rate['rate_rus_rubl'];
      $rate_additional=$rate['additional_rate'];

      $items=\app\models\Item::find()
          ->where(['not', ['number'=>null]])
          ->andWhere(['ikea_url_slug'=>null])
          ->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')
          ->all();
      /*
        $query = \app\models\Item::find()
        ->where(['not', ['number' => null]])
        ->andWhere(['ikea_url_slug' => null])
        ->orderBy('`updated_at` IS NULL DESC, `updated_at` ASC')
        ;
        var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        die(); */
      $i=0;
      $f=0;
      $deleted=0;
      $ikeaPageBlock=0;
      foreach ($items as $item) {
        if ($item->number) {
          $f++;
          $url='/ru/ru/search/?query=' . $item->number;
          $site='http://www.ikea.com/' . $url;
          $site=str_replace('com//', 'com/', $site);
          if (strpos($site, '00111994') === false) {
            $document=new Document($site, true);
            //$rightBar = $document->find('#rightNavInfoDiv #price1');

            $pageTitleObj=$document->find('title');
            $pageTitle=isset($pageTitleObj[0]) ? $pageTitleObj[0]->text() : null;

            if (!$pageTitle || (strpos($pageTitle, 'Technical Error') === false)) {

              $urlSlugObj=$document->find('#schemaProductUrl');
              $ikea_url_slug=isset($urlSlugObj[0]) ? $urlSlugObj[0]->text() : null;
              $ikea_url_slug=str_replace('http://www.ikea.com', '', $ikea_url_slug);

              $titleObj=$document->find('#name');
              $title=isset($titleObj[0]) ? $titleObj[0]->text() : null;
              $shortDescObj=$document->find('#rightNavInfoDiv #type');
              $shortDesc=isset($shortDescObj[0]) ? $shortDescObj[0]->text() : null;

              $fullDescObj=$document->find('#salesArg');
              $fullDesc=isset($fullDescObj[0]) ? $fullDescObj[0]->text() : null;

              $metricObj=$document->find('#metric');
              $metric=isset($metricObj[0]) ? $metricObj[0]->text() : null;

              $qtyInPackObj=$document->find('#storeformatpieces');
              $qtyInPack=isset($qtyInPackObj[0]) ? $qtyInPackObj[0]->text() : null;

              $imgObj=$document->find('#productImg');
              $imgSrc=isset($imgObj[0]) ? $imgObj[0]->attr('src') : null;

              $divPrice=$document->find('#price1');
              $priceText=isset($divPrice[0]) ? $divPrice[0]->text() : null;

              $itemNumberObj=$document->find('#rightNavInfoDiv #itemNumber');
              $itemNumber=isset($itemNumberObj[0]) ? $itemNumberObj[0]->text() : null;

              if ($priceText && $title && $ikea_url_slug) {
                $price=substr($priceText, 0, strpos($priceText, ".–"));
                $price=filter_var($priceText, FILTER_SANITIZE_NUMBER_INT);
                $item->ikea_url_slug=$ikea_url_slug;
                $item->rus_price=$price;
                $item->bel_price=\app\models\IkeaItem::calcBelPrice($price, $rate_additional, $rate_rus);
                $item->title=trim($title);
                $item->image_uri=$imgSrc;
                $item->short_description=trim($shortDesc);
                $item->full_description=trim(str_replace('Подробнее', '', $fullDesc));
                $item->metric=trim($metric);
                $item->qty_in_pack=((int)$qtyInPack > 0) ? $qtyInPack : 1;
                $item->updated_at=date('Y-m-d H:i:s');

                if ($itemNumber) {
                  $item->number=$itemNumber;
                }

                /*  if ($item->id == '28823') {
                  var_dump('----------------------------------');
                  var_dump($item);
                  var_dump($item->getErrors());
                  die();
                  } else {

                  } */

                if (!$item->save()) {
                  var_dump($item->getErrors());
                } else {
                  $i++;
                }
              } else {
                var_dump($item->validate());
                var_dump($item->getErrors());
                var_dump($item->attributes);
                var_dump($item->getErrors());
                //  $item->ikea_url_slug = null;
                // $item->save();
                /* if ($item->delete()) {
                  $deleted++;
                  } */
              }
            } else {
              $ikeaPageBlock++;
            }
          } else {

          }
        }
      }
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      // die();
    }


    $arr=array();
    $arr['success']=$i;
    $arr['attempts']=$f;
    $arr['deleted']=$deleted;
    $arr['ikeaPageBlock']=$ikeaPageBlock;
    print_r(json_encode($arr));
    die();
  }


  public function actionPItemStockSql()
  {
    $items=\app\models\Item::find()
        ->where(['not', ['in_stock'=>null]])
        ->all();

    foreach ($items as $item) {
      echo "UPDATE `ikea2u`.`item` SET `in_stock`='{$item->in_stock}', `stock_price`='{$item->stock_price}' WHERE  `number`='{$item->number}';" . "<br/>";
      //"INSERT INTO `ikea2u`.`item` (`number`, `bel_price`, `rus_price`, `image_uri`, `title`, `short_description`, `full_description`, `created_at`, `updated_at`, `ikea_url_slug`, `metric`, `qty_in_pack`, `key_features`, `package_info`, `care_inst`) VALUES ('1402.806.94', '3750000', '9999', '/ru/ru/images/products/varv-torser-s-besprovodnoj-zaradkoj__0363198_PE548608_S4.JPG', 'ВАРВ', 'торшер с беспроводной зарядкой', 'Вы легко можете заряжать телефон с помощью беспроводного зарядного устройства. Если ваш телефон поддерживает беспроводную зарядку, просто разместите его на интегрированном зарядном устройстве. А если нет – дополните телефон специальным чехлом ВИТАХУЛЬТ.', '2017-03-21 12:12:33', '2017-03-22 07:43:51', '/ru/ru/catalog/products/102830694/', 'Ширина абажура: 39.4 смДиаметр: 30 смВысота: 169 смДлина провода: 250 см', '1', NULL, NULL, NULL)";
    }


  }

}
