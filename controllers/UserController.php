<?php

namespace app\controllers;

use Yii;
use \app\models\User;
use yii\helpers\Url;
use app\models\LoginForm;
use yii\web\NotFoundHttpException;

class UserController extends DefaultController {

    public function actionLogin() {

        $x = Yii::$app->security->generatePasswordHash('password');

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->role === User::ROLE_ADMIN) {
                return $this->redirect('/admin');
            } else {

                return $this->goBack();
            }
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

}
