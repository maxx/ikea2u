<?php

namespace app\controllers;

use Yii;
use app\models\IkeaOrderForm;
use app\models\DeliveryForm;
use app\models\Order;
use app\models\OrderItem;
use app\models\Item;
use yii\helpers\Url;
use app\models\Utilities;
use yii\data\ActiveDataProvider;
use app\models\Category;
use yii\web\NotFoundHttpException;

class CategoryController extends DefaultController {

    public function actionView($id) {

        $root = $this->findModel($id);
        $categories = $root->children(1)->all();
        $items = $root->getItems()->all();
            

        //$categories = \app\models\Category::find()->all();
        return $this->render('view', [
                    'categories' => $categories,
                    'root'=>$root,
                    'items' => $items
        ]);
    }
    
        /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
