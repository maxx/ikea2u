<?php

namespace app\controllers;

use Yii;
use app\models\IkeaOrderForm;
use app\models\DeliveryForm;
use app\models\Order;
use app\models\OrderItem;
use app\models\Item;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use app\models\Utilities;
use yii\data\ActiveDataProvider;
use app\models\Category;
use \yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ItemController extends DefaultController
{

  public function actionIndex()
  {
    $dataProvider=new ActiveDataProvider([
        'query'=>Item::find(),
    ]);


    $root=Category::findOne(['title'=>'ikea_categories']);
    $categories=$root->children(1)->all();

    //$categories = \app\models\Category::find()->all();
    return $this->render('index', [
        'dataProvider'=>$dataProvider,
        'categories'=>$categories,
    ]);
  }

  /*
    public function actionWinter()
    {

      $q=['VINTER', 'ВИНТЕР', 'гирлянда', 'венок'];
      $where=[];
      $query=Item::find();
      foreach ($q as $value) {
        $query->orWhere(['LIKE', 'title', $value]);
        $query->orWhere(['LIKE', 'short_description', $value]);
      }


      $dataProvider=new ActiveDataProvider([
          'query'=>$query,
          'sort'=>[
              'attributes'=>[
                  'bel_price'=>[
                      'label'=>'Цена',
                  ],
                  'title'=>[
                      'label'=>'Название',
                  ],
              ]
          ],
      ]);

      return $this->render('search', [
          'dataProvider'=>$dataProvider
      ]);
    }

    public function actionStrola()
    {

      $q=['строла'];
      $where=[];
      $query=Item::find();
      foreach ($q as $value) {
        $query->orWhere(['LIKE', 'title', $value]);
        $query->orWhere(['LIKE', 'short_description', $value]);
      }


      $dataProvider=new ActiveDataProvider([
          'query'=>$query,
          'sort'=>[
              'attributes'=>[
                  'bel_price'=>[
                      'label'=>'Цена',
                  ],
                  'title'=>[
                      'label'=>'Название',
                  ],
              ]
          ],
      ]);

      return $this->render('search', [
          'dataProvider'=>$dataProvider
      ]);
    }
  */
  public function actionCategoryView($id)
  {

    $root=Category::findOne($id);
    if (!$root)
      throw new NotFoundHttpException('Страница не найдена.');
    $categories=$root->children()->all();
    $query=$root->getReadyItemsNEW($categories, $id);
    if (Yii::$app->user->isGuest) {

    } else {
      /* echo '<pre>';
       var_dump($query);
       echo '</pre>';
       die();*/
    }

    /*$ar=[];
    foreach ($categories as $cat) {
      $all=$cat->getReadyItems()->all();
      $ar=array_merge($ar, $all);
    }
    $dataProvider=new ArrayDataProvider([
        'allModels'=>$ar,
        'sort'=>[
            'attributes'=>[
                'bel_price'=>[
                    'label'=>'Цена',
                ],
                'title'=>[
                    'label'=>'Название',
                ],
            ]
        ],
    ]);*/

    $dataProvider=new ActiveDataProvider([
        'query'=>$query,
        'sort'=>[
            'attributes'=>[
                'bel_price'=>[
                    'label'=>'Цена',
                ],
                'title'=>[
                    'label'=>'Название',
                ],
            ]
        ],
    ]);

    if (Yii::$app->request->isAjax) {
      return $this->renderPartial('_category-view', [
          'categories'=>$categories,
          'root'=>$root,
          'dataProvider'=>$dataProvider
      ]);
    }

    //$categories = \app\models\Category::find()->all();
    return $this->render('category-view', [
        'categories'=>$categories,
        'root'=>$root,
        'dataProvider'=>$dataProvider
    ]);
  }

  public function actionSearch($q)
  {

    $q=strip_tags(trim($q));
    $request=Yii::$app->request;
    $page=$request->get('page', null);
    if (!$page) {
      $res=\app\models\ItemService::createOrUpdateFromIkea($q);

      if (is_null($res)) {
        $res=\app\models\ItemService::createOrUpdateFromIkeaByString($q);
      }
    }


    $query=Item::find()
        ->where(['LIKE', 'number', $q])
        ->orWhere(['LIKE', "replace(number, '.', '')", $q])
        ->orWhere(['LIKE', 'title', $q])
        ->orWhere(['LIKE', 'short_description', $q])
        ->orWhere(['LIKE', 'full_description', $q])
        ->andWhere(['>', 'bel_price', 0]);

    $dataProvider=new ActiveDataProvider([
        'query'=>$query,
        'sort'=>[
            'attributes'=>[
                'bel_price'=>[
                    'label'=>'Цена',
                ],
                'title'=>[
                    'label'=>'Название',
                ],
            ]
        ],
    ]);

    // $countQuery = clone $query;
    //   $pages = new Pagination(['totalCount' => $countQuery->count()]);
    /* $models = $query->offset($pages->offset)
             ->limit($pages->limit)
             ->all();*/

    return $this->render('search', [
      //      'items' => $models,
        'dataProvider'=>$dataProvider
      //  'pages' => $pages,
    ]);
  }

  public function actionInstock()
  {

    /*$query = \app\models\ItemStock::find()
            ->where(['>', 'in_stock', 0])
            ->andWhere(['>', 'price', 0])
            ->andWhere(['>', 'number', 0]);

    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'sort' => [
            'attributes' => [
                'price' => [
                    'label' => 'Цена',
                ],
                'title' => [
                    'label' => 'Название',
                ],
            ]
        ],
    ]);
    if (Yii::$app->request->isAjax) {
        return $this->renderPartial('_instock', [
                    'dataProvider' => $dataProvider,
        ]);
    }
*/
    return $this->render('instock', [
      //'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Item model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
        'model'=>$this->findModel($id),
    ]);
  }

  /**
   * Finds the Item model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Item the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model=Item::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
