<?php

namespace app\controllers;

use Yii;
use app\models\Feedback;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\DefaultController;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends DefaultController {

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Feedback::find()
                    ->where(['status' => Feedback::STATUS_CONFIRMED])
                    ->orderBy('created_at DESC'),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post())) {
            $model->status = Feedback::STATUS_ADDED;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Спасибо за отзыв! Вы помогаете стать нам лучше.');
                $body = 'Отзыв<br>';
                $body .= $model->message;
                $m = Yii::$app->mailer->compose('/mail/notify', ['body' => $body])
                        ->setFrom('sales@i2u.by')
                        ->setTo(Yii::$app->params['adminEmail'])
                        ->setSubject('Получен новый отзыв #' . $model->id)
                        ->send();

                return $this->redirect('/feedback');
            } else {
                
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
