<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\IkeaOrderForm;
use yz\shoppingcart\ShoppingCart;
use \app\models\CategoryItem;
use yii\data\ActiveDataProvider;
use app\models\Feedback;
use yii\helpers\VarDumper;
use DiDom\Document;
use yii\log\Logger;
use yii\web\NotFoundHttpException;

class LabController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCats() {
         $cats = \app\models\Category::find()->orderBy('`id` ASC')->all();
         echo 'Count: '.count($cats). "<br>";
        foreach ($cats as $cat) {
            echo $cat->ikea_url_slug . "<br>";
        }
        die();
    }

}
