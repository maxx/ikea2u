<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\IkeaOrderForm;
use yz\shoppingcart\ShoppingCart;
use \app\models\CategoryItem;
use yii\data\ActiveDataProvider;
use app\models\Feedback;
use yii\helpers\VarDumper;
use DiDom\Document;
use yii\log\Logger;
use yii\web\NotFoundHttpException;

class TestController extends Controller
{

  public function behaviors()
  {
    return [
        'access'=>[
            'class'=>AccessControl::className(),
            'only'=>['logout'],
            'rules'=>[
                [
                    'actions'=>['logout'],
                    'allow'=>true,
                    'roles'=>['@'],
                ],
            ],
        ],
        'verbs'=>[
            'class'=>VerbFilter::className(),
            'actions'=>[
                'logout'=>['post'],
            ],
        ],
    ];
  }

  public function actions()
  {
    return [
        'error'=>[
            'class'=>'yii\web\ErrorAction',
        ],
        'captcha'=>[
            'class'=>'yii\captcha\CaptchaAction',
            'fixedVerifyCode'=>YII_ENV_TEST ? 'testme' : null,
        ],
    ];
  }

  public function actionI()
  {
    phpinfo();
    die();
  }

  public function actionMail()
  {
    $alsophpmail=Yii::$app->getRequest()->getQueryParam('phpmail');
    if ($alsophpmail == true) {

      $to='maxx@tut.by';
      $subject='the subject';
      $message='hello';
      $headers='From: sales@i2u.by' . "\r\n" .
          'Reply-To: sales@i2u.by' . "\r\n" .
          'X-Mailer: PHP/' . phpversion();

      mail($to, $subject, $message, $headers);
    } else {

      $order=\app\models\Order::find()
          ->orderBy('id DESC')
          ->limit(1)
          ->offset(1)
          ->one();


      $m=Yii::$app->mailer->compose('/mail/new_order', ['order'=>$order])
          ->setFrom('sales@i2u.by')
          ->setTo(Yii::$app->params['adminEmail'])
          ->setSubject('Новый заказ №' . $order->id . ' Сумма: ' . \app\models\Utilities::CurrencyRUR($order->total_due_rus))
          ->send();
    }
    return;
  }

}
