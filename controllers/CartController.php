<?php

namespace app\controllers;

use Yii;
use app\models\IkeaOrderForm;
use app\models\DeliveryForm;
use app\models\Order;
use app\models\OrderItem;
use app\models\Item;
use yii\helpers\Url;
use app\models\Utilities;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;

class CartController extends DefaultController
{

  public function actionIndex()
  {

    $model=new IkeaOrderForm();

    if (!empty($_POST['items'])) {
      foreach ($_POST['items'] as $item=>$qty) {
        $ikeaItem=\Yii::$app->cart->getPositionById($item);
        if ($ikeaItem && $qty < 1000)
          \Yii::$app->cart->update($ikeaItem, $qty);
      }
      $this->refresh();
    }
    return $this->render('index', [
        'model'=>$model,
    ]);
  }

  /**
   * Delete item from shopping card
   */
  public function actionDelete()
  {


    $res['code']='404';

    $ikeaItem=\Yii::$app->cart->getPositionById($_POST['id']);
    if ($ikeaItem) {
      \Yii::$app->cart->remove($ikeaItem);
      $res['code']=200;
      //$res['count'] = Utilities::Currency(\Yii::$app->cart->getItemsCount(), false);
      $res['cost']=Utilities::Currency(\Yii::$app->cart->cost);
      $res['cart_html']=\app\components\ShoppingCartWidget::widget();
    }
    echo json_encode($res);

    Yii::$app->end();
  }

  public function actionStop()
  {
    return $this->render('stop');
  }

  public function actionCheckout()
  {
    $this->redirect(Url::toRoute('cart/stop'));

    if (Yii::$app->cart->getIsEmpty())
      $this->redirect('/cart');

    $model=new DeliveryForm();
    /*if ($model->load(Yii::$app->request->post())) {
      if ($model->validate()) {
        $order=new Order();
        $order->user_id=Yii::$app->user->id;
        $order->customer_name=$model->name;
        $order->customer_email=$model->email;
        $order->customer_phone=$model->phone;
        $order->address=$model->address;
        $order->note=$model->note;
        $order->status=Order::STATUS_ADDED;
        $order->total_due_bel=(float)Utilities::currencyDenom(\Yii::$app->cart->cost, false, false);
        $order->save();

        $total_due_rus=0;

        foreach (\Yii::$app->cart->getPositions() as $item) {

          $orderItem=new OrderItem();
          $orderItem->order_id=$order->id;
          $orderItem->item_id=$item->id;
          $orderItem->number=$item->number;
          $orderItem->short_description=$item->short_description;
          $orderItem->rus_price=$item->rus_price;
          $orderItem->bel_price=(float)Utilities::currencyDenom($item->getPrice(), false, false);
          $orderItem->total_bel_price=(float)Utilities::currencyDenom($item->getCost(), false, false);
          $orderItem->title=$item->title;
          $orderItem->ikea_url_slug=$item->ikea_url_slug;
          $orderItem->qty=$item->getQuantity();
          $orderItem->total_rus_price=$item->rus_price * $item->getQuantity();
          $orderItem->save();

          $total_due_rus+=$orderItem->rus_price;

        }

        $order->total_due_rus=$total_due_rus;
        $order->save();
        \Yii::$app->cart->removeAll();

        $this->redirect(Url::toRoute('cart/thank-you'));
      }
    }*/
    return $this->render('checkout', [
        'model'=>$model,
    ]);
  }

  public function actionThankYou()
  {
    return $this->render('thankYou', [
    ]);
  }

  /**
   * Clear shopping card
   */
  public function actionClear()
  {
    \Yii::$app->cart->removeAll();
    $this->redirect(Url::toRoute('cart/index'));
  }

  public function actionAdd()
  {
    $res['code']='404';
    $res['cart_qty']=0;
    $res['cart_total_price']=0;

    $id=Yii::$app->getRequest()->post('id');
    $qty=(int)Yii::$app->getRequest()->post('qty');
    $item=Item::findOne(['id'=>$id]);
    if ($item && $qty < 1000) {
      $res['code']='200';
      \Yii::$app->cart->put($item, $qty);

      $res['cart_qty']=\Yii::$app->cart->getCount();
      $res['cart_total_price']=Utilities::Currency(\Yii::$app->cart->getCost());
      $res['cart_total_items']=\Yii::$app->cart->getCount();
      $res['cart_html']=\app\components\ShoppingCartWidget::widget();
    }

    return json_encode($res);
  }

}
