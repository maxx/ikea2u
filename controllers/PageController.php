<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Page;
use yii\data\ActiveDataProvider;
use app\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends DefaultController
{
   
 
   
    /**
     * Displays a single Page model.
     * @param string $id
     * @return mixed
     */
    public function actionView($name)
    {
        
        return $this->render('view', [
            'model' => $this->findModel($name),
        ]);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($name)
    {
        if (($model = Page::findOne($name)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
