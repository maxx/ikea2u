<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if (Yii::$app->cart->getIsEmpty()): ?>
    <div class="container lg-padding text-center">
        <h4 class="text-muted">Ваша корзина пуста.</h4>
        <a href="/catalog" class="btn btn-primary">Вернуться в каталог</a>
    </div>
<?php else: ?>
    <?php
    $form = ActiveForm::begin([
                'id' => 'cart-form',
            ])
    ?>
    <div class="container cart">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>#</th><th></th>
                            <th class="text-left">Название, цена бел.руб</th>
                            <th class="text-center">Кол-во</th><th  class="text-center">Сумма</th><th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach (\Yii::$app->cart->getPositions() as $ikeaItem):
                            ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td class="product-thumbnail text-left"><img width="100" src="<?php echo  \app\models\Utilities::imageUrl($ikeaItem->image_uri) ?>"></td>
                                <td class="product-name text-left col-md-5">
                                    <a href=""><?php echo $ikeaItem->title ?> (арт. <?php echo $ikeaItem->number ?>)</a>
                                    <span class="price">
                                        <span class="amount"><?php echo app\models\Utilities::currencyDenom($ikeaItem->getPrice(), FALSE, false) ?></span>
                                    </span>
                                    <small><?php echo $ikeaItem->short_description ?></small>
                                    <small><?php echo isset($ikeaItem->qty_in_pack) ? 'В упаковке: ' . $ikeaItem->qty_in_pack . ' шт.' : '' ?></small>
                                </td>

                                <td class="product-quantity">
                                    <a href="" class="minus">-</a>
                                    <input type="text" id="item_<?php echo $ikeaItem->id ?>" name="items[<?php echo $ikeaItem->id ?>]" value="<?php echo $ikeaItem->getQuantity() ?>" class="qty" >
                                    <a href="" class="plus">+</a>
                                </td>
                                <td class="product-subtotal"><?php echo app\models\Utilities::currencyDenom($ikeaItem->getCost(), FALSE, false) ?></td>
                                <td class="product-remove text-center">
                                    <a href="<?php echo \yii\helpers\Url::toRoute('/cart/delete'); ?>" data-item="<?php echo $ikeaItem->id ?>" class="delete"><img src="/img/close.png" alt=""></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                        <tr class="">
                            <th colspan="3"></td>
                            <td class="text-primary">Итого:</td> 
                            <td class="text-primary"><span class="cart-cost"><?php echo app\models\Utilities::currencyDenom(\Yii::$app->cart->cost, FALSE, false) ?></span></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <div class="coupon"><a href="<?php echo \yii\helpers\Url::toRoute('/cart/clear'); ?>" class="">Очистить корзину</a></div>
            </div>
            
            <div class="container inforow no-padding">
                <div class="col-sm-6 text-left">
                    <?= Html::submitButton('Пересчитать корзину', ['class' => 'btn btn-primary']) ?> 
                </div>
                <div class="col-sm-6 text-right">  <a href="<?php echo \yii\helpers\Url::toRoute('/cart/checkout'); ?>" class="btn btn-default btn-lg btn-extra">Перейти к оформлению</a></div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end() ?>

<?php endif; ?>