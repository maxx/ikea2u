<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Адрес доставки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container cart">
    <div class="col-sm-7 col-sm-offset-0">
        <p>Пожалуйста, заполните поля для оформления заказа</p>
        
            <?php
            $form = ActiveForm::begin([
                        'id' => 'delivery-form',
                        'options' => ['class' => ''],
                        'fieldConfig' => [
                            'template' => "{input}<div class=\"\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-2 control-label'],
                        ],
            ]);
            ?>

            <?= $form->field($model, 'name')->textInput(array('placeholder' => 'Ваше имя',)) ?>
            <?= $form->field($model, 'phone')->textInput(array('placeholder' => 'Телефон',)) ?>
            <?= $form->field($model, 'email')->textInput(array('placeholder' => 'Email',)) ?>
            <?= $form->field($model, 'address')->textarea(array('placeholder' => 'Адрес','rows'=>4)) ?>
            <?= $form->field($model, 'note')->textarea(array('placeholder' => 'Комментарии к заказу','rows'=>4)) ?>
        
         <?= Html::submitButton('Оформить заказ', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?> <div class="text-muted" >
            После оформления заказа с вами свяжется менеджер.
        <?php ActiveForm::end(); ?>
</div>
       
    </div>
</div>
