<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title='Каталог';
$this->params['breadcrumbs'][]=$this->title;
$forbidCats=[67,//Еда в ИКЕА
    137,//Хлеб и молочные продукты
    190,//Продукты из мяса
    329,//Перекус с собой
    397,//Выпечка, десерты и печенье
    436,//Овощи и гарниры
    541,//Соусы и джемы
    609,//Гарниры и соусы
    745 //Напитки
];

?>
  <div class="container">
    <p>*Цены в каталоге указаны с учётом доставки в Гомель. <br/>
      **При заказе на сумму более 40 бел. руб. доставка на дом до квартиры по Гомелю осуществляется бесплатно. При
      заказе на сумму менее 40 бел. руб., стоимость доставки заказа составит 3 бел. руб.</p>
    <div class="row categories-list">
      <?php foreach ($categories as $category): ?>
        <div class="col-md-3 col-sm-4">
          <div class="card">
            <div class="card-body">
              <ul class="flat">
                <li>
                  <?php if ($category->main_image_src): ?>
                    <img src="<?php echo $category->main_image_src ?>" class="img-responsive" alt="">
                    <br>
                  <?php endif; ?>
                  <h4>
                    <a
                      href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id'=>$category->id]); ?>"><?php echo $category->title ?></a>
                  </h4>
                  <ul>
                    <?php foreach ($category->children()->all() as $subcategory): ?>
                      <?php if (!in_array($subcategory->id, $forbidCats)): ?>
                        <li style="padding-left:<?php echo $subcategory->depth * 25 - 25 ?>px ">
                          <a
                            href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id'=>$subcategory->id]); ?>">
                            <?php echo $subcategory->depth == 2 ? '<strong>' . $subcategory->title . '</strong>' : $subcategory->title ?>
                          </a>
                        </li>
                      <?php endif; ?>
                    <?php endforeach ?>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      <?php endforeach ?>
      <!--<div class="col-md-3 col-sm-4">
        <div class="card">
          <div class="card-body">
            <ul class="flat">
              <li>
                <img src="https://www.ikea.com/global/assets/navigation/images/winter-holiday-wt001.jpeg?imwidth=400"
                     class="img-responsive" alt="">
                <br>
                <h4>
                  <a
                    href="/winter">Зимняя коллекция</a>
                </h4>
              </li>
            </ul>
          </div>
        </div>
      </div>-->
    </div>
  </div>
<?php
