<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title=$model->short_description;
$this->params['breadcrumbs'][]=['label'=>'Каталог', 'url'=>['index']];
$this->params['breadcrumbs'][]=$this->title;

$images=$model->image_json ? json_decode($model->image_json) : null;

?>

  <!-- CONTAINER -->
  <div class="container type-product inforow">
    <div class="col-sm-8 text-center magnific-wrap">
      <div class="img-medium ">
        <?php /* ?><div class="sticker sticker-primary sticker-lg">New</div><?php */ ?>

        <?php if (!empty($images)): ?>
          <ul class="magnific-wrap-internal">
            <?php foreach ($images as $img): ?>
            <li>
              <a href="<?php echo \app\models\Utilities::imageUrl($img) ?>" title="<?php echo $model->title ?>" class="magnific">
                <img src="<?php echo \app\models\Utilities::imageUrl($img) ?>">
              </a>
              <?php endforeach; ?>
            </li>
          </ul>
        <?php else: ?>
          <ul>

            <li><a href="<?php echo \app\models\Utilities::imageUrl($model->image_uri) ?>" title="<?php echo $model->title ?>" class="magnific">
                <img src="<?php echo \app\models\Utilities::imageUrl($model->image_uri) ?>" alt=".."
                     style="width: 400px">

              </a></li>
          </ul>
        <?php endif; ?>
        <!-- <div class="product-nav">
           <a href="" class="product-prev arrow prev"><i></i></a>
           <div class="product-count"></div>
           <a href="" class="product-next arrow next"><i></i></a>
         </div>-->
      </div>
    </div>
    <div class="col-sm-4">
      <h3><?php echo $model->title ?></h3>
      <p><?php echo $model->short_description; ?></p>
      <p>Артикул: <?php echo $model->number; ?></p>
      <span class="price">
            <span class="amount-denom"><?php echo \app\models\Utilities::currencyDenom($model->getPrice()) ?></span>
        <span class="priceSpan"><?php echo \app\models\Utilities::priceSpan($model->price_span) ?></span>
        </span>
      <hr/>

      <?php if ($model->metric): ?>
        <p class="metric"><strong>Размеры:</strong><br/>
          <?php echo $model->metric ?>
        </p>
      <?php endif; ?>
      <p><a href="<?php echo \app\models\Utilities::ikeaUrl($model->ikea_url_slug) ?>" target="_blank">Подробнее о
          товаре...</a></p>
      <hr/>
	     <?php /*
      <div class="product-quantity inline">
        <a href="" class="minus">-</a>
        <input type="text" value="1" class="qty" max="999">
        <a href="" class="plus">+</a>
      </div>
	   <?php /*
      <a href="/cart/add" class="btn btn-primary add-cart icon add-to-cart" data-icon="k"
         data-item-id="<?php echo $model->id ?>">Добавить в корзину</a>
      <hr/>
      <?php /*
          <table class="table cart-total">
          <tr>
          <th>SKU</th>
          <td class="text-muted">345 678 0001</td>
          </tr>
          <tr>
          <th>Category</th>
          <td><a href="shop_grid_sidebar_2.html">Bar Stool</a>, <a href="shop_list_sidebar_2.html">Armchair</a></td>
          </tr>
          <tr>
          <th>Tags</th>
          <td><a href="shop_grid.html">Contemporary</a>, <a href="shop_list_sidebar_1.html">Plastic</a>, <a href="shop_list_sidebar_2.html">Outdoor</a></td>
          </tr>
          </table><?php */ ?>
    </div>
  </div>

<?php /*


  <div class="item-view">



  <div class="product-details"><!--product-details-->
  <div class="col-sm-5">
  <div class="view-product">
  <?php if ($this->beginCache($model->image_uri . '_big')): ?>
  <img src="http://www.ikea.com/<?php echo $model->image_uri ?>" alt=".." width="400">
  <?php $this->endCache(); ?>
  <?php endif; ?>

  </div>

  </div>
  <div class="col-sm-7">
  <div class="product-information"><!--/product-information-->
  <h2><?php echo $model->title ?></h2>
  <p>Артикул: <?php echo $model->number; ?></p>

  <span>
  <span><?php echo \app\models\Utilities::Currency($model->getPrice()) ?></span>
  </span>
  <p> </p>
  <p><b>Наличие:</b> доступно к заказу</p>
  <?php if ($model->metric): ?>
  <p class="metric"><strong>Размеры:</strong><br/>
  <?php echo $model->metric ?>
  </p>
  <?php endif; ?>
  <p><a href="http://www.ikea.com/<?php echo $model->ikea_url_slug ?>"  target="_blank">Подробнее о товаре...</a></p>
  <label>Количество:</label>
  <input type="text" value="1" class="qty" max="999" maxlength="4">
  <a href="/cart/add" class="btn btn-default add-to-cart" data-item-id="<?php echo $model->id ?>">
  <i class="fa fa-shopping-cart"></i>
  Добавить в корзину
  </a>
  </div><!--/product-information-->
  </div>
  </div>

  </div>
 */ ?>