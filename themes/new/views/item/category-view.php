<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title=$root->title;
$this->params['breadcrumbs'][]=['label'=>'Каталог', 'url'=>['index']];
$this->params['breadcrumbs'][]=$root->title;
?>
<?php if (!empty($categories)): ?>
  <div class="container" style="padding: 0">
    <div class="categories-short-list row hidden-xs">
      <h6>Подкатегории</h6>
      <ul>
        <?php foreach ($categories as $subcategory): ?>
          <li><a
              href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id'=>$subcategory->id]); ?>"><?php echo $subcategory->title ?></a>
          </li>
        <?php endforeach ?>
      </ul>
    </div>
  </div>
<?php endif; ?>
<div class="container categories">
  <div class="container products text-center">

    <?php
    echo ListView::widget([
        'dataProvider'=>$dataProvider,
        'layout'=>"<div class='subhead'>Сортировка:</div>{sorter}\n<div class=\"items-list\">{items}\n{pager}</div>",
        'itemView'=>'_itemCell',
        'id'=>'my-listview-id',
        'itemOptions'=>[
            'tag'=>'div',
            'class'=>'product',
        ],
        'pager'=>[
            'class'=>InfiniteScrollPager::className(),
            'widgetId'=>'my-listview-id',
            'itemsCssClass'=>'items',

        ],
    ]);
    ?>

  </div>
</div>


