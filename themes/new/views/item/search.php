<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поиск';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container categories">
    <div class="container products text-center">
        <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "<div class='subhead'>Сортировка:</div>{sorter}\n{items}\n{pager}",
            'itemView' => '_itemCell',
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'product',
            ],
        ]);
        ?>
      
    </div>
</div>

<?php /* else: ?>
    <div class="container lg-padding text-center">
        <h4 class="text-muted">Не найдено</h4>
        <a href="/catalog" class="btn btn-primary">Вернуться в каталог</a>
    </div>
<?php endif */ ?>


