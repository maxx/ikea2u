<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "\n<div class=\"items-list\">{items}\n{pager}</div>",
    'itemView' => '_list',
    'id' => 'my-listview-id',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'product',
    ],
    'pager' => [
        'class' => InfiniteScrollPager::className(),
        'widgetId' => 'my-listview-id',
        'itemsCssClass' => 'items',
    ],
]);
?>