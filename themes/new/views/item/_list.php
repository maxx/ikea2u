<?php if ($model->title): ?>
    <div class="product-img">
        <img src="<?php echo app\models\Utilities::imageUrl($model->image_uri) ?>" alt=".." width="300">
        <?php /* if ($this->beginCache($model->image_uri . '_small')): ?>
            <img src="http://www.ikea.com<?php echo $model->image_uri ?>" alt=".." width="300">
            <?php $this->endCache(); ?>
        <?php endif; */ ?>
    </div>
    <h6><a><?php echo $model->title ?></a></h6>
    <span class="desc">
        <?php echo trim($model->short_description) ?>
        <br/>(арт: <?php echo trim($model->number) ?>)
    </span>
    <span class="price">
        <span class="amount-denom"><?php echo \app\models\Utilities::currencyDenom($model->getPrice('in_stock')) ?></span>        
    </span>
    <a href="/buy-in-office" class="btn btn-default" role="button" data-item-id="<?php echo $model->id ?>">Купить в офисе</a>

<?php endif; ?>


