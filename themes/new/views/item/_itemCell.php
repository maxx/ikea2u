<div class="product-img">
  <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id'=>$model->id]); ?>">
    <img src="<?php echo app\models\Utilities::imageUrl($model->image_uri) ?>" alt=".." width="300" loading="lazy">
    <?php /* if ($this->beginCache($model->image_uri . '_small')): ?>
            <img src="http://www.ikea.com<?php echo $model->image_uri ?>" alt=".." width="300">
            <?php $this->endCache(); ?>
        <?php endif; */ ?>
  </a>
  <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id'=>$model->id]); ?>"
     class="btn btn-default btn-sm icon">Подробнее</a>
</div>
<h6><a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id'=>$model->id]); ?>"><?php echo $model->title ?></a>
</h6>
<span class="desc">
    <?php echo trim($model->short_description) ?>
</span>
<span class="price">
    <span class="amount-denom">
      <?php echo \app\models\Utilities::currencyDenom($model->getPrice()) ?>
    </span>
  <span class="priceSpan"><?php echo \app\models\Utilities::priceSpan($model->price_span) ?></span>
</span>
<?php
/*

<a href="/cart/add" class="btn btn-default add-cart icon btn-sm add-to-cart" data-item-id="<?php echo $model->id ?>"
   data-icon="k">Добавить в корзину</a>


<?php
/*

if ($index % 3 === 0): ?>
    <hr class="visible-xs">
    <hr/>
<?php endif; */ ?>


