<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\Feedback;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'send-form', 'class' => 'send-form']); ?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'message')->textArea(['rows' => 6]) ?>

<?=

        $form->field($model, 'opinion')
        ->radioList(
                Feedback::itemAlias('Opinion'), [
            'item' => function($index, $label, $name, $checked, $value) {

                $return = '<label class="modal-radio">';
                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" >';
                $return .= '<i></i>';
                $return .= '<span>' . $label . '</span>';
                $return .= '</label>';

                return $return;
            }, 'class'=>'radio'
                ]
        )
        ->label(false);
?>
<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-wd', 'name' => 'contact-button']) ?>
<p class="succs-msg">Мessage was sent</p>
<?php ActiveForm::end(); ?>
            
