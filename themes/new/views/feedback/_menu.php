<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Добавить отзыв';
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container inforow">
    <div class="col-sm-4">
        <h3>Отзыв</h3>
        <p>Мы с радостью рассмотрим ваш отзыв!</p>
    </div>
    <div class="col-sm-7 col-sm-offset-1" id="send-us">
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <h2>Спасибо за отзыв!</h2>
            <h6 class="text-muted">Мы постараемся рассмотреть его в самое ближайшее время.</h6>
        <?php else: ?>
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        <?php endif; ?>
    </div>
</div>

