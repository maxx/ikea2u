<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title='Доставка мебели и предметов интерьера на дом по городу Гомелю под заказ без предоплаты';
?>

<!-- PRODUCTS -->
<div class="container categories">
  <!-- PRODUCT-LIST -->
  <div class="col-md-9 col-sm-8 product-list text-center">
    <div class="container text-center no-padding-top">
      <?php echo $page->body ?>

    </div>
    <div class="row categories-list">
      <?php foreach ($categories as $category): ?>
        <div class="col-md-3 col-sm-4">
          <div class="card box-shadow">
            <div class="card-body">
              <?php if ($category->main_image_src): ?>
                <a
                  href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id'=>$category->id]); ?>">
                  <img src="<?php echo $category->main_image_src ?>" class="img-responsive" alt="">
                </a>
              <?php endif; ?>
              <h6>
                <a
                  href="<?php echo \yii\helpers\Url::toRoute(['item/category-view', 'id'=>$category->id]); ?>"><?php echo $category->title ?></a>
              </h6>
            </div>
          </div>
        </div>
      <?php endforeach ?>
      <!--<div class="col-md-3 col-sm-4">
        <div class="card box-shadow">
          <div class="card-body">
            <a href="/winter">
              <img src="https://www.ikea.com/global/assets/navigation/images/winter-holiday-wt001.jpeg?imwidth=400"
                   class="img-responsive" alt="Зимняя коллекция">
            </a>
            <h6>
              <a href="/winter">Зимняя коллекция</a>
            </h6>
          </div>
        </div>
      </div>-->
    </div>

    <a href="/catalog" class="btn btn-default">Посмотреть все категории</a>
  </div>


  <!-- /.product-list -->

  <!-- CATALOG BAR -->
  <div class="col-md-3 col-sm-4 catalog-bar">
    <div id="instagram">
      <h6>Мы в <a href="https://www.instagram.com/i2u_ikea_gomel/">Инстаграм</a></h6>
      <!--   <div id="pixlee_container"></div><script type="text/javascript">window.PixleeAsyncInit = function() {Pixlee.init({apiKey:'x_dLdoxZdhAv5lOGh1ss'});Pixlee.addSimpleWidget({widgetId:'22941'});};</script><script src="//instafeed.assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>-->
    </div>
    <!-- widget -->
    <div class="widget review">
      <h4>Последние отзывы</h4>
      <ul class="recent-posts flat">
        <?php foreach ($dataProviderFeedback->getModels() as $item): ?>
          <li>
            <div class="post-date"
                 data-select-like-a-boss="1"><?php echo date('d M Y', strtotime($item['created_at'])) ?></div>
            <div><?php echo Html::encode($item['message']) ?></div>
            <div class="post-name"><small><?php echo Html::encode($item['place']) ?></small> <?php echo $item['name'] ?>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
      <a href="/feedback" class="btn btn-block btn-primary">Все отзывы <sup><?php echo($feedbackCount + 30) ?></sup>
        &rarr;</a>
    </div>
    <!-- /.widget -->

    <hr>
    <!-- widget -->
    <div class="widget">
      <h4>Присоединяйтесь:</h4>
      <div id="ok_group_widget"></div>
      <div id="vk_groups"></div>
      <br/>

      <!--<div class="social"><img src="images/widget-ok.jpg" alt=""></div>
      <div class="social"><img src="images/widget-vk.jpg" alt=""></div>-->

      <script>
        !function (d, id, did, st) {
          var js = d.createElement("script");
          js.src = "http://connect.ok.ru/connect.js";
          js.onload = js.onreadystatechange = function () {
            if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
              if (!this.executed) {
                this.executed = true;
                setTimeout(function () {
                  OK.CONNECT.insertGroupWidget(id, did, st);
                }, 0);
              }
            }
          }
          d.documentElement.appendChild(js);
        }(document, "ok_group_widget", "57501799677996", "{width:240,height:460}");
      </script>

      <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
      <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 0, width: "240"}, 80053860);
      </script>

    </div>
    <!-- /.widget -->
  </div>
  <!-- /.catalog-bar -->
</div>
<!-- /.products -->