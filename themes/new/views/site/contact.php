<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title='Контакты';
$this->params['breadcrumbs'][]=$this->title;
?><!-- WRAPPER -->
<div class="wrapper border">
  <!-- HEADCONTENT -->
  <div class="headcontent">
    <div class="container">
      <div class="col-sm-6">
        <h1>Контакты</h1>
      </div>
      <div class="col-sm-6 text-right">
        <ol class="breadcrumb">
          <li><a href="/">Главная</a></li>
          <li class="active">Контакты</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- /.headcontent -->

  <!-- CONTAINER -->
  <div class="container inforow">
    <div class="col-sm-4">
      <div class="icon icon-default" data-icon="G"></div>
      <h4>Адрес офисов</h4>
      <address><p><strong>Гомель, ул.Карповича, д.19</strong></p>
        <p>К нам можно добраться:</p>
        <!--<p>автобусом: 15, 22, 22А</p>
        <p>троллейбусом: 2, 8, 11, 12, 19</p>-->
      </address>
      <address><p><strong>Гомель, ул.Бородина Т.С, д.6</strong></p>
        <p>К нам можно добраться:</p>
        <p>автобусом: 17, 18</p>
        <p>троллейбусом: </p>
      </address>
    </div>
    <div class="col-sm-4">
      <div class="icon icon-default" data-icon="Q"></div>
      <h4>Телефоны &amp; Email</h4>
      <p>Карповича, 19: <a href="tel:+375447111242">+375 (44) 7-111-242</a> <br/>
        Бородина Т.С, 6: <a href="tel:+375447111252">+375 (44) 7-111-252</a><br/>
        Рекламации, сотрудничество: <br/><a href="tel:0296589075">+375 (29) 658-90-75</a>
      </p>
      <p>
        Email: <a href="mailto:6589075@gmail.com">6589075@gmail.com</a>
      </p>
      <p>
        <a href="#send-us" class="internal btn btn-primary btn-wd">Написать нам</a>
      </p>
      <!--<dl class="dl-horizontal">
        <dd>

        </dd>
      </dl>-->
    </div>
    <div class="col-sm-4">
      <div class="icon icon-default" data-icon="2"></div>
      <h4>Время работы магазинов</h4>
      <h6>Ул. Карповича, 19</h6>
      <p> 10:00-19:00 ежедневно</p>
      <h6>Ул. Т.С.Бородина, 6</h6>
      <p> 10:00-19:00 ежедневно</p>
    </div>
  </div>
  <!-- /.container -->
  <!-- MAP -->
  <div class="google-map ">
    <div class="container no-padding-top">
      <div class="col-md-6 map-wrapper">
        <h3>Бородина, 6</h3>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2431.073838264503!2d31.025979615805213!3d52.45969017980279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46d4692491cd4553%3A0x87508994d4ae6f42!2z0YPQu9C40YbQsCDQoi7QoS4g0JHQvtGA0L7QtNC40L3QsCA2LCDQk9C-0LzQtdC70Yw!5e0!3m2!1sru!2sby!4v1589194618787!5m2!1sru!2sby"
          frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
          tabindex="0"></iframe>

      </div>
      <div class="col-md-6  map-wrapper">
        <h3>Карповича, 19</h3>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2432.85018140055!2d30.99613041595062!3d52.42751225083162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46d4690360b9dd33%3A0x6279a95347ffaf11!2z0JTQvtGB0YLQsNCy0LrQsCBJS0VBINCyINCT0L7QvNC10LvQtSAoaTJVLkJZKQ!5e0!3m2!1sru!2sby!4v1619526379369!5m2!1sru!2sby"
          style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>

      <!--<div class="google-map-container google-map-big" data-longitude="30.976902" data-latitude="52.437712"
           data-zoom="15"></div>-->
    </div>
  </div>
  <!-- /.google-map -->

  <div class="container no-padding-top">

    <h2>Виртуальный тур по магазину</h2>
    <div class="col-md-6">

      <div style="position:relative;overflow:hidden;"><a
          href="https://yandex.by/maps/org/i2u_by/145016139629/?utm_medium=mapframe&utm_source=maps"
          style="color:#eee;font-size:12px;position:absolute;top:0px;">I2u.by</a><a
          href="https://yandex.by/maps/155/gomel/category/custom_made_furniture/184107879/?utm_medium=mapframe&utm_source=maps"
          style="color:#eee;font-size:12px;position:absolute;top:14px;">Мебель на заказ в Гомеле</a>
        <iframe src="https://yandex.by/map-widget/v1/-/CCUANEGDpA" width="560" height="400" frameborder="1"
                allowfullscreen="true" style="position:relative;"></iframe>
      </div>


      <iframe
        src="https://www.google.com/maps/embed?pb=!4v1605522022625!6m8!1m7!1sCAoSLEFGMVFpcE5kbGEydEhEaS1EV0E0UVR0TXk0MkczZmRqMVltZ2NqS0pGSU1U!2m2!1d52.43784952698778!2d30.97455172394256!3f257.76368752679264!4f2.045745868052961!5f0.7820865974627469"
        width="560" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>

    </div>
    <div class="col-md-6">
      <!--
            <div style="position:relative;overflow:hidden;">
              <a href="https://yandex.by/maps/org/i2u_by/62102739521/?utm_medium=mapframe&utm_source=maps"
                 style="color:#eee;font-size:12px;position:absolute;top:0px;">I2u.by</a><a
                href="https://yandex.by/maps/155/gomel/category/interior_goods/?utm_medium=mapframe&utm_source=maps"
                style="color:#eee;font-size:12px;position:absolute;top:14px;">Товары для интерьера в Гомеле</a>
              <a
                href="https://yandex.by/maps/155/gomel/category/custom_made_furniture/?utm_medium=mapframe&utm_source=maps"
                style="color:#eee;font-size:12px;position:absolute;top:28px;">Мебель на заказ в Гомеле</a>
              <iframe src="https://yandex.by/map-widget/v1/-/CCQ3zRxKDD" width="560" height="400" frameborder="1"
                      allowfullscreen="true" style="position:relative;"></iframe>
            </div>

      -->
      <iframe
        src="https://www.google.com/maps/embed?pb=!4v1619526089997!6m8!1m7!1sCAoSLEFGMVFpcE5XZFpZemYxYmVreXZrdENCWmdyVVlFOFYxcnlEUkVFZmtadVJD!2m2!1d52.4276150790416!2d30.99841511459833!3f160!4f0!5f0.7820865974627469"
        width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>

  </div>
  <!-- /.wrapper -->
  <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvg-QhjSVnkzlFeUuX0H6IfH-8rZfWYus&callback=initMap">
  </script>
