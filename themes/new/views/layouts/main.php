<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\components\SearchFormWidget;
use app\components\DeliveryCountdownWidget;
use app\components\ShoppingCartWidget;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <link href="<?php echo \Yii::$app->homeUrl ?>favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="http://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">
    <link href="<?php echo \Yii::$app->view->theme->baseUrl ?>/css/preloader.css" rel="stylesheet">
    <link href="<?php echo \Yii::$app->view->theme->baseUrl ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo \Yii::$app->view->theme->baseUrl ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo \Yii::$app->view->theme->baseUrl ?>/css/animate.css" rel="stylesheet">
    <link href="<?php echo \Yii::$app->view->theme->baseUrl ?>/css/style.css?v=r3234344" rel="stylesheet">
    <?php $this->head() ?>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start':
            new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
          'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-5PK65DT');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body>
  <!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5PK65DT"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
  <!-- Preloader -->
  <div class="preloader"></div>

  <!-- HEADER -->
  <header class="header not-sticky header-search">
    <div class="supheader solid-color pattern">
      <div class="container">
        <div class="col-lg-3 col-md-3" style="margin-top: 0px;">
          <a class="logo" href="/">
            <img src="/img/logo_2.png" title="Доставка IKEA в Гомеле">
            <?php /*?>
                            <svg version="1.1" x="0px" y="0px" viewBox="0 0 150 78" width="100" height="52">
                            <path d="M144.4,78H5.6C2.9,78,0,75.6,0,73V39.2c0-1.8,1.4-3.4,3-4.2L72.6,0.9C75-0.3,78,0.6,79.2,3c1.2,2.3,0.1,5.2-2.3,6.3 L10,42.2V70h130V39c0-2.6,2.3-4.7,5-4.7s5,2.1,5,4.7V73C150,75.6,147.1,78,144.4,78z">
                            </svg> <?php */ ?>
            <!--<span>
                <span>i2U.BY</span>
                <span>Доставка IKEΛ в Гомеле</span>
            </span>-->
          </a>
        </div>
        <div class="col-lg-3 col-md-4 visible-lg">
          Доставка мебели и предметов интерьера на дом по городу Гомелю под заказ без предоплаты
        </div>
        <div class="visible-sm h-10"></div>
        <div class="col-lg-2 col-md-3 col-sm-6">
          <br class="hidden-lg hidden-md">
          Звоните нам: <br class="hidden-xs">
          c 09:00 до 21:00 <br/>
          <a href="<?php echo \yii\helpers\Url::toRoute('/site/contact#send-us'); ?>">Не дозвонились?</a>
        </div>
        <div class="visible-xs h-10"></div>
        <div class="col-lg-4 col-md-6 col-sm-6 phones">
          <a href="tel:+375447111252">(044) 7-111-252</a> Т.С. Бородина, 6 <br/>
          <a href="tel:+375447111242">(044) 7-111-242</a> Карповича, 19<br/>
          <a href="mailto:6589075@gmail.com">6589075@gmail.com</a>&nbsp;
        </div>
      </div>
    </div>
    <nav class="navbar">
      <div class="container">
        <!--<a class="navbar-brand visible-xs visible-sm" href="/">
                        <?php /*?>
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 150 78" height="26" width="50">
                        <path d="M144.4,78H5.6C2.9,78,0,75.6,0,73V39.2c0-1.8,1.4-3.4,3-4.2L72.6,0.9C75-0.3,78,0.6,79.2,3c1.2,2.3,0.1,5.2-2.3,6.3 L10,42.2V70h130V39c0-2.6,2.3-4.7,5-4.7s5,2.1,5,4.7V73C150,75.6,147.1,78,144.4,78z">
                        </svg><?php */ ?>
                        <span>i2U.BY <?php /*?><span class="hidden-xs">.BY</span><?php */ ?></span>
                    </a> -->

        <?php echo ShoppingCartWidget::widget(); ?>

        <button class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div id="navbar" class="navbar-collapse collapse">

          <?php
          echo Nav::widget([
              'options'=>['class'=>'nav navbar-nav pull-left'],
              'items'=>[
                  ['label'=>'Главная', 'url'=>['/']],
                //    !Yii::$app->user->isGuest ? ['label' => 'Каталог', 'url' => ['/item/index']]:'',
                  ['label'=>'Каталог', 'url'=>['/catalog']],
                  //['label'=>'Зимняя коллекция', 'url'=>['/winter'], 'options'=>['class'=>'winter']],
                  //['label'=>'СТРОЛА', 'url'=>['/strola'], 'options'=>['class'=>'winter']],
                // ['label' => 'В наличии', 'url' => ['/item/instock']],
                // ['label' => 'Акции', 'url' => ['/page/view', 'name' => 'promo']],
                  ['label'=>'Сертификаты', 'url'=>['/page/view', 'name'=>'certificate']],
                //  ['label' => 'Корзина', 'url' => ['/cart/index']],
                //        ['label' => 'Оплата и доставка', 'url' => ['/page/view', 'name' => 'pay']],
                // ['label' => 'Заказать в кредит', 'url' => ['/page/view', 'name' => 'credit']],
                  ['label'=>'О нас', 'url'=>['/about']],
                //    !Yii::$app->user->isGuest ? ['label' => 'Сделать Заказ', 'url' => ['/cart'], 'options' => ['style' => 'background-color:#dd5600 ']] : '',
                //                      ['label' => 'Оплата', 'url' => ['/site/about']],
//                        ['label' => 'Доставка', 'url' => ['/site/about']],
                //['label' => 'Товары в наличии', 'url' => ['/site/about3']],
                  ['label'=>'Отзывы', 'url'=>['/feedback']],
                //['label' => 'О нас', 'url' => ['/page/view', 'name' => 'about']],
                  ['label'=>'Контакты', 'url'=>['/contacts']],
                //  ['label' => 'Остались вопросы?', 'url' => ['/site/contact']],
                  Yii::$app->user->isGuest ? ['label'=>'', 'url'=>['/user/login']] : ['label'=>'Админка ', 'url'=>['/admin'],],
                  Yii::$app->user->isGuest ?
                    /* ['label' => 'Вход', 'url' => ['/user/login']] */ '' :
                      ['label'=>'Выход',
                          'url'=>['/user/logout'],
                          'linkOptions'=>['data-method'=>'post']],
              ],
          ]);
          ?>
        </div>
      </div>
    </nav>

    <div class="search">
      <div class="container">
        <?= SearchFormWidget::widget() ?>
      </div>
    </div>
  </header>
  <!-- /.header -->

  <!-- WRAPPER -->
  <div class="wrapper">
    <!-- HEADCONTENT -->
    <div class="headcontent">
      <div class="container">
        <div class="order-finish">
          <?= DeliveryCountdownWidget::widget() ?>
        </div>
      </div>
    </div>
    <!-- /.headcontent -->
    <div class="container" style="padding: 0">
    <?=
    Breadcrumbs::widget([
        'links'=>isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])
    ?>
    </div>
    <?= $content ?>

  </div>
  <!-- /.wrapper -->

  <!-- FOOTER -->
  <footer class="footer footer-dark ">
    <div class="container">
      <div class="row inforow">
        <div class="col-md-4 col-sm-6">
          <a class="navbar-brand" href="/">i2U.BY</a>
          <p>ИП Кусенков В.В. <br/>
            УНП 491139768 <br/>
            г.Гомель <br/>
            Свидетельство ОГРНИП 491139768 выдано 22.07.2014г. ГГИК Администрация Советского района г.Гомеля. <br/>
            Регистрация интернет-магазина в торговом реестре №302945 от 22.01.2016г.</p>
        </div>
        <div class="col-md-2 col-sm-4">
          <?php /*?>
                        <h4>Twitter</h4>
                        <ul class="twitter-list flat">
                            <li>
                                <div class="fa fa-twitter hidden-sm"></div><p><a href="">@gatwick:</a> Utnon enim  feugiat <a href="">goo.gl/56Rt78</a> famesac turpis egestas <span class="date">June 18, 2014</span></p>
                            </li>
                            <li>
                                <div class="fa fa-twitter hidden-sm"></div><p><a href="">@gatwick:</a> Mellentesque
                                    netus et malesuada <a href="">goo.gl/A65qt</a> enim eleifend felis pretium <span class="date">September 8, 2014</span></p>
                            </li>
                        </ul> <?php */ ?>
        </div>
        <div class="col-md-3 col-sm-6">
          <h4><a href="">Как с нами связаться</a></h4>
          <address>
            <p>+375 (44) 7-111-242<br/>
              <a href="mailto:6589075@gmail.com">6589075@gmail.com</a>
            </p>
            <p> ул.Карповича, д.19<br/>
              ул. Т.С. Бородина, д.6<br/>
              Гомель, РБ</p>
          </address>
        </div>
        <div class="col-md-2 col-sm-6">
          <h4>Ссылки</h4>
          <ul>
            <li><a href="/catalog">Каталог</a></li>
            <li><a href="/about">О нас</a></li>
            <li><a href="/promo">Акции</a></li>
          </ul>
        </div>
      </div>

      <hr/>

      <div class="row">
        <div class="col-sm-6">
          <p class="copyright">&copy; i2u.by <?= date('Y') ?>. Все права защищены.</p>
        </div>
        <div class="col-sm-6 text-right">
          <?php /*?>  <nav>
                            <span>Следуй за нами:</span>
                            <a href=""><i class="fa fa-dribbble"></i></a>
                            <a href=""><i class="fa fa-instagram"></i></a>
                        </nav> <?php */ ?>
        </div>
      </div>
    </div>
  </footer>
  <!-- /.footer -->

  <!-- ScrollTop -->
  <a href="#" class="scrolltop"><i></i></a>

  <!-- SCRIPTS -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="<?php echo \Yii::$app->view->theme->baseUrl ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo \Yii::$app->view->theme->baseUrl ?>/js/plugins.js"></script>
  <script src="<?php echo \Yii::$app->view->theme->baseUrl ?>/js/custom.js"></script>
  <!-- Start SiteHeart code -->
  <?php /*?>  <script>
            (function () {
                var widget_id = 748758;
                _shcp = [{widget_id: widget_id}];
                var lang = (navigator.language || navigator.systemLanguage
                        || navigator.userLanguage || "en")
                        .substr(0, 2).toLowerCase();
                var url = "widget.siteheart.com/widget/sh/" + widget_id + "/" + lang + "/widget.js";
                var hcc = document.createElement("script");
                hcc.type = "text/javascript";
                hcc.async = true;
                hcc.src = ("https:" == document.location.protocol ? "https" : "http")
                        + "://" + url;
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(hcc, s.nextSibling);
            })();
        </script>
        <!-- End SiteHeart code -->
<?php */ ?>

  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
    (function (d, w, c) {
      (w[c] = w[c] || []).push(function () {
        try {
          w.yaCounter29925139 = new Ya.Metrika({
            id: 29925139,
            webvisor: true,
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true
          });
        } catch (e) {
        }
      });

      var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () {
          n.parentNode.insertBefore(s, n);
        };
      s.type = "text/javascript";
      s.async = true;
      s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

      if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
      } else {
        f();
      }
    })(document, window, "yandex_metrika_callbacks");
  </script>
  <noscript>
    <div><img src="//mc.yandex.ru/watch/29925139" style="position:absolute; left:-9999px;" alt=""/></div>
  </noscript>
  <!-- /Yandex.Metrika counter -->


  <!-- Google Code for &#1054;&#1092;&#1086;&#1088;&#1084;&#1080;&#1090;&#1100; &#1079;&#1072;&#1082;&#1072;&#1079; Conversion Page -->
  <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 877937841;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "uu39COONpW4QsYnRogM";
    var google_remarketing_only = false;
    /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
    <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt=""
           src="//www.googleadservices.com/pagead/conversion/877937841/?label=uu39COONpW4QsYnRogM&amp;guid=ON&amp;script=0"/>
    </div>
  </noscript>

  <script src="<?php echo Url::base() . '/js/jquery.scrollUp.min.js' ?>"></script>
  <script src="<?php echo Url::base() . '/js/jquery.countdown.min.js' ?>"></script>
  <script src="<?php echo Url::base() . '/js/jscroll-master/jquery.jscroll.min.js' ?>"></script>
  <script src="<?php echo Url::base() . '/js/shopping-cart.js' ?>"></script>
  <script src="<?php echo Url::base() . '/js/main.js?v=2134543' ?>"></script>
  </body>
  </html>
<?php $this->endPage() ?>