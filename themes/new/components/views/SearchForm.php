<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
            'id' => 'search-form',
            'options' => ['class' => 'search-form'],
            'action' => '/item/search',
            'method' => 'GET',
            'fieldConfig' => [
                'template' => '{input}',

            ],
        ]);
?>
<?= $form->field($model, 'q')->textInput(array('name' => 'q', 'type'=>'search','class' => "", 'placeholder' => 'Поиск по каталогу...',)); ?><button type="submit" class="btn btn-primary">Найти</button>
<?php ActiveForm::end();
?>