<div class="cart-list pull-right">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="text-default">Ваша корзина: </span><span class="cart-total-price">
        <?php echo app\models\Utilities::currencyDenom(\Yii::$app->cart->getCost(), FALSE, false) ?></span> <ins class="cart-qty"><?php echo $cart_qty ?></ins></a>
    <div class="dropdown-menu">
        <ul>
            <?php
            $i = 1;
            foreach (\Yii::$app->cart->getPositions() as $item):
                ?>
                <li>
                    <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>" class="fig text-center pull-left"><img src="<?php echo  \app\models\Utilities::imageUrl($item->image_uri) ?>" alt=""></a>
                    <div>
                        <a href="<?php echo \yii\helpers\Url::toRoute(['/item/view', 'id' => $item->id]); ?>"><?php echo $item->title ?></a>
                        <span class="price">
                            <span class="amount"><?php echo $item->getQuantity() ?> x <?php echo app\models\Utilities::currencyDenom($item->getPrice(), FALSE, false) ?></span>
                        </span>
                    </div>
                </li>
                <?php
                $i++;
            endforeach;
            ?>
        </ul>
        <div class="hcart-total clearfix">
            <div class="total pull-left">Всего - <ins class="cart-total-items"><?php echo app\models\Utilities::currencyDenom(\Yii::$app->cart->getCost(), FALSE, false) ?></ins></div>
            <a href="/cart" class="btn btn-block btn-primary pull-left">Перейти в корзину</a>
        </div>
    </div>
</div>